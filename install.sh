apt update
apt upgrade
apt -y install git curl nano
cd /opt/lampp/htdoc
cd core
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer
php init
composer install
composer update
git submodule init
git submodule update

php yii migrate
php yii migrate --migrationPath=backend/modules/contabilidad/migrations
php yii migrate --migrationPath=backend/modules/calendario/migrations
php yii migrate --migrationPath=@bedezign/yii2/audit/migrations

mysql -u root -e "CREATE USER 'dmin'@'%' IDENTIFIED BY 'dmin';"
mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'dmin'@'%' WITH GRANT OPTION;"
mysql -u root -e "CREATE DATABASE dmin;"
mysql -u root -e "GRANT ALL PRIVILEGES ON dmin.* TO 'dmin'@'%' WITH GRANT OPTION;"

# chown daemon ../web/uploads/
# chmod -R 0755 ../web/uploads/