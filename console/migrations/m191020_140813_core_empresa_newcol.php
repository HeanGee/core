<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m191020_140813_core_empresa_newcol extends Migration
{
    public function safeUp()
    {
        $this->addColumn('core_empresa', 'logo', $this->string(10)->null());
        $this->addColumn('core_empresa', 'fondo', $this->string(10)->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
