<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180428_030316_alter_column_codigo_set_bar_dep_cuid extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('codigo_set', 'core_ciudad');
        $this->dropIndex('codigo_set', 'core_barrio');
        $this->dropIndex('codigo_set', 'core_departamento');
    }

    public function safeDown()
    {
        echo "m180428_030316_alter_column_codigo_set_bar_dep_cuid no puede ser revertido.\n";
        return false;
    }
}
