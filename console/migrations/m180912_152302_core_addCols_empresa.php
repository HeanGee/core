<?php

use jamband\schemadump\Migration;

class m180912_152302_core_addCols_empresa extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%core_empresa}}', 'primer_nombre', "{$this->string(100)->null()} AFTER nombre");
        $this->addColumn('{{%core_empresa}}', 'segundo_nombre', "{$this->string(100)->null()} AFTER primer_nombre");
        $this->addColumn('{{%core_empresa}}', 'primer_apellido', "{$this->string(100)->null()} AFTER segundo_nombre");
        $this->addColumn('{{%core_empresa}}', 'segundo_apellido', "{$this->string(100)->null()} AFTER primer_apellido");
        $this->addColumn('{{%core_empresa}}', 'cedula', "{$this->string(100)->null()} AFTER segundo_apellido");
        $this->addColumn('{{%core_empresa}}', 'tipo', "ENUM('juridica', 'personal') NOT NULL DEFAULT 'juridica' AFTER cedula");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
