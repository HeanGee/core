INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (34, 0, 'CAPITAL', 1);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (35, 1, 'CONCEPCION', 2);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (36, 2, 'SAN PEDRO', 3);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (37, 3, 'CORDILLERA', 4);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (38, 4, 'GUAIRA', 5);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (39, 5, 'CAAGUAZU', 6);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (40, 6, 'CAAZAPA', 7);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (41, 7, 'ITAPUA', 8);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (42, 8, 'MISIONES', 9);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (43, 9, 'PARAGUARI', 10);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (44, 10, 'ALTO PARANA', 11);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (45, 11, 'CENTRAL', 12);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (46, 12, 'NEEMBUCU', 13);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (47, 13, 'AMAMBAY', 14);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (48, 14, 'PTE. HAYES', 15);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (49, 15, 'BOQUERON', 16);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (50, 16, 'ALTO PARAGUAY', 17);
INSERT INTO core_departamento(id, numero, nombre, codigo_set) VALUES (51, 17, 'CANINDEYU', 18);