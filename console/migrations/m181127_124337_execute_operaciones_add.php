<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181127_124337_execute_operaciones_add extends Migration
{
    public function safeUp()
    {
        $this->execute(file_get_contents(__DIR__ . "/script_operacion.sql"));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido\n';
        return false;
    }
}
