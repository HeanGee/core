<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180423_133016_drop_index_simbolo_unique_core_moneda extends Migration
{
    public function safeUp()
    {
        $db = Yii::$app->getDb()->schema;
        try {
            $this->execute('DROP INDEX simbolo ON core_moneda;');
        } catch (Exception $e) {
            echo "El indice ya no existe.";
        }
    }

    public function safeDown()
    {
        echo "m180423_133016_drop_index_simbolo_unique_core_moneda no puede ser revertido.\n";
        return false;
    }
}
