<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180410_153031_crear_tabla_iva extends Migration
{
    public function safeUp()
    {
        // crear tabla de iva
        $this->createTable('{{%core_iva}}', [
            'id'            => $this->primaryKey()->unsigned(),
            'porcentaje'    => $this->integer(45)->notNull(),
            'estado'        => $this->integer(2)->notNull(),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        echo "m180410_153031_crear_tabla_iva no puede ser revertido.\n";
        return false;
    }
}
