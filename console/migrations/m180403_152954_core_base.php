<?php

use jamband\schemadump\Migration;

class m180403_152954_core_base extends Migration
{
    public function safeUp()
    {
        // estado
        $this->createTable('{{%core_estado}}', [
            'id' => $this->primaryKey(),
            'estado_nombre' => $this->string(45)->notNull(),
            'estado_valor' => $this->smallInteger(6)->notNull(),
        ], $this->tableOptions);

        // genero
        $this->createTable('{{%core_genero}}', [
            'id' => $this->primaryKey()->unsigned(),
            'genero_nombre' => $this->string(45)->notNull(),
        ], $this->tableOptions);

        // operacion
        $this->createTable('{{%core_operacion}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(64)->notNull(),
        ], $this->tableOptions);

        // perfil
        $this->createTable('{{%core_perfil}}', [
            'id' => $this->primaryKey()->unsigned(),
            'usuario_id' => $this->integer(11)->unsigned()->notNull(),
            'nombre' => $this->text()->null(),
            'apellido' => $this->text()->null(),
            'fecha_nacimiento' => $this->datetime()->null(),
            'genero_id' => $this->integer(10)->unsigned()->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime()->notNull(),
            'avatar' => $this->text()->null(),
        ], $this->tableOptions);

        // rol
        $this->createTable('{{%core_rol}}', [
            'id' => $this->primaryKey(),
            'rol_nombre' => $this->string(45)->notNull(),
            'rol_valor' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        // rol_operacion
        $this->createTable('{{%core_rol_operacion}}', [
            'rol_id' => $this->integer(11)->notNull(),
            'operacion_id' => $this->integer(11)->notNull(),
            'PRIMARY KEY (rol_id, operacion_id)',
        ], $this->tableOptions);

        // barrio
        $this->createTable('{{%core_barrio}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(255)->notNull(),
            'ciudad_id' => $this->integer(11)->notNull(),
            'active' => $this->boolean()->null()->defaultValue(1),
            'codigo_set' => $this->integer(10)->null()->unique(),
        ], $this->tableOptions);

        // ciudad
        $this->createTable('{{%core_ciudad}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(255)->notNull(),
            'departamento_id' => $this->integer(11)->notNull(),
            'active' => $this->boolean()->notNull()->defaultValue(1),
            'codigo_set' => $this->integer(10)->null()->unique(),
        ], $this->tableOptions);

        // departamento
        $this->createTable('{{%core_departamento}}', [
            'id' => $this->primaryKey(),
            'numero' => $this->integer(11)->notNull(),
            'nombre' => $this->string(255)->notNull(),
            'active' => $this->smallInteger(1)->unsigned()->null()->defaultValue(1),
            'codigo_set' => $this->integer(10)->null()->unique(),
        ], $this->tableOptions);

        // empresa
        $this->createTable('{{%core_empresa}}', [
            'id' => $this->primaryKey()->unsigned(),
            'razon_social' => $this->string(255)->notNull(),
            'nombre' => $this->string(45)->null(),
            'ruc' => $this->integer(11)->notNull(),
            'digito_verificador' => $this->boolean()->notNull(),
            'actividades_comerciales' => $this->text()->null(),
            'direccion' => $this->string(255)->null(),
            'telefonos' => $this->string(255)->null(),
            'coordenadas' => $this->string(255)->null(),
            'observaciones' => $this->text()->null(),
            'departamento_id' => $this->integer(11)->null(),
            'ciudad_id' => $this->integer(11)->null(),
            'barrio_id' => $this->integer(11)->null(),
            'activo' => $this->smallInteger(1)->unsigned()->null()->defaultValue(1),
        ], $this->tableOptions);

        // empresa_sucursal
        $this->createTable('{{%core_empresa_sucursal}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nombre' => $this->string(45)->notNull(),
            'departamento_id' => $this->integer(11)->null(),
            'ciudad_id' => $this->integer(11)->null(),
            'barrio_id' => $this->integer(11)->null(),
            'direccion' => $this->string(255)->null(),
            'telefono' => $this->string(45)->null(),
            'usuario_id' => $this->integer(10)->unsigned()->null(),
            'empresa_id' => $this->integer(11)->unsigned()->notNull(),
        ], $this->tableOptions);

        // usuario_empresa
        $this->createTable('{{%core_usuario_empresa}}', [
            'usuario_id' => $this->integer(10)->unsigned()->notNull(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'PRIMARY KEY (usuario_id, empresa_id)',
        ], $this->tableOptions);

        // tipo_usuario
        $this->createTable('{{%core_tipo_usuario}}', [
            'id' => $this->primaryKey(),
            'tipo_usuario_nombre' => $this->string(45)->notNull(),
            'tipo_usuario_valor' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        // usuario
        $this->createTable('{{%core_usuario}}', [
            'id' => $this->primaryKey()->unsigned(),
            'username' => $this->string(255)->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string(255)->notNull(),
            'password_reset_token' => $this->string(255)->null()->unique(),
            'email' => $this->string(255)->notNull()->unique(),
            'rol_id' => $this->integer(11)->notNull(),
            'estado_id' => $this->integer(11)->notNull(),
            'tipo_usuario_id' => $this->integer(11)->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime()->notNull(),
        ], $this->tableOptions);

        // core_moneda
        $this->createTable('{{%core_moneda}}', [
            'id' => $this->primaryKey(),
            'simbolo' => $this->string(5)->notNull()->unique(),
            'nombre' => $this->string(45)->notNull()->unique(),
            'plural' => $this->string(45)->notNull(),
            'tiene_decimales' => "ENUM ('si', 'no') NOT NULL DEFAULT 'si'"
        ], $this->tableOptions);

        // parametro_sistema
        $this->createTable('{{%core_parametro_sistema}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nombre' => $this->string(45)->notNull()->unique(),
            'valor' => $this->string(45)->notNull(),
        ], $this->tableOptions);

        // fk: perfil
        $this->addForeignKey('fk_perfil_genero_id', '{{%core_perfil}}', 'genero_id', '{{%core_genero}}', 'id');
        $this->addForeignKey('fk_perfil_usuario_id', '{{%core_perfil}}', 'usuario_id', '{{%core_usuario}}', 'id');

        // fk: rol_operacion
        $this->addForeignKey('fk_rol_operacion_operacion_id', '{{%core_rol_operacion}}', 'operacion_id', '{{%core_operacion}}', 'id');
        $this->addForeignKey('fk_rol_operacion_rol_id', '{{%core_rol_operacion}}', 'rol_id', '{{%core_rol}}', 'id');

        // fk: barrio
        $this->addForeignKey('fk_core_barrio_ciudad_id', '{{%core_barrio}}', 'ciudad_id', '{{%core_ciudad}}', 'id');

        // fk: ciudad
        $this->addForeignKey('fk_core_ciudad_departamento_id', '{{%core_ciudad}}', 'departamento_id', '{{%core_departamento}}', 'id');

        // fk: core_empresa
        $this->addForeignKey('fk_core_empresa_barrio_id', '{{%core_empresa}}', 'barrio_id', '{{%core_barrio}}', 'id');
        $this->addForeignKey('fk_core_empresa_ciudad_id', '{{%core_empresa}}', 'ciudad_id', '{{%core_ciudad}}', 'id');
        $this->addForeignKey('fk_core_empresa_departamento_id', '{{%core_empresa}}', 'departamento_id', '{{%core_departamento}}', 'id');

        // fk: core_empresa_sucursal
        $this->addForeignKey('fk_core_empresa_sucursal_barrio_id', '{{%core_empresa_sucursal}}', 'barrio_id', '{{%core_barrio}}', 'id');
        $this->addForeignKey('fk_core_empresa_sucursal_ciudad_id', '{{%core_empresa_sucursal}}', 'ciudad_id', '{{%core_ciudad}}', 'id');
        $this->addForeignKey('fk_core_empresa_sucursal_departamento_id', '{{%core_empresa_sucursal}}', 'departamento_id', '{{%core_departamento}}', 'id');
        $this->addForeignKey('fk_core_empresa_sucursal_usuario_id', '{{%core_empresa_sucursal}}', 'usuario_id', '{{%core_usuario}}', 'id');
        $this->addForeignKey('fk_core_empresa_sucursal_empresa_id', '{{%core_empresa_sucursal}}', 'empresa_id', '{{%core_empresa}}', 'id');

        // fk: core_usuario_empresa
        $this->addForeignKey('fk_core_usuario_empresa_empresa_id', '{{%core_usuario_empresa}}', 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey('fk_core_usuario_empresa_usuario_id', '{{%core_usuario_empresa}}', 'usuario_id', '{{%core_usuario}}', 'id');

        // fk: usuario
        $this->addForeignKey('fk_usuario_estado_id', '{{%core_usuario}}', 'estado_id', '{{%core_estado}}', 'id');
        $this->addForeignKey('fk_usuario_rol_id', '{{%core_usuario}}', 'rol_id', '{{%core_rol}}', 'id');
        $this->addForeignKey('fk_usuario_tipo_usuario_id', '{{%core_usuario}}', 'tipo_usuario_id', '{{%core_tipo_usuario}}', 'id');

        //insert data
        //Estado
        $this->insert('core_estado', array(
            'estado_nombre' => 'Activo',
            'estado_valor' => 10,
        ));
        $this->insert('core_estado', array(
            'estado_nombre' => 'Pendiente',
            'estado_valor' => 5,
        ));
        $this->insert('core_estado', array(
            'estado_nombre' => 'Inactivo',
            'estado_valor' => 0,
        ));

        //Genero
        $this->insert('core_genero', array(
            'genero_nombre' => 'Masculino',
        ));
        $this->insert('core_genero', array(
            'genero_nombre' => 'Femenino',
        ));

        //Rol
        $this->insert('core_rol', array(
            'rol_nombre' => 'Usuario',
            'rol_valor' => 10,
        ));
        $this->insert('core_rol', array(
            'rol_nombre' => 'Admin',
            'rol_valor' => 20,
        ));
        $this->insert('core_rol', array(
            'rol_nombre' => 'SuperUsuario',
            'rol_valor' => 30,
        ));

        //Tipo Usuario
        $this->insert('core_tipo_usuario', array(
            'tipo_usuario_nombre' => 'normal',
            'tipo_usuario_valor' => 10,
        ));
        $this->insert('core_tipo_usuario', array(
            'tipo_usuario_nombre' => 'administrador',
            'tipo_usuario_valor' => 30,
        ));

        $this->insert('core_usuario', array(
            'username' => 'admin',
            'auth_key' => 'ZcqWkNR0Vom_GQOY9AWnkVK-PnwpRf2w',
            'password_hash' => 'e0147107f34b9f31cef695f5826ec1fb', //.admin.
            'email' => 'admin@ingenio.com.py',
            'rol_id' => 3, //SuperUsuario
            'tipo_usuario_id' => 2,//administrador
            'estado_id' => 1, //Activo
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ));
    }

    public function safeDown()
    {
        echo "m180403_152954_core_base no puede ser revertido.\n";
        return false;
    }
}
