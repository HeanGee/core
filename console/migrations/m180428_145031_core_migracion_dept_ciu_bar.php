<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180428_145031_core_migracion_dept_ciu_bar extends Migration
{
    public function safeUp()
    {
//        echo __DIR__.system."migracion_core_dept_ciu_bar.sql";
        $this->execute(file_get_contents(__DIR__."/migracion_core_dept.sql"));
        $this->execute(file_get_contents(__DIR__."/migracion_core_ciu.sql"));
        $this->execute(file_get_contents(__DIR__."/migracion_core_bar.sql"));
    }

    public function safeDown()
    {
        echo "m180428_145031_core_migracion_dept_ciu_bar no puede ser revertido.\n";
        return false;
    }
}