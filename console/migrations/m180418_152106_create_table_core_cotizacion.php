<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180418_152106_create_table_core_cotizacion extends Migration
{
    public function safeUp()
    {
        // core_cotizacion
        $this->createTable('{{%core_cotizacion}}', [
            'id' => $this->primaryKey(),
            'moneda_id' => $this->integer(11)->notNull(),
            'compra' => $this->decimal(7, 2)->notNull(),
            'venta' => $this->decimal(7, 2)->notNull(),
            'fecha' => $this->date()->notNull(),
        ], $this->tableOptions);

        // fk: core_cotizacion
        $this->addForeignKey('fk_core_cotizacion_moneda_id', '{{%core_cotizacion}}', 'moneda_id', '{{%core_moneda}}', 'id');
    }

    public function safeDown()
    {
        echo "m180403_152954_core_base no puede ser revertido.\n";
        return false;
    }
}
