<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181127_123437_core_operacion_alter_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('core_operacion', 'operacion', $this->string(90)->notNull()->unique());
        $this->alterColumn('core_operacion', 'nombre', $this->string(90)->notNull());
    }

    public function safeDown()
    {
        echo "m181127_123437_core_operacion_alter_columns no puede ser revertido.\n";
        return false;
    }
}
