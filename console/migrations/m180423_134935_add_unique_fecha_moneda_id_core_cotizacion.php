<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180423_134935_add_unique_fecha_moneda_id_core_cotizacion extends Migration
{
    public function safeUp()
    {
        try {
            $this->execute('CREATE UNIQUE INDEX core_cotizacion_fecha_moneda_id_uindex ON core_cotizacion (fecha, moneda_id);');
        } catch (Exception $e) {
            echo "El indice ya existe.";
        }
    }

    public function safeDown()
    {
        echo "m180423_134935_add_unique_fecha_moneda_id_core_cotizacion no puede ser revertido.\n";
        return false;
    }
}
