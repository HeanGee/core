<?php

namespace console\controllers;

use Faker\Provider\Base;
use yii\helpers\BaseConsole;
//use common\helpers\Kafka;
use RdKafka;


class EntidadController extends \yii\console\Controller
{
    public $message;

    public function options($actionID)
    {
        return ['message'];
    }

    public function optionAliases()
    {
        return [
            'm' => 'message'
        ];
    }

    public function actionProcesarEntidades()
    {
        $conf = new RdKafka\Conf();
        $conf->set('log_level', (string) LOG_DEBUG);
        $conf->set('debug', 'all');
        $rk = new RdKafka\Producer($conf);
        $rk->addBrokers("localhost:9092");
        $topic = $rk->newTopic("t1");

        function microtime_float() {
            list($usec, $sec) = explode(" ", microtime());
            return ((float)$usec + (float)$sec);
        }
        $timeA = microtime_float();

        $ratio = 2000;
        $return = 0;
        $current_user = $this->message;

        if (!$current_user) {
            $this->stdout("username not specified.\n", BaseConsole::FG_YELLOW);
        } else {
//            $current_user = str_replace(" ", "\ ", $current_user);
            $dir = dirname(__DIR__,2) . "/backend/web/uploads/$current_user/entidades/";
            $dir_exists = file_exists($dir) && is_dir($dir);
            if (! $dir_exists ) {
                $this->stdout("path '$dir' doesn't exist.\n", BaseConsole::FG_YELLOW);
            } else {
                $files = array_filter(scandir($dir), function ($value) {
                    return !in_array($value, [".", "..", ".zip"]);
                });
                $bulk = [];
                $processed_ruc = [];
                foreach ($files as $file) {
                    $timeC = microtime_float();
                    $_counter = 0;

                    $filepath = $dir . $file;
                    $rows = number_format(count(file($filepath)));

                    print("File '{$file}' has {$rows} rows.\n");

                    $handler = fopen($filepath, 'r');
                    $line_counter = 0;
                    while (($line = trim(fgets($handler))) != false) {
                        $_counter++;

                        if (!strlen($line)) continue;
                        else {
                            $register = explode('|', $line);
                            if (count($register) == 5) // 4 columnas + fin de linea
                            {
                                $ruc = trim($register[0]);
                                if (!in_array($ruc, $processed_ruc)) {
                                    $processed_ruc[] = $ruc;

                                    $razon_social = trim($register[1]);
                                    $digito_verificador = trim($register[2]);
                                    $direccion = '';
                                    $telefonos = '';
                                    $es_contribuyente = 'Si';
                                    $tipo_entidad = null;
                                    $extranjero = "No";
                                    $bulk[] = "{$razon_social};{$ruc};{$tipo_entidad};{$digito_verificador};{$direccion};{$telefonos};{$es_contribuyente};{$extranjero}";
                                }
                            }
                        }
                         $line_counter++;

                        if ($line_counter % $ratio == 0) {
                            $this->stdout("  > Rows Processed: " . number_format($line_counter) . "\r", BaseConsole::FG_YELLOW);
                        }
                    }
                    $this->stdout("\n");

                    $timeD = microtime_float();
                    $timeDiff = ($timeD - $timeC) / 60;
                    $msg = "Time for {$file}: {$timeDiff} minutes.";
                    print($msg);
                    print("\n\n");
                    $topic->produce(RD_KAFKA_PARTITION_UA, 0, $msg);
                }

                $timeB = microtime_float();
                $timeDiff = ($timeB - $timeA) / 60;

                print("\n\nTime taken: {$timeDiff} minutes.");
            }
        }
        $rk->flush(1000);
        return $return;
    }
}