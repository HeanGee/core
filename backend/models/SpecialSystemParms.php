<?php

namespace backend\models;

/**
 *
 *
 * @property string $motivo_no_vigencia_venta
 * @property string $permitir_carga_venta_paralelo
 * // * @property string $cuenta_intereses_pagados
 * @property string $tipodoc_set_retencion_id
 * // * @property string $plantilla_seguro_a_devengar_id
 * @property string $cuenta_seguros_pagados_id
 * @property string $cta_resultado_diff_cambio_debe
 * @property string $cta_resultado_diff_cambio_haber
 * @property string $coeficiente_costo
 * @property string $cuenta_retenciones_recibidas
 * @property string $cuenta_retenciones_emitidas
 * @property string $cuenta_retenciones_renta_emitidas
 * @property string $cuenta_retenciones_renta_recibidas
 * @property string $cuenta_error_redondeo_ganancia
 * @property string $cuenta_error_redondeo_perdida
 * @property string $cta_importacion_retencion_renta_debe
 * @property string $cta_importacion_retencion_iva_debe
 * @property string $cta_importacion_retencion_renta_haber
 * @property string $cta_importacion_retencion_iva_haber
 * @property string $cta_importacion_gnd_multa
 * @property string $cta_importacion_mercaderias_importadas
 * @property string $cta_importacion_iva_10
 * @property string $cta_importacion_prov_exterior
 * @property string $cta_importacion_haber
 * // * @property string $id_tipo_documento_autofactura
 * @property string $salario_minimo
 * @property string $cantidad_salario_min
 *
 * PARA DEPRECIACION DE ACTIVO FIJO
 * @property string $criterio_revaluo
 *
 * PARA ASIENTO DE CIERRE DE INVENTARIO DE A.BIO
 * @property string $cuenta_ganado;
 * @property string $cuenta_costo_venta_gnd;
 * @property string $cuenta_costo_venta_gd;
 * @property string $cuenta_procreo;
 * @property string $cuenta_mortandad_gnd;
 * @property string $cuenta_mortandad_gd;
 * @property string $cuenta_consumo_gnd;
 * @property string $cuenta_consumo_gd;
 * @property string $cuenta_valorizacion_hacienda;
 *
 * @property string $replicate_to
 * @property string $replicate_as
 *
 * @property array $attributes
 */
class SpecialSystemParms extends BaseModel
{
    public $motivo_no_vigencia_venta;
    public $permitir_carga_venta_paralelo;
//    public $cuenta_intereses_pagados;
    public $tipodoc_set_retencion_id;
//    public $plantilla_seguro_a_devengar_id;
    public $cuenta_seguros_pagados_id;
    public $cta_resultado_diff_cambio_debe;
    public $cta_resultado_diff_cambio_haber;
    public $coeficiente_costo;
    public $cuenta_retenciones_emitidas;
    public $cuenta_retenciones_recibidas;
    public $cuenta_retenciones_renta_emitidas;
    public $cuenta_retenciones_renta_recibidas;
    public $cuenta_error_redondeo_perdida;
    public $cuenta_error_redondeo_ganancia;
    public $cta_importacion_retencion_renta_debe;
    public $cta_importacion_retencion_iva_debe;
    public $cta_importacion_retencion_renta_haber;
    public $cta_importacion_retencion_iva_haber;
    public $cta_importacion_gnd_multa;
    public $cta_importacion_mercaderias_importadas;
    public $cta_importacion_iva_10;
    public $cta_importacion_prov_exterior;
    public $cta_importacion_haber;
//    public $id_tipo_documento_autofactura;
    public $salario_minimo;
    public $cantidad_salario_min;

    /** para asiento de cierre de inventario de a.bio. */
    public $cuenta_ganado;
    public $cuenta_costo_venta_gnd;
    public $cuenta_costo_venta_gd;
    public $cuenta_procreo;
    public $cuenta_mortandad_gnd;
    public $cuenta_mortandad_gd;
    public $cuenta_consumo_gnd;
    public $cuenta_consumo_gd;
    public $cuenta_valorizacion_hacienda;

    /** para depreciacion de activos fijos */
    public $criterio_revaluo;
    const CRITERIO_REVALUO_PERIODO_SIGUIENTE = 'siguiente_periodo_fiscal';
    const CRITERIO_REVALUO_MES_SIGUIENTE = 'siguiente_mes';

    public $replicate_to;
    public $replicate_as;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_parametro_sistema';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['motivo_no_vigencia_venta',
                'permitir_carga_venta_paralelo',
                'tipodoc_set_retencion_id',
//            'plantilla_seguro_a_devengar_id' => '',
                'cuenta_seguros_pagados_id',
                'cta_resultado_diff_cambio_debe',
                'cta_resultado_diff_cambio_haber',
                'coeficiente_costo',
                'cuenta_retenciones_emitidas',
                'cuenta_retenciones_recibidas',
                'cuenta_retenciones_renta_emitidas',
                'cuenta_retenciones_renta_recibidas',
                'cuenta_error_redondeo_perdida',
                'cuenta_error_redondeo_ganancia',
                'cta_importacion_retencion_renta_debe',
                'cta_importacion_retencion_iva_debe',
                'cta_importacion_retencion_renta_haber',
                'cta_importacion_retencion_iva_haber',
                'cta_importacion_gnd_multa',
                'cta_importacion_mercaderias_importadas',
                'cta_importacion_iva_10',
                'cta_importacion_prov_exterior',
                'cta_importacion_haber',
//            'id_tipo_documento_autofactura' => '',
                'salario_minimo',
                'cantidad_salario_min',
                'cuenta_ganado',
                'cuenta_costo_venta_gnd',
                'cuenta_costo_venta_gd',
                'cuenta_procreo',
                'cuenta_mortandad_gnd',
                'cuenta_mortandad_gd',
                'cuenta_consumo_gnd',
                'cuenta_consumo_gd',
                'cuenta_valorizacion_hacienda',
                'criterio_revaluo'], 'safe'],

            [['replicate_to', 'replicate_as'], 'safe'],
            [['replicate_to', 'replicate_as'], 'checkValidation'],
        ];
    }

    public function checkValidation($attribute, $params)
    {
        if ($this->replicate_to == '' && $this->replicate_as != '') {
            $this->addError('replicate_as', "\"{$this->getAttributeLabel('replicate_as')}\" solamente es necesario cuando \"{$this->getAttributeLabel('replicate_to')}\" no es vacío");
        }
        if ($this->replicate_to != '' && $this->replicate_as == '') {
            $this->addError('replicate_as', "\"{$this->getAttributeLabel('replicate_as')}\" es estrictamente necesario cuando \"{$this->getAttributeLabel('replicate_to')}\" no es vacío");
        }
    }

    public function getAttributes($names = null, $except = [])
    {
        $keys = [
            'motivo_no_vigencia_venta',
            'permitir_carga_venta_paralelo',
            'tipodoc_set_retencion_id',
//            'plantilla_seguro_a_devengar_id' => '',
            'cuenta_seguros_pagados_id',
            'cta_resultado_diff_cambio_debe',
            'cta_resultado_diff_cambio_haber',
            'coeficiente_costo',
            'cuenta_retenciones_emitidas',
            'cuenta_retenciones_recibidas',
            'cuenta_retenciones_renta_emitidas',
            'cuenta_retenciones_renta_recibidas',
            'cuenta_error_redondeo_perdida',
            'cuenta_error_redondeo_ganancia',
            'cta_importacion_retencion_renta_debe',
            'cta_importacion_retencion_iva_debe',
            'cta_importacion_retencion_renta_haber',
            'cta_importacion_retencion_iva_haber',
            'cta_importacion_gnd_multa',
            'cta_importacion_mercaderias_importadas',
            'cta_importacion_iva_10',
            'cta_importacion_prov_exterior',
            'cta_importacion_haber',
//            'id_tipo_documento_autofactura' => '',
            'salario_minimo',
            'cantidad_salario_min',
            'cuenta_ganado',
            'cuenta_costo_venta_gnd',
            'cuenta_costo_venta_gd',
            'cuenta_procreo',
            'cuenta_mortandad_gnd',
            'cuenta_mortandad_gd',
            'cuenta_consumo_gnd',
            'cuenta_consumo_gd',
            'cuenta_valorizacion_hacienda',
            'criterio_revaluo',
        ];

        $array = [];
        foreach ($keys as $key) {
            $array[$key] = $this->$key;
        }
        return $array;
    }
}
