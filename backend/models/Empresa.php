<?php

namespace backend\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "core_empresa_empresa".
 *
 * @property int $id
 * @property string $razon_social
 * @property string $nombre
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property string $cedula
 * @property string $tipo
 * @property int $ruc
 * @property int $digito_verificador
 * @property string $actividades_comerciales
 * @property string $direccion
 * @property int $departamento_id
 * @property int $ciudad_id
 * @property int $barrio_id
 * @property string $telefonos
 * @property string $coordenadas
 * @property string $observaciones
 * @property int $activo
 *
 * @property string $motivo_no_vigencia_venta
 * @property string $permitir_carga_venta_paralelo
 * // * @property string $cuenta_intereses_pagados
 * @property string $tipodoc_set_retencion_id
 * @property string $plantilla_seguro_a_devengar_id
 * @property string $cuenta_seguros_pagados_id
 * @property string $cta_resultado_diff_cambio_debe
 * @property string $cta_resultado_diff_cambio_haber
 * @property string $coeficiente_costo
 * @property string $timbrado_para_retenciones
 * @property string $cuenta_retenciones_recibidas
 * @property string $cuenta_retenciones_emitidas
 * @property string $cuenta_retenciones_renta_emitidas
 * @property string $cuenta_retenciones_renta_recibidas
 * @property string $cuenta_error_redondeo_ganancia
 * @property string $cuenta_error_redondeo_perdida
 * @property string $cta_importacion_retencion_renta_debe
 * @property string $cta_importacion_retencion_iva_debe
 * @property string $cta_importacion_retencion_renta_haber
 * @property string $cta_importacion_retencion_iva_haber
 * @property string $cta_importacion_gnd_multa
 * @property string $cta_importacion_mercaderias_importadas
 * @property string $cta_importacion_iva_10
 * @property string $cta_importacion_prov_exterior
 * @property string $cta_importacion_haber
 * @property string $cedula_dv
 * @property string $id_tipo_documento_autofactura
 * @property string $salario_minimo
 * @property string $cantidad_salario_min
 *
 * PARA DEPRECIACION DE ACTIVO FIJO
 * @property string $criterio_revaluo
 *
 * PARA ASIENTO DE CIERRE DE INVENTARIO DE A.BIO
 * @property string $cuenta_ganado;
 * @property string $cuenta_costo_venta_gnd;
 * @property string $cuenta_costo_venta_gd;
 * @property string $cuenta_procreo;
 * @property string $cuenta_mortandad_gnd;
 * @property string $cuenta_mortandad_gd;
 * @property string $cuenta_consumo_gnd;
 * @property string $cuenta_consumo_gd;
 * @property string $cuenta_valorizacion_hacienda;
 * @property string $dvCedula
 */
class Empresa extends BaseModel
{
    public $motivo_no_vigencia_venta;
    public $permitir_carga_venta_paralelo;
//    public $cuenta_intereses_pagados;
    public $tipodoc_set_retencion_id;
    public $plantilla_seguro_a_devengar_id;
    public $cuenta_seguros_pagados_id;
    public $cta_resultado_diff_cambio_debe;
    public $cta_resultado_diff_cambio_haber;
    public $coeficiente_costo;
    public $timbrado_para_retenciones;
    public $cuenta_retenciones_emitidas;
    public $cuenta_retenciones_recibidas;
    public $cuenta_retenciones_renta_emitidas;
    public $cuenta_retenciones_renta_recibidas;
    public $cuenta_error_redondeo_perdida;
    public $cuenta_error_redondeo_ganancia;
    public $cta_importacion_retencion_renta_debe;
    public $cta_importacion_retencion_iva_debe;
    public $cta_importacion_retencion_renta_haber;
    public $cta_importacion_retencion_iva_haber;
    public $cta_importacion_gnd_multa;
    public $cta_importacion_mercaderias_importadas;
    public $cta_importacion_iva_10;
    public $cta_importacion_prov_exterior;
    public $cta_importacion_haber;
    public $cedula_dv;
    public $id_tipo_documento_autofactura;
    public $salario_minimo;
    public $cantidad_salario_min;

    /** para asiento de cierre de inventario de a.bio. */
    public $cuenta_ganado;
    public $cuenta_costo_venta_gnd;
    public $cuenta_costo_venta_gd;
    public $cuenta_procreo;
    public $cuenta_mortandad_gnd;
    public $cuenta_mortandad_gd;
    public $cuenta_consumo_gnd;
    public $cuenta_consumo_gd;
    public $cuenta_valorizacion_hacienda;

    /** para depreciacion de activos fijos */
    public $criterio_revaluo;
    const CRITERIO_REVALUO_PERIODO_SIGUIENTE = 'siguiente_periodo_fiscal';
    const CRITERIO_REVALUO_MES_SIGUIENTE = 'siguiente_mes';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_empresa';
    }

    public static function getEmpresaLista()
    {
        $dropciones = Empresa::find()->asArray()->all();
        return ArrayHelper::map($dropciones, 'id', 'razon_social');
    }

    public static function getDigitoVerificador($numero)
    {
        $basemax = 11;
        $v_numero_al = $numero;
        //cambiamos la ultima letra a ascii en caso que la cedula termine en letra
        $ultimoNro = substr($numero, -1);
        if (!($ultimoNro <= "9" && $ultimoNro >= "0"))
            $v_numero_al = substr($numero, 0, -1) . ord($ultimoNro);

        $k = 2;
        $v_total = 0;
        for ($i = strlen($v_numero_al) - 1; $i >= 0; $i--) {
            if ($k > $basemax) {
                $k = 2;
            }
            $v_numero_aux = intval(substr($v_numero_al, $i, 1));
            $v_total = $v_total + ($v_numero_aux * $k);
            $k++;
        }
        $v_resto = $v_total % $basemax;
        if ($v_resto > 1) {
            $v_digit = $basemax - $v_resto;
        } else {
            $v_digit = 0;
        }

        return $v_digit;
    }

    public static function manejarTextoAuditoria($campo, &$viejo, &$nuevo)
    {
        switch ($campo) {
            case 'departamento_id':
                $viejo[0] = !empty($viejo[0]) ? Departamento::findOne($viejo[0])->nombre : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Departamento::findOne($nuevo[0])->nombre : $nuevo[0];
                return true;
            case 'ciudad_id':
                $viejo[0] = !empty($viejo[0]) ? Ciudad::findOne($viejo[0])->nombre : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Ciudad::findOne($nuevo[0])->nombre : $nuevo[0];
                return true;
            case 'barrio_id':
                $viejo[0] = !empty($viejo[0]) ? Barrio::findOne($viejo[0])->nombre : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Barrio::findOne($nuevo[0])->nombre : $nuevo[0];
                return true;
            case 'activo':
                $viejo[0] = $viejo[0] != '' ? ($viejo[0] == 1 ? 'Activo' : 'Pasivo') : $viejo[0];
                $nuevo[0] = $nuevo[0] != '' ? ($nuevo[0] == 1 ? 'Activo' : 'Pasivo') : $nuevo[0];
                return true;
        }
        return false;
    }

//    public function behaviors()
//    {
//        return [
//            'bedezign\yii2\audit\AuditTrailBehavior'
//        ];
//    }

    /**
     * @param bool $idTextFormat
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function getTipos($idTextFormat = false)
    {
        $valores_enum = self::getTableSchema()->columns['tipo']->enumValues;
        $result = [];
        $i = 0;
        foreach ($valores_enum as $_v) {
            $label = $_v;

            //TODO temporal
            if ($_v == 'fisica') {
                $label = 'física';
            }

            if ($idTextFormat) $result[] = ['id' => $_v, 'text' => ++$i . ' - ' . str_replace('_', " ", ucfirst($label))];
            else $result[$_v] = ++$i . ' - ' . str_replace('_', " ", ucfirst($label));
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['razon_social', 'nombre', 'ruc', 'digito_verificador', 'actividades_comerciales', 'direccion', 'departamento_id',
                'ciudad_id', 'barrio_id', 'telefonos'], 'required'],
            [['razon_social', 'nombre', 'actividades_comerciales', 'direccion', 'primer_nombre', 'segundo_nombre',
                'primer_apellido', 'segundo_apellido'], 'filter', 'filter' => 'strtoupper'],
            [['ruc', 'digito_verificador', 'departamento_id', 'ciudad_id', 'barrio_id', 'activo'], 'integer'],
            [['actividades_comerciales', 'observaciones', 'tipo', 'cedula'], 'string'],
            [['razon_social', 'direccion', 'telefonos', 'coordenadas'], 'string', 'max' => 255],
            [['nombre'], 'string', 'max' => 45],
            [['primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido'], 'string', 'max' => 100],
            [['tipo'/*, 'cuenta_intereses_pagados'*/, 'cuenta_error_redondeo_perdida', 'cuenta_error_redondeo_ganancia'], 'safe'],

            [['motivo_no_vigencia_venta', 'permitir_carga_venta_paralelo', 'tipodoc_set_retencion_id', 'plantilla_seguro_a_devengar_id',
                'cuenta_seguros_pagados_id', 'cta_resultado_diff_cambio_haber', 'cta_resultado_diff_cambio_debe',
                'coeficiente_costo', 'timbrado_para_retenciones', 'cta_importacion_retencion_renta_debe',
                'cta_importacion_retencion_iva_debe', 'cta_importacion_retencion_renta_haber', 'cta_importacion_gnd_multa',
                'cta_importacion_retencion_iva_haber', 'id_tipo_documento_autofactura', 'salario_minimo',
                'cantidad_salario_min', 'cta_importacion_mercaderias_importadas', 'cta_importacion_iva_10',
                'cta_importacion_prov_exterior', 'cta_importacion_haber'], 'safe'],

            [['cuenta_ganado', 'cuenta_costo_venta_gnd', 'cuenta_costo_venta_gd', 'cuenta_procreo',
                'cuenta_mortandad_gnd', 'cuenta_mortandad_gd', 'cuenta_consumo_gnd', 'cuenta_consumo_gd',
                'cuenta_valorizacion_hacienda', 'criterio_revaluo', 'cuenta_retenciones_recibidas',
                'cuenta_retenciones_emitidas', 'cuenta_retenciones_renta_emitidas', 'cuenta_retenciones_renta_recibidas'], 'safe'],

            [['permitir_carga_venta_paralelo',
                'cuenta_seguros_pagados_id', 'cta_resultado_diff_cambio_haber', 'cta_resultado_diff_cambio_debe',
                'coeficiente_costo'], 'required', 'when' => function ($model) {
                return class_exists('\backend\modules\contabilidad\MainModule') &&
                    (\Yii::$app->session->has('core_empresa_actual') && $this->id == \Yii::$app->session->get('core_empresa_actual'));
            }],

//            [['cuenta_ganado', 'cuenta_costo_venta_gnd', 'cuenta_costo_venta_gd', 'cuenta_procreo',
//                'cuenta_mortandad_gnd', 'cuenta_mortandad_gd', 'cuenta_consumo_gnd', 'cuenta_consumo_gd',
//                'cuenta_valorizacion_hacienda'], 'required', 'when' => function ($model) {
//                return class_exists('\backend\modules\contabilidad\MainModule') && $this->id == \Yii::$app->session->get('core_empresa_actual');
//            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return Empresa::getAttributesLabelText();
    }

    public static function getAttributesLabelText()
    {
        return [
            'id' => 'ID',
            'razon_social' => 'Razón Social',
            'nombre' => 'Nombre',
            'ruc' => 'RUC',
            'digito_verificador' => 'D. V.',
            'actividades_comerciales' => 'Actividades Comerciales',
            'direccion' => 'Direccion',
            'departamento_id' => 'Departamento',
            'ciudad_id' => 'Ciudad',
            'barrio_id' => 'Barrio',
            'telefonos' => 'Telefonos',
            'coordenadas' => 'Ubicación',
            'observaciones' => 'Observaciones',
            'activo' => 'Empresa Activa',
            'primer_nombre' => "Primer Nombre",
            'segundo_nombre' => 'Segundo Nombre',
            'primer_apellid' => 'Primer Apellido',
            'segundo_apellido' => "Segundo Apellido",
            'tipo' => 'Tipo',
            'id_tipo_documento_autofactura' => 'Tipo de Documento para Autofacturas',
            'salario_minimo' => 'Salario Minimo',
            'cantidad_salario_min' => 'Cantidad de Salario Minimo para Autofacturas',
            'permitir_carga_venta_paralelo' => "Permitir cargar factura venta en paralelo?",
            /*'cuenta_intereses_pagados' => "Cuenta para intereses pagados",*/
            'tipodoc_set_retencion_id' => 'Tipo de Documento SET Para Retenciones',
            'plantilla_seguro_a_devengar_id' => 'Plantilla para Seguros a Devengar',
            'cuenta_seguros_pagados_id' => 'Cuenta para Seguros Pagados',
            'cta_resultado_diff_cambio_haber' => 'Cuenta para Resultados por Diferencia de Cambio como Pérdida',
            'cta_resultado_diff_cambio_debe' => 'Cuenta para Resultados por Diferencia de Cambio como Ganancia',
            'coeficiente_costo' => 'Coeficiente de costo',
            'timbrado_para_retenciones' => 'Timbrado Para Retenciones',
            'cuenta_ganado' => 'Cuenta Ganado Vacuno',
            'cuenta_costo_venta_gnd' => 'Cuenta Costo de Venta de Ganado GND',
            'cuenta_costo_venta_gd' => 'Cuenta Costo de Venta de Ganado GD',
            'cuenta_consumo_gnd' => 'Cuenta Consumo Ganado Vacuno GND',
            'cuenta_consumo_gd' => 'Cuenta Consumo Ganado Vacuno GD',
            'cuenta_mortandad_gnd' => 'Cuenta Mortandad Ganado Vacuno GND',
            'cuenta_mortandad_gd' => 'Cuenta Mortandad Ganado Vacuno GD',
            'cuenta_procreo' => 'Cuenta Procreo Ganado Vacuno',
            'cuenta_valorizacion_hacienda' => 'Cuenta Valorización Hacienda',
            'cta_importacion_retencion_renta_debe' => 'Cuenta para Retención de Renta Debe',
            'cta_importacion_retencion_iva_debe' => 'Cuenta para Retención de IVA Debe',
            'cta_importacion_retencion_renta_haber' => 'Cuenta para Retención de Renta Haber',
            'cta_importacion_retencion_iva_haber' => 'Cuenta para Retención de IVA Haber',
            'criterio_revaluo' => "Criterio de revalúo de activos fijos",
            'cta_importacion_gnd_multa' => 'Cuenta para GND asiento importación',
            'cta_importacion_mercaderias_importadas' => 'Cuenta para mercaderias importadas asiento de importación',
            'cta_importacion_iva_10' => 'Cuenta para IVA 10 asiento de importación',
            'cta_importacion_prov_exterior' => 'Cuenta para proveedores del exterior asiento de importación',
            'cta_importacion_haber' => 'Cuenta para HABER/SALIDA asiento de importación',
            'cuenta_error_redondeo_perdida' => 'Cuenta para pérdida por error de redondeo',
            'cuenta_error_redondeo_ganancia' => 'Cuenta para ganancia por error de redondeo',
        ];
    }

    public function getDepartamento()
    {
        return $this->hasOne(Departamento::className(), ['id' => 'departamento_id']);
    }

    public function getCiudad()
    {
        return $this->hasOne(Ciudad::className(), ['id' => 'ciudad_id']);
    }

    public function getBarrio()
    {
        return $this->hasOne(Barrio::className(), ['id' => 'barrio_id']);
    }

    public function getEmpresaNombre()
    {
        return $this->razon_social ? $this->razon_social : '- sin razon social -';
    }

    public function getNombre()
    {
        return $this->nombre ? $this->nombre : '- sin nombre -';
    }

    public function getSucursales()
    {
        return $this->hasMany(Sucursal::className(), ['empresa_id' => 'id']);
    }

    public function getDvCedula()
    {
        if ($this->cedula != '')
            return self::getDigitoVerificador($this->cedula);
        else return '';
    }
}
