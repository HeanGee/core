<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "core_moneda".
 *
 * @property integer $id
 * @property string $simbolo
 * @property string $nombre
 * @property string $plural
 * @property string $tiene_decimales
 *
 */
class Moneda extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_moneda';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['simbolo', 'nombre', 'plural', 'tiene_decimales'], 'required'],
            [['simbolo'], 'string', 'max' => 5],
            [['tiene_decimales'], 'string', 'max' => 2],
            [['nombre', 'plural'], 'string', 'max' => 45],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'simbolo' => Yii::t('app', 'Simbolo'),
            'nombre' => Yii::t('app', 'Nombre'),
            'plural' => Yii::t('app', 'Plural'),
            'tiene_decimales' => Yii::t('app', 'Tiene Decimales')
        ];
    }

    public function tieneDecimales()
    {
        return $this->tiene_decimales == 'si';
    }

    public static function getMonedas()
    {
        return ArrayHelper::map(
            Moneda::find()
                ->asArray()
                ->all(),
            'id', 'nombre'
        );
    }

    public static function getAll()
    {
        $query = Moneda::find();
        return $query->select(['id', 'text' => 'nombre', 'tiene_decimales' => 'tiene_decimales'])->asArray()->all();
    }
}
