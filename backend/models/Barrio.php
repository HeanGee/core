<?php

namespace backend\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "core_barrio".
 *
 * @property int $id
 * @property string $nombre
 * @property int $ciudad_id
 * @property int $active
 * @property int $codigo_set
 *
 * @property Ciudad $ciudad
 */
class Barrio extends BaseModel
{
    public $departamento_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_barrio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'codigo_set', 'ciudad_id'], 'required'],
            [['departamento_id'], 'safe'],
            [['ciudad_id', 'active', 'codigo_set'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
            [['ciudad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ciudad::className(), 'targetAttribute' => ['ciudad_id' => 'id']],
            [['codigo_set'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'ciudad_id' => 'Ciudad',
            'active' => 'Activo',
            'departamento_id' => 'Departamento',
            'codigo_set' => Yii::t('app', 'Código Set')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudad()
    {
        return $this->hasOne(Ciudad::className(), ['id' => 'ciudad_id']);
    }

    public function getDepartamentoNombre()
    {
        return $this->ciudad->getDepartamentoNombre();
    }

    public function getCiudadNombre()
    {
        return $this->ciudad->nombre;
    }

    public static function getBarriosPorCiudadLista($id = null, $q = null)
    {
        $resultado = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($id)) {
            $consulta = new Query();
            $consulta->select('id, nombre AS text')
                ->from('core_barrio')
                ->where(['ciudad_id' => $id])
                ->andWhere(['=', 'active', '1'])
                ->limit(20);

            empty($q) || $consulta->andWhere(['like', 'nombre', $q]);

            $comando = $consulta->createCommand();
            $datos = $comando->queryAll();
            $resultado['results'] = array_values($datos);
        }

        return $resultado;
    }

    public static function getBarrioLista()
    {
        $opciones = Barrio::find()->asArray()->all();
        return ArrayHelper::map($opciones, 'id', 'nombre');
    }
}
