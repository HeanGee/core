<?php

namespace backend\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "core_ciudad".
 *
 * @property int $id
 * @property string $nombre
 * @property int $departamento_id
 * @property boolean $active
 * @property int $codigo_set
 *
 * @property Departamento $departamento
 */
class Ciudad extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_ciudad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'codigo_set', 'departamento_id'], 'required'],
            [['departamento_id', 'active', 'codigo_set'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
            [['departamento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departamento::className(), 'targetAttribute' => ['departamento_id' => 'id']],
            [['codigo_set'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'departamento_id' => 'Departamento',
            'active' => 'Activo',
            'codigo_set' => Yii::t('app', 'Código Set')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartamento()
    {
        return $this->hasOne(Departamento::className(), ['id' => 'departamento_id']);
    }

    public function getDepartamentoNombre()
    {
        return $this->departamento->nombre;
    }

    public static function getCiudadLista()
    {
        $opciones = Ciudad::find()->asArray()->all();
        return ArrayHelper::map($opciones, 'id', 'nombre');
    }

    public function getBarrios()
    {
        return $this->hasMany(Barrio::className(), ['ciudad_id' => 'id']);
    }

    public static function getCiudadPorDepartamentoLista($id = null, $q = null)
    {
        $resultado = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($id)) {
            $consulta = new Query();
            $consulta->select('id, nombre AS text')
                ->from('core_ciudad')
                ->where(['departamento_id' => $id])
                ->andWhere(['=', 'active', '1'])
                ->limit(20);

            empty($q) || $consulta->andWhere(['like', 'nombre', $q]);

            $comando = $consulta->createCommand();
            $datos = $comando->queryAll();
            $resultado['results'] = array_values($datos);
        }

        return $resultado;
    }
}
