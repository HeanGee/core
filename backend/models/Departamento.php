<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "core_departamento".
 *
 * @property int $id
 * @property int $numero
 * @property string $nombre
 * @property int $active
 * @property int $codigo_set
 * @property Ciudad[] $ciudades
 */
class Departamento extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_departamento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['numero', 'active', 'codigo_set'], 'integer'],
            [['nombre', 'codigo_set'], 'required'],
            [['nombre'], 'string', 'max' => 255],
            [['codigo_set'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero' => 'Numero',
            'nombre' => 'Nombre',
            'active' => 'Activo',
            'codigo_set' => Yii::t('app', 'Código Set')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudades()
    {
        return $this->hasMany(Ciudad::className(), ['departamento_id' => 'id']);
    }

    public static function getDepartamentoLista($solo_activos = false)
    {
        $active_query = Departamento::find();
        if ($solo_activos)
            $active_query->where("active = '1'");

        return ArrayHelper::map($active_query->asArray()->all(), 'id', 'nombre');
    }
}
