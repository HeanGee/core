<?php

namespace backend\models\search;

use common\models\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the
 * search form about `common\models\user`.
 */
class UserSearch extends User
{

    /**
     * attributes
     *
     * @var mixed
     */
    public $rolNombre;
    public $tipoUsuarioNombre;
    public $tipo_usuario_nombre;
    public $tipo_usuario_id;
    public $estadoNombre;
    public $perfilId;
    public $perfil_nombre;
    public $fecha_creado;
    public $usuario;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'rol_id',
                    'estado_id',
                    'tipo_usuario_id'
                ], 'integer'],
            [
                [
                    'username',
                    'email',
                    'created_at',
                    'updated_at',
                    'rolNombre',
                    'estadoNombre',
                    'tipoUsuarioNombre',
                    'perfilId',
                    'tipo_usuario_nombre',
                    'perfil_nombre',
                    'fecha_creado',
                    'usuario'
                ], 'safe'
            ],
            [
                [
                    'fecha_creado',
                ],
                'match',
                'pattern' => '/^.+\s\-\s.+$/'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {

        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->alias('user');
        $query->joinWith(['rol as rol']);
        $query->joinWith(['estado as estado']);
        $query->joinWith(['tipoUsuario as tipoUsuario']);
        $query->joinWith(['perfil as perfil']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params)
         * statement below
         */
        $dataProvider->setSort([
            'attributes' => [
                'id',
                'perfil_nombre' => [
                    'asc' => ['perfil.nombre' => SORT_ASC],
                    'desc' => ['perfil.nombre' => SORT_DESC],
                    'label' => 'Perfil'
                ],
                'username' => [
                    'asc' => ['user.username' => SORT_ASC],
                    'desc' => ['user.username' => SORT_DESC],
                    'label' => 'Nombre Us.'
                ],
                'rolNombre' => [
                    'asc' => ['rol.rol_nombre' => SORT_ASC],
                    'desc' => ['rol.rol_nombre' => SORT_DESC],
                    'label' => 'Rol'
                ],
                'estadoNombre' => [
                    'asc' => ['estado.estado_nombre' => SORT_ASC],
                    'desc' => ['estado.estado_nombre' => SORT_DESC],
                    'label' => 'Estado'
                ],
                'tipoUsuarioNombre' => [
                    'asc' => ['tipo_usuario.tipo_usuario_nombre' => SORT_ASC],
                    'desc' => ['tipo_usuario.tipo_usuario_nombre' => SORT_DESC],
                    'label' => 'Tipo Usuario'
                ],
                'created_at' => [
                    'asc' => ['created_at' => SORT_ASC],
                    'desc' => ['created_at' => SORT_DESC],
                    'label' => 'Creado el'
                ],
                'email' => [
                    'asc' => ['email' => SORT_ASC],
                    'desc' => ['email' => SORT_DESC],
                    'label' => 'Correo'
                ],
                'usuario' => [
                    'asc' => ['username' => SORT_ASC, 'perfil.nombre' => SORT_ASC],
                    'desc' => ['username' => SORT_DESC, 'perfil.nombre' => SORT_DESC],
                    'label' => 'Usuario'
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {

            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'username' => $this->username,
            'email' => $this->email,
            'rol_id' => $this->rol_id,
            'estado_id' => $this->estado_id,
            'tipo_usuario_id' => $this->tipo_usuario_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ]);

        $query->andFilterWhere(['like', 'rol.rol_nombre', $this->rolNombre])
            ->andFilterWhere(['like', 'estado.estado_nombre', $this->estadoNombre])
            ->andFilterWhere(['like', 'tipoUsuario.tipo_usuario_nombre', $this->tipoUsuarioNombre])
            ->andFilterWhere(['like', 'perfil.nombre', $this->perfil_nombre])
            ->andFilterWhere(['like', 'perfil.nombre', $this->usuario])
            ->orFilterWhere(['like', 'username', $this->usuario]);


        if (!empty($this->fecha_creado) && strpos($this->fecha_creado, '-') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->fecha_creado);
            $query->andFilterWhere(['>=', 'fecha', date('y-m-d', strtotime($start_date))])
                ->andFilterWhere(['<=', 'fecha', date('y-m-d', strtotime($end_date . '+1 day'))]);
        }

        return $dataProvider;
    }

    protected function addSearchParameter($query, $attribute, $partialMatch = false)
    {
        if (($pos = strrpos($attribute, '.')) !== false) {
            $modelAttribute = substr($attribute, $pos + 1);
        } else {
            $modelAttribute = $attribute;
        }

        $value = $this->$modelAttribute;

        if (trim($value) === '') {
            return;
        }

        /*
         * The following line is additionally added for right aliasing
         * of columns so filtering happen correctly in the self join
         */


        $attribute = "user.$attribute";

        if ($partialMatch) {
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }

}
