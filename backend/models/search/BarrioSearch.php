<?php

namespace backend\models\search;

use backend\models\Barrio;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BarrioSearch represents the model behind the search form of `backend\models\Barrio`.
 */
class BarrioSearch extends Barrio
{
    public $ciudad;
    public $departamento;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ciudad_id', 'active'], 'integer'],
            [['nombre', 'ciudad', 'departamento'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Barrio::find();
        $query->joinWith(['ciudad as ciu']);
        $query->joinWith(['ciudad.departamento as dep']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['nombre' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['ciudad'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['ciu.nombre' => SORT_ASC],
            'desc' => ['ciu.nombre' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['departamento'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['dep.nombre' => SORT_ASC],
            'desc' => ['dep.nombre' => SORT_DESC],
        ];

        $this->load($params);

        if (!($this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            Barrio::tableName().'.id' => $this->id,
            'ciudad_id' => $this->ciudad_id,
            Barrio::tableName().'.active' => $this->active <> 3 ? $this->active : '',
        ]);

        $query->andFilterWhere(['like', Barrio::tableName().'.nombre', $this->nombre])
            ->andFilterWhere(['like', 'ciu.nombre', $this->ciudad])
            ->andFilterWhere(['like', 'dep.nombre', $this->departamento]);

        return $dataProvider;
    }
}
