<?php

namespace backend\models\search;

use bedezign\yii2\audit\models\AuditTrail;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Moneda represents the model behind the search form about `backend\models\Moneda`.
 */
class AuditEntrySearch extends AuditTrail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['action', 'model', 'user_id', 'model_id', 'field', 'new_value', 'old_value', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AuditTrail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'model', trim($this->model)]);
        $query->andFilterWhere(['like', 'user_id', trim($this->user_id)]);
        $query->andFilterWhere(['like', 'model_id', trim($this->model_id)]);
        $query->andFilterWhere(['like', 'field', trim($this->field)]);
        $query->andFilterWhere(['like', 'new_value', trim($this->new_value)]);
        $query->andFilterWhere(['like', 'old_value', trim($this->old_value)]);
        $query->andFilterWhere(['like', 'action', trim($this->action)]);
        $query->andFilterWhere(['like', 'created', implode('-', array_reverse(explode('-', trim($this->created))))]);

        $query->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }
}
