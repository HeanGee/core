<?php

namespace backend\models\search;

use backend\models\Ciudad;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 */
class CiudadSearch extends Ciudad
{
    public $departamento;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'departamento_id', 'active'], 'integer'],
            [['nombre', 'departamento'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ciudad::find();

        $query->joinWith(['departamento as dep']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['nombre' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['departamento'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['dep.nombre' => SORT_ASC],
            'desc' => ['dep.nombre' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            Ciudad::tableName().'.id' => $this->id,
            'departamento_id' => $this->departamento_id,
            Ciudad::tableName().'.active'=>$this->active <> 3 ? $this->active : '',
        ]);

        $query->andFilterWhere(['like', Ciudad::tableName().'.nombre', $this->nombre])
            ->andFilterWhere(['like', 'dep.nombre', $this->departamento]);

        return $dataProvider;
    }
}
