<?php

namespace backend\models\search;

use backend\models\Moneda;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Moneda represents the model behind the search form about `backend\models\Moneda`.
 */
class MonedaSearch extends Moneda
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['simbolo', 'nombre', 'plural', 'tiene_decimales'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Moneda::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'simbolo', $this->simbolo])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'plural', $this->plural])
            ->andFilterWhere(['like', 'tiene_decimales', $this->tiene_decimales]);

        return $dataProvider;
    }
}
