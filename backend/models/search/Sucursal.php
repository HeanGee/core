<?php

namespace backend\models\search;

use backend\models\Sucursal as SucursalModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 */
class Sucursal extends SucursalModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'departamento_id', 'ciudad_id', 'barrio_id', 'usuario_id'], 'integer'],
            [['nombre', 'direccion', 'telefono'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SucursalModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'departamento_id' => $this->departamento_id,
            'ciudad_id' => $this->ciudad_id,
            'barrio_id' => $this->barrio_id,
            'usuario_id' => $this->usuario_id,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'telefono', $this->telefono]);

        return $dataProvider;
    }
}
