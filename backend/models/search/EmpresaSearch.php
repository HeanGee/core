<?php

namespace backend\models\search;

use backend\models\Empresa;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EmpresaSearch represents the model behind the search form of `app\models\Empresa`.
 */
class EmpresaSearch extends Empresa
{
    public $ciudad_nombre;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ruc', 'digito_verificador', 'departamento_id', 'ciudad_id', 'barrio_id'], 'integer'],
            [['razon_social', 'nombre', 'actividades_comerciales', 'direccion',
                'telefonos', 'coordenadas', 'observaciones', 'ciudad_nombre', 'activo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Empresa::find();
        $query->alias('empresa');
        $query->joinWith(['ciudad']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['razon_social' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['ciudad_nombre'] = [
            'asc' => ['core_ciudad.nombre' => SORT_ASC],
            'desc' => ['core_ciudad.nombre' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->activo = $this->activo == '' ? 1 : ($this->activo == -1 ? '' : $this->activo);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ruc' => $this->ruc,
            'digito_verificador' => $this->digito_verificador,
            'departamento_id' => $this->departamento_id,
            'ciudad_id' => $this->ciudad_id,
            'barrio_localidad_id' => $this->barrio_id,
            'activo' => $this->activo
        ]);

        $query->andFilterWhere(['like', 'empresa.razon_social', $this->razon_social])
            ->andFilterWhere(['like', 'empresa.nombre', $this->nombre])
            ->andFilterWhere(['like', 'empresa.actividades_comerciales', $this->actividades_comerciales])
            ->andFilterWhere(['like', 'empresa.direccion', $this->direccion])
            ->andFilterWhere(['like', 'empresa.telefonos', $this->telefonos])
            ->andFilterWhere(['like', 'empresa.observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'core_ciudad.nombre', $this->ciudad_nombre]);

        return $dataProvider;
    }
}
