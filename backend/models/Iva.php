<?php

namespace backend\models;

/**
 * This is the model class for table "core_iva".
 *
 * @property string $id
 * @property int $porcentaje
 * @property int $estado
 */
class Iva extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_iva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['porcentaje', 'estado'], 'integer'],
            [['porcentaje', 'estado'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'porcentaje' => 'Porcentaje I.V.A.',
            'estado' => 'Estado',
        ];
    }

    public function getPorcentajeTextual()
    {
        return $this->porcentaje . '%';
    }

    public static function getAll()
    {
        $query = Iva::find()->where(['estado' => 1])->orderBy('porcentaje');
        $array = [];
        foreach ($query->all() as $item) {
            if ($item->porcentaje == 0) {
                array_push($array, ['id' => $item->id, 'text' => 'Exenta de IVA']);
            } else {
                array_push($array, ['id' => $item->id, 'text' => 'IVA ' . $item->porcentaje . ' %']);
            }
        }

        return $array;
    }
}
