<?php

namespace backend\models;

/**
 * This is the model class for table "operacion".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $operacion
 *
 * @property RolOperacion[] $rolOperacions
 * @property Rol[] $rols
 */
class Operacion extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_operacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'operacion'], 'required'],
            [['nombre', 'operacion'], 'string', 'max' => 90],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'operacion' => 'Operación'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRolOperacions()
    {
        return $this->hasMany(RolOperacion::className(), ['operacion_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRols()
    {
        return $this->hasMany(Rol::className(), ['id' => 'rol_id'])->viaTable('core_rol_operacion', ['operacion_id' => 'id']);
    }
}
