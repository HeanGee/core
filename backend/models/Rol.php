<?php

namespace backend\models;

use common\models\User;
use Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "rol".
 *
 * @property integer $id
 * @property string $rol_nombre
 * @property integer $rol_valor
 * @property RolOperacion[] $rolOperaciones
 */
class Rol extends BaseModel
{

    public $operaciones;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_rol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rol_nombre', 'rol_valor'], 'required'],
            [['rol_valor'], 'integer'],
            [['rol_nombre'], 'string', 'max' => 45],
            ['operaciones', 'safe'],
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        //Para create
        if (isset($this->operaciones) || sizeof($this->operaciones) > 0) {
            foreach ($this->operaciones as $opId) {
                if (!RolOperacion::find()
                    ->where(['rol_id' => $this->id])
                    ->andWhere(['operacion_id' => $opId])
                    ->exists()) {
                    $rolOperacion = new RolOperacion();
                    $rolOperacion->operacion_id = $opId;
                    $rolOperacion->rol_id = $this->id;
                    $rolOperacion->save();
                }
            }
        } else
            throw new Exception('Operaciones no puede estar vacío.');

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        //Para Update
        if (!$this->isNewRecord) {
            $rolOperacionesBD = $this->rolOperaciones;

            /** @var RolOperacion $rolOperacioneBD */
            foreach ($rolOperacionesBD as $rolOperacioneBD) {
                $encontrado = false;
                if (isset($this->operaciones) || sizeof($this->operaciones) > 0)
                    foreach ($this->operaciones as $opId) {
                        if ($rolOperacioneBD->operacion_id == $opId) {
                            $encontrado = true;
                            break;
                        }
                    }
                if (!$encontrado)
                    $rolOperacioneBD->delete();
            }
        }

        return parent::save($runValidation, $attributeNames);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rol_nombre' => 'Rol Nombre',
            'rol_valor' => 'Rol Valor'
        ];
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['rol_id' => 'id']);
    }

    /**
     * get rol nombre
     *
     */
    public function getRolNombre()
    {
        return $this->rol ? $this->rol->rol_nombre : '- sin rol -';
    }

    /**
     * get lista de roles para lista desplegable
     */
    public static function getRolLista()
    {
        $dropciones = Rol::find()->asArray()->all();
        return ArrayHelper::map($dropciones, 'id', 'rol_nombre');
    }

    public function getRolOperaciones()
    {
        return $this->hasMany(RolOperacion::className(), ['rol_id' => 'id']);
    }

    public function getOperacionesPermitidas()
    {
        return $this->hasMany(Operacion::className(), ['id' => 'operacion_id'])
            ->viaTable('core_rol_operacion', ['rol_id' => 'id']);
    }

    public function getOperacionesPermitidasList()
    {
        return $this->getOperacionesPermitidas()->asArray();
    }

    public function eliminar()
    {
        $operaciones = $this->getRolOperaciones()->all();
        foreach ($operaciones as $op) {
            $op->delete();
        }
        $this->delete();
    }
}
