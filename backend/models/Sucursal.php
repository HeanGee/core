<?php

namespace backend\models;

use common\models\User;
use yii\db\Query;

/**
 * This is the model class for table "core_empresa_sucursal".
 *
 * @property int $id
 * @property string $nombre
 * @property int $departamento_id
 * @property int $ciudad_id
 * @property int $barrio_id
 * @property string $direccion
 * @property string $telefono
 * @property int $usuario_id
 * @property int $empresa_id
 *
 * @property Barrio $barrio
 * @property Ciudad $ciudad
 * @property Departamento $departamento
 * @property User $encargado
 */
class Sucursal extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_empresa_sucursal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'empresa_id', 'departamento_id', 'ciudad_id', 'barrio_id', 'direccion'], 'required'],
            [['departamento_id', 'ciudad_id', 'barrio_id', 'usuario_id', 'empresa_id'], 'integer'],
            [['nombre', 'telefono'], 'string', 'max' => 45],
            [['direccion'], 'string', 'max' => 255],
            [['barrio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Barrio::className(), 'targetAttribute' => ['barrio_id' => 'id']],
            [['ciudad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ciudad::className(), 'targetAttribute' => ['ciudad_id' => 'id']],
            [['departamento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departamento::className(), 'targetAttribute' => ['departamento_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return Sucursal::getAttributesLabelText();
    }

    public static function getAttributesLabelText()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'departamento_id' => 'Departamento',
            'ciudad_id' => 'Ciudad',
            'barrio_id' => 'Barrio',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'usuario_id' => 'Encargado',
            'empresa_id' => 'Empresa'
        ];
    }

//    public function behaviors()
//    {
//        return [
//            'bedezign\yii2\audit\AuditTrailBehavior'
//        ];
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarrio()
    {
        return $this->hasOne(Barrio::className(), ['id' => 'barrio_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudad()
    {
        return $this->hasOne(Ciudad::className(), ['id' => 'ciudad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartamento()
    {
        return $this->hasOne(Departamento::className(), ['id' => 'departamento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEncargado()
    {
        return $this->hasOne(User::className(), ['id' => 'usuario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }


    public static function getSucursalPorEmpresaLista($id = null, $q = null)
    {
        $resultado = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($id)) {
            $consulta = new Query();
            $consulta->select('id, nombre AS text')
                ->from('core_empresa_sucursal')
                ->where(['empresa_id' => $id])
                ->limit(20);

            empty($q) || $consulta->andWhere(['like', 'nombre', $q]);

            $comando = $consulta->createCommand();
            $datos = $comando->queryAll();
            $resultado['results'] = array_values($datos);
        }

        return $resultado;
    }

    public static function manejarTextoAuditoria($campo, &$viejo, &$nuevo)
    {
        switch ($campo) {
            case 'departamento_id':
                $viejo[0] = !empty($viejo[0]) ? Departamento::findOne($viejo[0])->nombre : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Departamento::findOne($nuevo[0])->nombre : $nuevo[0];
                return true;
            case 'ciudad_id':
                $viejo[0] = !empty($viejo[0]) ? Ciudad::findOne($viejo[0])->nombre : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Ciudad::findOne($nuevo[0])->nombre : $nuevo[0];
                return true;
            case 'barrio_id':
                $viejo[0] = !empty($viejo[0]) ? Barrio::findOne($viejo[0])->nombre : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Barrio::findOne($nuevo[0])->nombre : $nuevo[0];
                return true;
            case 'usuario_id':
                $viejo[0] = !empty($viejo[0]) ? User::findOne($viejo[0])->perfil->getNombreCompleto() : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? User::findOne($nuevo[0])->perfil->getNombreCompleto() : $nuevo[0];
                return true;
            case 'empresa_id':
                $viejo[0] = !empty($viejo[0]) ? Empresa::findOne($viejo[0])->razon_social : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Empresa::findOne($nuevo[0])->razon_social : $nuevo[0];
                return true;
            case 'activo':
                $viejo[0] = $viejo[0] != '' ? ($viejo[0] == 1 ? 'Activo' : 'Pasivo') : $viejo[0];
                $nuevo[0] = $nuevo[0] != '' ? ($nuevo[0] == 1 ? 'Activo' : 'Pasivo') : $nuevo[0];
                return true;
        }
        return false;
    }
}
