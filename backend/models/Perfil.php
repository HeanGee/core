<?php

namespace backend\models;

use common\models\User;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "perfil".
 *
 * @property string $id
 * @property string $usuario_id
 * @property string $nombre
 * @property string $apellido
 * @property string $fecha_nacimiento
 * @property integer $genero_id
 * @property string $avatar
 * @property string $created_at
 * @property string $updated_at
 *
 *
 * @property Genero $genero
 * @property User $usuario
 */
class Perfil extends BaseModel
{

    public $imagen_avatar;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_perfil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id', 'genero_id', 'nombre', 'apellido', 'fecha_nacimiento'], 'required'],
            [['usuario_id', 'genero_id'], 'integer'],
            [['nombre', 'apellido'], 'string'],
            [['fecha_nacimiento', 'created_at', 'updated_at'], 'safe'],
            [['genero_id'], 'exist', 'skipOnError' => true, 'targetClass' => Genero::className(), 'targetAttribute' => ['genero_id' => 'id']],
            [['genero_id'], 'in', 'range' => array_keys($this->getGeneroLista())],
            [['imagen_avatar'], 'safe'],
            [['imagen_avatar'], 'file', 'extensions' => 'jpg, gif, png'],
            [['imagen_avatar'], 'file', 'maxSize' => '1048576'],
            [['avatar'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'ID',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'genero_id' => 'Genero',
            'created_at' => 'Creado el',
            'updated_at' => 'Actualizado el',
            'generoNombre' => Yii::t('app', 'Genero'),
            'userLink' => Yii::t('app', 'Usuario'),
            'perfilIdLink' => Yii::t('app', 'Perfil'),
            'avatar' => "Avatar",
        ];
    }

    /**
     * behaviors
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenero()
    {
        return $this->hasOne(Genero::className(), ['id' => 'genero_id']);
    }

    /**
     * @return string
     */
    public function getGeneroNombre()
    {
        return $this->genero->genero_nombre;
    }

    /**
     * get lista de generos para lista desplegable
     */
    public static function getGeneroLista()
    {
        $droptions = Genero::find()->asArray()->all();
        return ArrayHelper::map($droptions, 'id', 'genero_nombre');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'usuario_id']);
    }

    /**
     * @get Username
     */
    public function getUsername()
    {
        return $this->usuario->username;
    }

    /**
     * @getUserId
     */
    public function getUserId()
    {
        return $this->usuario ? $this->usuario->id : 'none';
    }

    /**
     * @getUserLink
     */
    public function getUserLink()
    {
        $url = Url::to(['usuario/view', 'id' => $this->UserId]);
        $opciones = [];
        return Html::a($this->getUserName(), $url, $opciones);
    }

    /**
     * @getProfileLink
     */
    public function getPerfilIdLink()
    {
        $url = Url::to(['perfil/update', 'id' => $this->id]);
        $opciones = [];
        return Html::a($this->id, $url, $opciones);
    }

    public static function carpetaCheck($id)
    {
        $rutaCarpeta = Perfil::getFolderPath($id);
        is_dir($rutaCarpeta) || @mkdir($rutaCarpeta, 0777, true);

        return $rutaCarpeta;
    }

    public static function getFolderPath($id = false)
    {
        return Yii::$app->basePath . '/../uploads/perfiles/' . ($id ? "{$id}/" : '');
    }

    public static function getInitialPreviewImage($id)
    {
        return Url::base() . '/../../uploads/perfiles/' . "{$id}" . '/';
    }

    public function getNombreCompleto()
    {
        $nombre_completo = $this->nombre;
        $nombre_completo .= " {$this->apellido}";
        return $nombre_completo;
    }
}
