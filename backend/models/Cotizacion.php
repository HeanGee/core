<?php

namespace backend\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "core_cotizacion".
 *
 * @property int $id
 * @property int $moneda_id
 * @property string $compra
 * @property string $venta
 * @property string $fecha
 *
 * @property Moneda $moneda
 */
class Cotizacion extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_cotizacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['moneda_id', 'compra', 'venta', 'fecha'], 'required'],
            [['moneda_id'], 'integer'],
            [['fecha'], 'safe'],
            [['moneda_id'], 'exist', 'skipOnError' => true, 'targetClass' => Moneda::className(), 'targetAttribute' => ['moneda_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'moneda_id' => 'Moneda',
            'compra' => 'Compra',
            'venta' => 'Venta',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneda()
    {
        return $this->hasOne(Moneda::className(), ['id' => 'moneda_id']);
    }

    public static function getCotizaciones() {
        $ayer = date('Y-m-d',strtotime("-1 days"));

        $query = Cotizacion::find()->joinWith('moneda as moneda')->select([
            'core_cotizacion.id as id',
            'compra',
            'moneda_id',
            'moneda.id',
            'moneda.nombre',
            'concat((moneda.nombre), (\' - \'), (compra)) as text',
        ])->where(['fecha'=>$ayer]);

        return $query->exists() ? ArrayHelper::map(
            $query->asArray()->all(), 'id', 'text'
        ) : null;
    }
}
