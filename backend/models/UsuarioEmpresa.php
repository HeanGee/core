<?php

namespace backend\models;

use common\models\User;

/**
 * This is the model class for table "core_usuario_empresa".
 *
 * @property string $usuario_id
 * @property string $empresa_id
 *
 * @property Empresa $empresa
 * @property User $usuario
 */
class UsuarioEmpresa extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_usuario_empresa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id', 'empresa_id'], 'required'],
            [['usuario_id', 'empresa_id'], 'integer'],
            [['usuario_id', 'empresa_id'], 'unique', 'targetAttribute' => ['usuario_id', 'empresa_id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'usuario_id' => 'Usuario',
            'empresa_id' => 'Empresa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'usuario_id']);
    }
}
