<?php

namespace backend\models;

/**
 * This is the model class for table "genero".
 *
 * @property integer $id
 * @property string $genero_nombre
 *
 * @property Perfil[] $perfils
 */
class Genero extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_genero';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['genero_nombre'], 'required'],
            [['genero_nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'genero_nombre' => 'Genero',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerfils()
    {
        return $this->hasMany(Perfil::className(), ['genero_id' => 'id']);
    }
}
