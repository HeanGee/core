<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 09/03/2018
 * Time: 8:39
 */

namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'csv, xlsx, xls', 'maxFiles' => 1],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
//            $this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }
}