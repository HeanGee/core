<?php

namespace backend\controllers;

use backend\models\Rol;
use backend\models\search\UserSearch;
use common\helpers\FlashMessageHelpsers;
use common\helpers\PermisosHelpers;
use common\models\CambioPassForm;
use common\models\SignupForm;
use common\models\User;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UsuarioController extends BaseController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'view', 'signup', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'signup', 'view',],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $model = $this->findModel(Yii::$app->request->get('id'));
                            if (Yii::$app->user->identity->username == 'admin')
                                return true;
                            else {
                                if ($model->username == 'admin') return false;
                                elseif (PermisosHelpers::esSuperUsuario()) return true;
                                else {
                                    if ($model->username == Yii::$app->user->identity->username) {
                                        return true;
                                    }
                                    return false;
                                }
                            }
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionCambiarpass($id)
    {

        $model = new CambioPassForm();
        $usuario = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->username = $usuario->username;
            if ($model->cambiarPass()) {
                return $this->redirect(['update', 'id' => $id]);
            }
        }
        return $this->render('cambiar_pass', [
            'model' => $model, 'id' => $usuario->id, 'username' => $usuario->username,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $trans = Yii::$app->db->beginTransaction();
        try {
            $model = $this->findModel($id);

            if ($model->id == Yii::$app->user->id) {
                throw new \Exception("No se puede eliminar al usuario {$model->username} porque es el que se
                 encuentra autenticado en la sesión actualmente.");
            }

            $superUsuario = Rol::findOne(['rol_nombre' => 'SuperUsuario']);
            if ($model->rol->id == $superUsuario->id) {
                throw new \Exception("Está prohibido eliminar a usuarios con el rol 'SuperUsuario'.");
            }

            if (!$model->delete()) {
                throw new \Exception("Error eliminando usuario {$model->username}: {$model->getErrorSummaryAsString()}");
            }

            $trans->commit();
            FlashMessageHelpsers::createSuccessMessage("Usuario {$model->username} eliminado correctamente.");
        } catch (\Exception $exception) {
            $trans->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                return $this->redirect(['index']);
            }
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false;
    }
}
