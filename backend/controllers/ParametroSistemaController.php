<?php

namespace backend\controllers;

use backend\models\SpecialSystemParms;
use common\helpers\FlashMessageHelpsers;
use common\models\ParametroSistema;
use common\models\search\ParametroSistemaSearch;
use Exception;
use Throwable;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ParametroSistemaController implements the CRUD actions for ParametroSistema model.
 */
class ParametroSistemaController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ParametroSistema models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ParametroSistemaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ParametroSistema model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ParametroSistema model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ParametroSistema();

        if ($model->load(Yii::$app->request->post())) {
            try {
                $transaction = Yii::$app->db->beginTransaction();

                // tiene un afterSave
                if (!$model->save())
                    throw new Exception("No se pudo guardar {$model->nombre}: {$model->getErrorSummaryAsString()}");

                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage("\"{$model->nombre}\" creado correctamente.");
                return $this->redirect(['index']);
            } catch (Exception $exception) {
                !isset($transaction) || $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage(), 15000);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ParametroSistema model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            try {
                $transaction = Yii::$app->db->beginTransaction();

                // tiene un afterSave
                if (!$model->save())
                    throw new Exception("No se pudo guardar {$model->nombre}: {$model->getErrorSummaryAsString()}");

                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage("\"{$model->nombre}\" modificado correctamente.");
                return $this->redirect(['index']);
            } catch (Exception $exception) {
                !isset($transaction) || $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage(), 15000);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ParametroSistema model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        try {
            if (!$model->delete()) {
                throw new Exception("No se pudo borrar parametro {$model->nombre}: {$model->getErrorSummaryAsString()}");
            }

            FlashMessageHelpsers::createSuccessMessage("El parametro {$model->nombre} se ha borrado correctamente");;
        } catch (Exception $exception) {
            FlashMessageHelpsers::createErrorMessage($exception->getMessage(), 15000);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ParametroSistema model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ParametroSistema the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ParametroSistema::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionSpecialIndex()
    {
        if (class_exists('\backend\modules\contabilidad\MainModule')) {
            $model = new SpecialSystemParms();
            foreach ($model->getAttributes() as $attribute => $value) {
                /** @var ParametroSistema $parm */
                if (($parm = ParametroSistema::find()->where(['=', 'nombre', "{$attribute}"])->one()) != null) {
                    $model->$attribute = trim($parm->valor);
                }
            }

            if ($model->load(Yii::$app->request->post())) {
                try {
                    $transaction = Yii::$app->db->beginTransaction();

                    foreach ($model->getAttributes() as $attribute => $value) {
                        /** @var ParametroSistema $parm */
                        $attribute = trim($attribute);
                        $value = trim($value);
                        $parm = ParametroSistema::find()->where(['=', 'nombre', "{$attribute}"])->one();

//                        if (round(trim($value)) == 0)
//                            $value = '';
                        if ($value) {
                            Yii::warning("{$attribute}: {$value}");
                            if (!isset($parm))
                                $parm = new ParametroSistema();

                            $parm->nombre = $attribute;
                            $parm->valor = $value;
                            $parm->replicate_to = $model->replicate_to;
                            $parm->replicate_as = $model->replicate_as;
                            // tiene un aftersave
                            if (!$parm->save())
                                throw new Exception("No se pudo guardar parámetro {$parm->nombre}: {$parm->getErrorSummaryAsString()}");
                        } else {
                            if (isset($parm) && !$parm->delete())
                                throw new Exception("Error borrando parámetro {$parm->nombre}: {$parm->getErrorSummaryAsString()}");
                        }
                    }

                    $transaction->commit();
                    FlashMessageHelpsers::createSuccessMessage("Ok");
                    return $this->redirect(['special-index']);
                } catch (Exception $exception) {
                    !isset($transaction) || $transaction->rollBack();
                    FlashMessageHelpsers::createErrorMessage($exception->getMessage(), 15000);
                }
            }

            return $this->render('special-index', [
                'model' => $model,
            ]);
        } else
            Yii::warning("NO existe");

        return $this->redirect(['index']);
    }

    /**
     * @return string|Response
     * @throws Throwable
     */
    public function actionDeleteParametro()
    {
        if (!(Yii::$app->user->identity->getId() == 1)) {
            return $this->redirect(['index']);
        }

        if (Yii::$app->request->isGet) {
            return $this->renderAjax('_modal-delete-parametro', []);
        } else {
            try {
                $postAttribs = $_POST['kvform'];

                if (!isset($postAttribs['parametro']) || $postAttribs['parametro'] == '')
                    FlashMessageHelpsers::createInfoMessage("No se ha especificado ningún parámetro");
                else {
                    $transaction = Yii::$app->db->beginTransaction();
                    $value = trim($postAttribs['parametro']);
                    $exists = false;
                    /** @var ParametroSistema $parm */
                    foreach (ParametroSistema::find()->where(['LIKE', 'nombre', "%{$value}", false])->all() as $parm) {
                        if (!$parm->delete())
                            throw new Exception("No se pudo borrar el parámetro {$parm->nombre}: {$parm->getErrorSummaryAsString()}");
                        $exists || $exists = true;

                    }

                    $transaction->commit();
                    if ($exists)
                        FlashMessageHelpsers::createSuccessMessage("Todos los parámetros que terminan en {$value} se han eliminado correctamente.");
                    else
                        FlashMessageHelpsers::createInfoMessage("NO se ha encontrado ningún parámetro que se parezca a \"{$value}\"");
                }
            } catch (Exception $exception) {
                FlashMessageHelpsers::createErrorMessage($exception->getMessage(), 15000);
                !isset($transaction) || $transaction->rollBack();
            } catch (Throwable $throwable) {
                FlashMessageHelpsers::createErrorMessage($throwable->getMessage(), 15000);
                !isset($transaction) || $transaction->rollBack();
            }
        }

        return $this->redirect(['index']);
    }
}
