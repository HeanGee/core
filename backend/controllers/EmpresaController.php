<?php

namespace backend\controllers;

use backend\models\Empresa;
use backend\models\search\EmpresaSearch;
use backend\models\Sucursal;
use backend\modules\contabilidad\models\CuentaPrestamoSelector;
use backend\modules\contabilidad\models\EmpresaObligacion;
use backend\modules\contabilidad\models\EmpresaRubro;
use backend\modules\contabilidad\models\EmpresaSubObligacion;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\ParametroSistema as ParamContabilidad;
use common\helpers\FlashMessageHelpsers;
use kartik\form\ActiveForm;
use Throwable;
use Yii;
use yii\base\Exception;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * EmpresaController implements the CRUD actions for Empresa model.
 */
class EmpresaController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Empresa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmpresaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        $skey = 'core_empresa_en_edicion';
        $skey_rubros = 'rubros_seleccionados';
        $skey_obligaciones = 'obligaciones_seleccionados';
        $skey_cuentas_prestamo = 'cont_cuentas_prestamo_empresa';
        Yii::$app->session->remove($skey);
        Yii::$app->session->remove($skey_rubros);
        Yii::$app->session->remove($skey_obligaciones);
        Yii::$app->session->remove($skey_cuentas_prestamo);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Empresa model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Empresa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $model = new Empresa();
        $model->permitir_carga_venta_paralelo = 'no';

        $show_tab = 'general';
        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->digito_verificador = Empresa::getDigitoVerificador($model->ruc);

                $es_valido = $model->validate();
                if (!$es_valido) {
                    $req_loc_arr = ['direccion', 'departamento_id', 'ciudad_id', 'telefonos'];
                    $err_arr = $model->getErrors(); /* getError devuelve ordenado en base a las rules() */
                    reset($err_arr);
                    !in_array(key($err_arr), $req_loc_arr) || $show_tab = 'localizacion';
                }
                if ($es_valido) {
                    $model->save(false);

                    $model_sucursal = new Sucursal();
                    $model_sucursal->nombre = 'Matriz';
                    $model_sucursal->departamento_id = $model->departamento_id;
                    $model_sucursal->ciudad_id = $model->ciudad_id;
                    $model_sucursal->barrio_id = $model->barrio_id;
                    $model_sucursal->direccion = $model->direccion;
                    $model_sucursal->telefono = $model->telefonos;
                    $model_sucursal->empresa_id = $model->id;
                    $model_sucursal->save();

                    $exito = true;
                    if (class_exists('\backend\modules\contabilidad\MainModule')) {
                        $model_periodo = new \backend\modules\contabilidad\models\EmpresaPeriodoContable();
                        $model_periodo->empresa_id = $model->id;
                        $model_periodo->anho = $today = date('Y');
                        $model_periodo->activo = 'Si';
                        $model_periodo->descripcion = '';
                        $exito = $model_periodo->save();

                        $model_periodo->refresh();
                        // Parametros automaticos.
                        // Son exactamente los mismos procedimientos que desde EmpresaPeriodoContable excepto que desde
                        // alli se debe controlar duplicados.
                        // REQUISITO FUNDAMENTAL: Las primeras 9 lineas del archivo son parametros sin empresa y periodo

                        // Abrir archivo base de parametros
                        $my_file = Yii::$app->basePath . '/views/empresa/' . 'parametro_contab.sql';
                        $handle = fopen($my_file, 'r');
                        $counter = 0;
                        while (!feof($handle)) {
                            $counter++;
                            $file_line = trim(fgets($handle));
                            $slices = explode(',', $file_line);
                            $nombre = str_replace('\'', '', $slices[0]);
                            $valor = str_replace('\'', '', $slices[1]);
                            $nuevo_nombre = "";

                            if ($counter < 10) {
                                $query = ParamContabilidad::find()->where(['nombre' => $nombre]);
                                if (!$query->exists()) {
                                    $cont_parm = new ParamContabilidad();
                                    $cont_parm->nombre = $nombre;
                                    $cont_parm->valor = $valor;
                                    $cont_parm->save(false);
                                }
                            } else {
                                $pattern = '/(core_empresa)-([0-9]+)-(periodo)-([0-9]+)-([^"]+)/';
                                $result = preg_match($pattern, $nombre, $matches);
                                if ($result && isset($matches)) {
                                    $matches[2] = $model->id;
                                    $matches[4] = $model_periodo->id;
                                    unset($matches[0]);
                                    $nuevo_nombre = implode('-', $matches);
                                } else {
                                    $pattern = '/(core_empresa)-([0-9]+)-([^"]+)/';
                                    $result = preg_match($pattern, $nombre, $matches);
                                    if ($result && isset($matches)) {
                                        $matches[2] = $model->id;
                                        unset($matches[0]);
                                        $nuevo_nombre = implode('-', $matches);
                                    }
                                }
                                if ($nuevo_nombre != '') {
                                    $cont_parm = new ParamContabilidad();
                                    $cont_parm->nombre = $nuevo_nombre;
                                    $cont_parm->valor = $valor;
                                    $cont_parm->save(false);
                                }
                            }
                        }
                    }

                    $transaction->commit();

                    if ($exito) {
                        FlashMessageHelpsers::createSuccessMessage('Empresa actualizada correctamente.');
                    } else {
                        FlashMessageHelpsers::createWarningMessage('Error creando automaticamente periodo contable. Cree manualmente.');
                    }
                    return $this->redirect(['index']);
                }
            } catch (\Exception $exception) {
                $transaction->rollBack();
                throw $exception;
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
            'show_tab' => $show_tab,
        ]);
    }

    /**
     * Updates an existing Empresa model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->cedula != '') $model->cedula_dv = $model->dvCedula;
        else $model->cedula_dv = '';
        $skey = 'core_empresa_en_edicion';
        Yii::$app->session->set($skey, $model->id);

        if (class_exists('\backend\modules\contabilidad\MainModule') && \Yii::$app->session->has('core_empresa_actual') && \Yii::$app->session->has('core_empresa_actual_pc') && $model->id == \Yii::$app->session->get('core_empresa_actual')) {
            if (!Yii::$app->session->has('core_empresa_actual_pc')) {
                FlashMessageHelpsers::createWarningMessage("Necesita establecer primero el periodo contable.");
                return $this->redirect(['index']);
            }

            $skey_prestamo_cuenta = 'cont_cuentas_prestamo_empresa';

            // manejar cuentas para prestamo

            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $param_cont_key = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_prestamo-";

            $parametros = ParametroSistema::find()->where(['like', 'nombre', $param_cont_key])->all();

            if (!empty($parametros) && !Yii::$app->session->has($skey_prestamo_cuenta)) {

                $cuentaPrestamoSelectorArray = [];

                foreach ($parametros as $parametro) {
                    $cuentaPrestamoSelector = new CuentaPrestamoSelector();
                    $cuentaPrestamoSelector->cuenta_id = $parametro->valor;
                    $cuentaPrestamoSelector->concepto = str_replace($param_cont_key, '', $parametro->nombre);
                    $cuentaPrestamoSelectorArray[] = $cuentaPrestamoSelector;
                }

                if (!empty($cuentaPrestamoSelectorArray)) {
                    Yii::$app->session->set($skey_prestamo_cuenta, new ArrayDataProvider([
                        'allModels' => $cuentaPrestamoSelectorArray,
                        'pagination' => false,
                    ]));
                }
            } elseif (empty($parametro) && !Yii::$app->session->has($skey_prestamo_cuenta)) {
                $allModels = [];
                foreach (CuentaPrestamoSelector::conceptosLista() as $concepto => $nombre) {
                    $_model = new CuentaPrestamoSelector();
                    $_model->concepto = $concepto;
                    $allModels[] = $_model;
                }
                Yii::$app->session->set($skey_prestamo_cuenta, new ArrayDataProvider([
                    'allModels' => $allModels,
                    'pagination' => false,
                ]));
            }

            /** Manejar permitividad para carga paralela de factura venta. */
            /** Manejar cuentas a usar para intereses pagados. */
            // factura venta paralela
            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $param_cont_key = "core_empresa-{$id}-periodo-{$periodo_id}-fact_venta_paralelo";
            $permitir_nro_manual = ParamContabilidad::getValorByNombre($param_cont_key);

            $model->permitir_carga_venta_paralelo = $permitir_nro_manual;

            if ($permitir_nro_manual == '')
                $model->permitir_carga_venta_paralelo = 'no';
        }

        $show_tab = 'general';
        if (Yii::$app->request->isGet) {
            $skey_rubros = 'rubros_seleccionados';
            $skey_obligaciones = 'obligaciones_seleccionados';
            Yii::$app->session->remove($skey_rubros);
            Yii::$app->session->remove($skey_obligaciones);

            if (class_exists('\backend\modules\contabilidad\MainModule')) {
                $empresa_obligaciones = EmpresaObligacion::find()
                    ->alias('emObl')
                    ->leftJoin('cont_obligacion ob', 'ob.id = emObl.obligacion_id')
                    ->where(['empresa_id' => $model->id, 'ob.estado' => 'activo'])->all();
                $list = [];
                foreach ($empresa_obligaciones as $concepto) {
                    $list[] = $concepto->obligacion_id;
                }

//                $empresa_sub_obligaciones = EmpresaSubObligacion::find()
//                    ->alias('empSubObl')
//                    ->leftJoin('cont_sub_obligacion subObl', 'subObl.id = empSubObl.sub_obligacion_id')
//                    ->leftJoin('cont_obligacion ob', 'ob.id = subObl.obligacion_id')
//                    ->where(['empresa_id' => $model->id, 'ob.estado' => 'activo'])->all();
//                foreach ($empresa_sub_obligaciones as $concepto2) {
//                    $list[] = "{$concepto2->subObligacion->obligacion_id}_{$concepto2->sub_obligacion_id}";
//                }

                Yii::$app->session->set($skey_obligaciones, $list);

                $empresa_rubro = EmpresaRubro::findAll(['empresa_id' => $model->id]);
                $list = [];
                foreach ($empresa_rubro as $concepto) {
                    $list[] = $concepto->rubro_id;
                }
                Yii::$app->session->set($skey_rubros, $list);
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->digito_verificador = Empresa::getDigitoVerificador($model->ruc);

            $es_valido = $model->validate();

            if (!$es_valido) {
                $req_loc_arr = ['direccion', 'departamento_id', 'ciudad_id', 'telefonos'];
                $err_arr = $model->getErrors(); /* getError devuelve ordenado en base a las rules() */
                reset($err_arr);
                !in_array(key($err_arr), $req_loc_arr) || $show_tab = 'localizacion';
                $msg = implode(', ', $model->getErrorSummary(true));
                FlashMessageHelpsers::createErrorMessage("Ocurrió un error al actualizar: {$msg}");
            } else {
                $trans = Yii::$app->db->beginTransaction();

                try {
                    $model->save(false);

                    if (class_exists('\backend\modules\contabilidad\MainModule')) {

                        // manejar obligaciones y rubros
                        $rubro_ids = [];
                        $obligacion_ids = [];
                        $skey_rubros = 'rubros_seleccionados';
                        $skey_obligaciones = 'obligaciones_seleccionados';

                        if (Yii::$app->session->has($skey_rubros)) {
                            $rubro_ids = Yii::$app->session->get($skey_rubros); // obtener id de rubros seleccionados
                        }
                        if (Yii::$app->session->has($skey_obligaciones)) {
                            $obligacion_ids = Yii::$app->session->get($skey_obligaciones); // obtener id de obligaciones seleccionadas
                        }

                        $rubros_empresa = [];
                        $obligaciones_empresa = [];
                        $sub_obligaciones_empresa = [];
                        $obligacion_padre_ids = [];
                        foreach ($rubro_ids as $rubro_id) {
                            $newRubroEmpresa = new EmpresaRubro();
                            $newRubroEmpresa->rubro_id = $rubro_id;
                            $newRubroEmpresa->empresa_id = $model->id;
                            $rubros_empresa[] = $newRubroEmpresa; // crear nuevos rubros_empresa
                        }
                        foreach ($obligacion_ids as $obligacion_id) {
                            $slices = explode('_', $obligacion_id);

                            if (sizeof($slices) == 1) {
                                $newObligacionEmpresa = new EmpresaObligacion();
                                $newObligacionEmpresa->obligacion_id = $obligacion_id;
                                $newObligacionEmpresa->empresa_id = $model->id;
                                $obligaciones_empresa[] = $newObligacionEmpresa; // crear nuevas obligaciones_empresa
                                $obligacion_padre_ids[] = $newObligacionEmpresa->obligacion_id;
                            } else {
                                $newSubObligacionEmpresa = new EmpresaSubObligacion();
                                $newSubObligacionEmpresa->sub_obligacion_id = $slices[1];
                                $newSubObligacionEmpresa->empresa_id = $model->id;
                                $sub_obligaciones_empresa[] = $newSubObligacionEmpresa; // crear nuevas obligaciones_empresa
                            }
                        }

                        $queryRubro = EmpresaRubro::findAll(['empresa_id' => $id]);
                        $queryObligac = EmpresaObligacion::find()
                            ->alias('emObl')
                            ->leftJoin('cont_obligacion ob', 'ob.id = emObl.obligacion_id')
                            ->where(['empresa_id' => $model->id, 'ob.estado' => 'activo'])->all();  # eliminar solamente obligaciones y rubros activos.
                        $querySubObligac = EmpresaSubObligacion::find()
                            ->alias('empSubObl')
                            ->leftJoin('cont_sub_obligacion subObl', 'subObl.id = empSubObl.sub_obligacion_id')
                            ->leftJoin('cont_obligacion ob', 'ob.id = subObl.obligacion_id')
                            ->where(['empresa_id' => $model->id, 'ob.estado' => 'activo'])->all();  # eliminar solamente obligaciones y rubros activos.

                        foreach ($queryRubro as $item) {
                            if (!$item->delete()) {
                                throw new \Exception("Error borrando rubros anteriores: {$item->getErrorSummary(true)[0]}");
                            }
                        }

                        foreach ($queryObligac as $item) {
                            if (!$item->delete()) {
                                throw new \Exception("Error borrando obligaciones anteriores: {$item->getErrorSummary(true)[0]}");
                            }
                        }

                        foreach ($querySubObligac as $item) {
                            if (!$item->delete()) {
                                throw new \Exception("Error borrando sub-obligaciones anteriores: {$item->getErrorSummary(true)[0]}");
                            }
                        }

                        foreach ($rubros_empresa as $item) {
                            if (!$item->save()) {
                                throw new \Exception("Error guardando rubros: {$item->getErrorSummaryAsString()}");
                            }
                        }

                        foreach ($obligaciones_empresa as $item) {
                            if (!$item->save()) {
                                $attrs = json_encode($item->attributes);
                                throw new \Exception("Error guardando obligaciones: {$item->getErrorSummaryAsString()} {$attrs}");
                            }
                        }

                        foreach ($sub_obligaciones_empresa as $item) {
                            if (!in_array($item->subObligacion->obligacion_id, $obligacion_padre_ids)) {
                                $obligacion_padre_ids[] = $item->subObligacion->obligacion_id;
                                $obligacion_padre = new EmpresaObligacion();
                                $obligacion_padre->empresa_id = $id;
                                $obligacion_padre->obligacion_id = $item->subObligacion->obligacion_id;
                                if (!$obligacion_padre->save()) {
                                    throw new \Exception("Error guardando obligación padre: {$obligacion_padre->getErrorSummaryAsString()}");
                                }
                                $obligacion_padre->refresh();
                            }
                            if (!$item->save()) {
                                throw new \Exception("Error guardando sub-obligaciones: {$item->getErrorSumary(true)[0]}");
                            }
                        }

                        if (\Yii::$app->session->has('core_empresa_actual') && \Yii::$app->session->has('core_empresa_actual_pc') && $model->id == \Yii::$app->session->get('core_empresa_actual')) {
                            // factura venta paralelo
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $param_cont_key = "core_empresa-{$model->id}-periodo-{$periodo_id}-fact_venta_paralelo";
                            $permitir_nro_manual = ParamContabilidad::getValorByNombre($param_cont_key);

                            $cont_parm = new ParametroSistema();
                            if ($permitir_nro_manual == '') {
                                $cont_parm->nombre = $param_cont_key;
                                $cont_parm->valor = $model->permitir_carga_venta_paralelo;
                            } else {
                                $cont_parm = ParamContabilidad::findOne(['nombre' => $param_cont_key]);
                                $cont_parm->valor = $model->permitir_carga_venta_paralelo;
                            }
                            $cont_parm->save(false);

                            // venta no vigente motivo
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $param_cont_key = "core_empresa-{$model->id}-periodo-{$periodo_id}-motivo_no_vigencia_venta";
                            $parmsys = ParametroSistema::findOne(['nombre' => $param_cont_key]);

                            $cont_parm = new ParametroSistema();
                            if (!isset($parmsys)) {
                                $cont_parm->nombre = $param_cont_key;
                                $cont_parm->valor = trim($model->motivo_no_vigencia_venta);
                            } else {
                                $cont_parm = $parmsys; //ParamContabilidad::findOne(['nombre' => $param_cont_key]);
                                $cont_parm->valor = trim($model->motivo_no_vigencia_venta);
                            }
                            $cont_parm->save(false);

                            // manejar cuentas para prestamo

//                            // borrar anteriores
//                            $param_cont_key = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_prestamo-";
//                            $cont_parm = ParamContabilidad::find()->where(['like', 'nombre', $param_cont_key]);
//                            if ($cont_parm->exists()) {
//                                foreach ($cont_parm->all() as $param) {
//                                    $param->delete();
//                                    $param->refresh();
//                                }
//                            }

                            $skey_prestamo_cuenta = 'cont_cuentas_prestamo_empresa';
                            $dataProvider = Yii::$app->session->get($skey_prestamo_cuenta);

                            if (isset($dataProvider)) {

                                /** @var CuentaPrestamoSelector $cuentaPrestamo */
                                foreach ($dataProvider->allModels as $cuentaPrestamo) {
                                    $concepto = $cuentaPrestamo->concepto;
                                    $cuenta_id = $cuentaPrestamo->cuenta_id;

                                    $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                                    $param_cont_key = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_prestamo-{$concepto}";
                                    $cont_parm = ParamContabilidad::findOne(['nombre' => $param_cont_key]);

                                    if ($cont_parm == null) {
                                        $cont_parm = new ParametroSistema();
                                        $cont_parm->nombre = $param_cont_key;
                                        $cont_parm->valor = $cuenta_id;
                                    } else {
                                        $cont_parm->nombre = $param_cont_key;
                                        $cont_parm->valor = $cuenta_id;
                                    }
                                    if (!$cont_parm->save()) {
                                        throw new \Exception("Error validando cuentas de préstamos. Vaya a la pestaña de Contabilidad y verifique que los campos necesarios no estén vacíos.");
                                    }
                                }
                            }

                            // manejar parametro para tipoDocumentoSet de retencion
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-tipodoc_set_retencion";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->tipodoc_set_retencion_id == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->tipodoc_set_retencion_id;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->tipodoc_set_retencion_id;
                                    $parametro_sistema->save(false);
                                }
                            }

                            // manejar parametro para tipo de documento autofactura
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-id_tipo_documento_autofactura";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->id_tipo_documento_autofactura == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->id_tipo_documento_autofactura;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->id_tipo_documento_autofactura;
                                    $parametro_sistema->save(false);
                                }
                            }

                            // manejar parametro para salario minimo
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-salario_minimo";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->salario_minimo == "" || (int)$model->salario_minimo == 0) {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                $salario = str_replace([".", ","], ["", "."], $model->salario_minimo);
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $salario;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $salario;
                                    $parametro_sistema->save(false);
                                }
                            }

                            // manejar parametro para salario minimo para autofat
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cantidad_salario_min";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cantidad_salario_min == "" || (int)$model->cantidad_salario_min == 0) {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cantidad_salario_min;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cantidad_salario_min;
                                    $parametro_sistema->save(false);
                                }
                            }

                            // manejar plantilla para seguros a devengar
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-plantilla_seguro_a_devengar_id";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->plantilla_seguro_a_devengar_id == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->plantilla_seguro_a_devengar_id;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->plantilla_seguro_a_devengar_id;
                                    $parametro_sistema->save(false);
                                }
                            }

                            // manejar cuenta por seguros pagados
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_seguros_pagados_id";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cuenta_seguros_pagados_id == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cuenta_seguros_pagados_id;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_seguros_pagados_id;
                                    $parametro_sistema->save(false);
                                }
                            }

                            // manejar cuenta por resultado por diferencia de cambio como ganancia
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cta_resultado_diff_cambio_debe == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cta_resultado_diff_cambio_debe;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cta_resultado_diff_cambio_debe;
                                    $parametro_sistema->save(false);
                                }
                            }

                            // manejar cuenta por resultado por diferencia de cambio como perdida
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cta_resultado_diff_cambio_haber == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cta_resultado_diff_cambio_haber;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cta_resultado_diff_cambio_haber;
                                    $parametro_sistema->save(false);
                                }
                            }

                            // manejar cuenta por error de redondeos
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_error_redondeo_ganancia";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cuenta_error_redondeo_ganancia == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cuenta_error_redondeo_ganancia;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_error_redondeo_ganancia;
                                    $parametro_sistema->save(false);
                                }
                            }
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_error_redondeo_perdida";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cuenta_error_redondeo_perdida == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cuenta_error_redondeo_perdida;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_error_redondeo_perdida;
                                    $parametro_sistema->save(false);
                                }
                            }

                            // manejar coef costo
                            $model->coeficiente_costo = (float)(str_replace(',', '.', str_replace('.', '', $model->coeficiente_costo)));
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-coeficiente_costo";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            $coef = (float)$model->coeficiente_costo;
                            if ($coef >= 1 || $coef < 0) {
                                $msg = 'Coeficiente de costo debe valer entre 0 y 1 (excluyentes).';
                                $model->addError('coeficiente_costo', $msg);
                                $show_tab = 'periodo_cont';
                                throw new Exception($msg);
                            }
                            if (!isset($parametro_sistema)) {
                                $new_parametro_sistema = new ParamContabilidad();
                                $new_parametro_sistema->nombre = $nombre;
                                $new_parametro_sistema->valor = $model->coeficiente_costo;
                                $new_parametro_sistema->save(false);
                            } else {
                                $parametro_sistema->nombre = $nombre;
                                $parametro_sistema->valor = $model->coeficiente_costo;
                                $parametro_sistema->save(false);
                            }

                            // manejar timbrado de retencion
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-timbrado_para_retenciones";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->timbrado_para_retenciones != '')
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->timbrado_para_retenciones;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->timbrado_para_retenciones;
                                    $parametro_sistema->save(false);
                                }

                            // manejar cuentas de retenciones emitidas/recibidas
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_retenciones_recibidas";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if (!isset($parametro_sistema) && $model->cuenta_retenciones_recibidas != '') {
                                $new_parametro_sistema = new ParamContabilidad();
                                $new_parametro_sistema->nombre = $nombre;
                                $new_parametro_sistema->valor = $model->cuenta_retenciones_recibidas;
                                $new_parametro_sistema->save(false);
                            } else {
                                if ($model->cuenta_retenciones_recibidas == '' && isset($parametro_sistema))
                                    $parametro_sistema->delete();
                                elseif ($model->cuenta_retenciones_recibidas != '' && isset($parametro_sistema)) {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_retenciones_recibidas;
                                    $parametro_sistema->save(false);
                                }
                            }
                            #retenciones emitidas
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_retenciones_emitidas";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if (!isset($parametro_sistema) && $model->cuenta_retenciones_emitidas != '') {
                                $new_parametro_sistema = new ParamContabilidad();
                                $new_parametro_sistema->nombre = $nombre;
                                $new_parametro_sistema->valor = $model->cuenta_retenciones_emitidas;
                                $new_parametro_sistema->save(false);
                            } else {
                                if ($model->cuenta_retenciones_emitidas == '' && isset($parametro_sistema))
                                    $parametro_sistema->delete();
                                elseif ($model->cuenta_retenciones_emitidas != '' && isset($parametro_sistema)) {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_retenciones_emitidas;
                                    $parametro_sistema->save(false);
                                }
                            }
                            #retenciones renta emitidas
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_retenciones_renta_emitidas";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if (!isset($parametro_sistema) && $model->cuenta_retenciones_renta_emitidas != '') {
                                $new_parametro_sistema = new ParamContabilidad();
                                $new_parametro_sistema->nombre = $nombre;
                                $new_parametro_sistema->valor = $model->cuenta_retenciones_renta_emitidas;
                                $new_parametro_sistema->save(false);
                            } else {
                                if ($model->cuenta_retenciones_renta_emitidas == '' && isset($parametro_sistema))
                                    $parametro_sistema->delete();
                                elseif ($model->cuenta_retenciones_renta_emitidas != '' && isset($parametro_sistema)) {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_retenciones_renta_emitidas;
                                    $parametro_sistema->save(false);
                                }
                            }
                            #retenciones renta recibidas
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_retenciones_renta_recibidas";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if (!isset($parametro_sistema) && $model->cuenta_retenciones_renta_recibidas != '') {
                                $new_parametro_sistema = new ParamContabilidad();
                                $new_parametro_sistema->nombre = $nombre;
                                $new_parametro_sistema->valor = $model->cuenta_retenciones_renta_recibidas;
                                $new_parametro_sistema->save(false);
                            } else {
                                if ($model->cuenta_retenciones_renta_recibidas == '' && isset($parametro_sistema))
                                    $parametro_sistema->delete();
                                elseif ($model->cuenta_retenciones_renta_recibidas != '' && isset($parametro_sistema)) {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_retenciones_renta_recibidas;
                                    $parametro_sistema->save(false);
                                }
                            }

                            /** PARA CUENTAS DE CIERRE DE INVENTARIO DE A.BIO. */
                            $nombre = "core_empresa-{$model->id}-cuenta_ganado";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cuenta_ganado == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cuenta_ganado;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_ganado;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-cuenta_costo_venta_gnd";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cuenta_costo_venta_gnd == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cuenta_costo_venta_gnd;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_costo_venta_gnd;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-cuenta_costo_venta_gd";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cuenta_costo_venta_gd == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cuenta_costo_venta_gd;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_costo_venta_gd;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-cuenta_procreo";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cuenta_procreo == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cuenta_procreo;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_procreo;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-cuenta_mortandad_gnd";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cuenta_mortandad_gnd == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cuenta_mortandad_gnd;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_mortandad_gnd;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-cuenta_mortandad_gd";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cuenta_mortandad_gd == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cuenta_mortandad_gd;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_mortandad_gd;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-cuenta_consumo_gnd";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cuenta_consumo_gnd == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cuenta_consumo_gnd;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_consumo_gnd;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-cuenta_consumo_gd";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cuenta_consumo_gd == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cuenta_consumo_gd;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_consumo_gd;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-cuenta_valorizacion_hacienda";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cuenta_valorizacion_hacienda == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cuenta_valorizacion_hacienda;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cuenta_valorizacion_hacienda;
                                    $parametro_sistema->save(false);
                                }
                            }

                            //Para importación
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_retencion_renta_debe";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cta_importacion_retencion_renta_debe == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cta_importacion_retencion_renta_debe;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cta_importacion_retencion_renta_debe;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_retencion_renta_haber";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cta_importacion_retencion_renta_haber == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cta_importacion_retencion_renta_haber;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cta_importacion_retencion_renta_haber;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_retencion_iva_haber";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cta_importacion_retencion_iva_haber == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cta_importacion_retencion_iva_haber;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cta_importacion_retencion_iva_haber;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_retencion_iva_debe";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cta_importacion_retencion_iva_debe == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cta_importacion_retencion_iva_debe;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cta_importacion_retencion_iva_debe;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_gnd_multa";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cta_importacion_gnd_multa == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cta_importacion_gnd_multa;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cta_importacion_gnd_multa;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_mercaderias_importadas";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cta_importacion_mercaderias_importadas == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cta_importacion_mercaderias_importadas;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cta_importacion_mercaderias_importadas;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_iva_10";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cta_importacion_iva_10 == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cta_importacion_iva_10;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cta_importacion_iva_10;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_prov_exterior";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cta_importacion_prov_exterior == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cta_importacion_prov_exterior;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cta_importacion_prov_exterior;
                                    $parametro_sistema->save(false);
                                }
                            }

                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_haber";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->cta_importacion_haber == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->cta_importacion_haber;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->cta_importacion_haber;
                                    $parametro_sistema->save(false);
                                }
                            }
                            //Fin para importación

                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-criterio_revaluo";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                            if ($model->criterio_revaluo == "") {
                                if (isset($parametro_sistema)) {
                                    $parametro_sistema->delete();
                                    $parametro_sistema->refresh();
                                }
                            } else {
                                if (!isset($parametro_sistema)) {
                                    $new_parametro_sistema = new ParamContabilidad();
                                    $new_parametro_sistema->nombre = $nombre;
                                    $new_parametro_sistema->valor = $model->criterio_revaluo;
                                    $new_parametro_sistema->save(false);
                                } else {
                                    $parametro_sistema->nombre = $nombre;
                                    $parametro_sistema->valor = $model->criterio_revaluo;
                                    $parametro_sistema->save(false);
                                }
                            }
                        }
                    }

                    $trans->commit();
                    FlashMessageHelpsers::createSuccessMessage('Empresa modificada correctamente.');
                    return $this->redirect(['index']);

                } catch (\Exception $exception) {
//                    throw $exception;
                    $trans->rollBack();
                    FlashMessageHelpsers::createWarningMessage($exception->getMessage());
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'show_tab' => $show_tab
        ]);
    }

    /**
     * Deletes an existing Empresa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $sucursales = Sucursal::find()->where("empresa_id = '{$id}'")->all();

        if ($sucursales) {
            FlashMessageHelpsers::createErrorMessage("La Empresa \"{$model->razon_social}\" no se puede borrar. Tiene datos asociados.");

        } elseif (class_exists('\backend\modules\contabilidad\MainModule')) {
            $periodos = \backend\modules\contabilidad\models\EmpresaPeriodoContable::find()->where(['empresa_id' => $model->id])->all();
            if ($periodos) {
                FlashMessageHelpsers::createErrorMessage("La Empresa \"{$model->razon_social}\" no se puede borrar. Tiene datos asociados.");
            }
        } else {
            FlashMessageHelpsers::createSuccessMessage('Empresa eliminada exitosamente.');
            $model->delete();
        }


        return $this->redirect(['index']);
    }

    /**
     * Finds the Empresa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Empresa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Empresa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Retorna el Digito Verificador del RUC.
     * @param int $numero
     * @return int
     */
    public function actionGetDigitoVerificador($numero = -1)
    {
        if (Yii::$app->request->isAjax && !($numero = Yii::$app->request->post('numero', null)))
            return -1;

        return $numero >= 0 ? Empresa::getDigitoVerificador($numero) : -1;
    }

    public function actionAddSucursal($empresaId, $submit = false)
    {
        $model = new Sucursal();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->empresa_id = $empresaId;
            if ($model->save()) {
                FlashMessageHelpsers::createSuccessMessage('Sucursal creada correctamente');
                return true;
            } else {
                FlashMessageHelpsers::createErrorMessage('Error en la creación de la Sucursal');
                return false;
            }
        }

        return $this->renderAjax('detalle/create-detalle-sucursal', [
            'model' => $model,
            'empresaId' => $empresaId
        ]);
    }

    public function actionViewSucursal($sucursalId)
    {
        $modelo_sucursal = Sucursal::findOne($sucursalId);
        if ($modelo_sucursal) {
            return $this->renderAjax('detalle/ver-detalle-sucursal', [
                'modelo_sucursal' => $modelo_sucursal
            ]);
        }
    }

    public function actionEditSucursal($sucursalId, $submit = false)
    {
        $modelo_sucursal = Sucursal::findOne($sucursalId);

        if (Yii::$app->request->isAjax && $modelo_sucursal->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($modelo_sucursal);
        }

        if ($modelo_sucursal->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($modelo_sucursal->save()) {
                FlashMessageHelpsers::createSuccessMessage('Sucursal modificada correctamente');
                return true;
            } else {
                FlashMessageHelpsers::createErrorMessage('Error en la modificación de la Sucursal');
                return false;
            }
        }

        return $this->renderAjax('detalle/create-detalle-sucursal', [
            'model' => $modelo_sucursal,
            'empresaId' => $modelo_sucursal->empresa_id
        ]);
    }

    public function actionDeleteSucursal($sucursalId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $modelo_sucursal = Sucursal::findOne($sucursalId);
        if ($modelo_sucursal) {
            $modelo_sucursal->delete();
            FlashMessageHelpsers::createSuccessMessage('Se ha eliminado correctamente la sucursal');
            return true;
        }
        FlashMessageHelpsers::createErrorMessage('No se ha podido eliminar la sucursal');
        return false;
    }

    public function actionGetSucursalLista($q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return Sucursal::getSucursalPorEmpresaLista(\Yii::$app->session->get('core_empresa_actual'), $q);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false;
    }

    public function actionGuardarRubrosEnSession($all = "false", $checked = "false")
    {
        $message = '';
        $skey = 'rubros_seleccionados';
        if ($all === "false") // se selecciona 1 elemento
        {
            if (isset($_POST['id_registro_seleccionado'])) {
                $lista = [];
                if (!Yii::$app->getSession()->has($skey)) // si en la sesion no habia ninguno
                {
                    if ($checked === "true") {
                        array_push($lista, $_POST['id_registro_seleccionado']);
                        $message = "La OBLIGACION seleccionado ha sido agregado a la sesión.";
                    } else {
                        $message = "La OBLIGACION se ha deseleccionado pero no se ha puesto en la session.";
                    }
                } else {
                    $lista = Yii::$app->getSession()->get($skey);
                    if ($checked === "true") {
                        if (!in_array($_POST['id_registro_seleccionado'], $lista)) {
                            array_push($lista, $_POST['id_registro_seleccionado']);
                        }
                        $message = "El rubro seleccionado ha sido agregado a la sesión.";
                    } else {
                        $index = array_search($_POST['id_registro_seleccionado'], $lista);
                        unset($lista[$index]);
                        $message = "El rubro seleccionado ha sido eliminado de la sesión.";
                        if (empty($lista)) {
                            if (Yii::$app->getSession()->has($skey))
                                Yii::$app->getSession()->remove($skey);
                            return $message;
                        }
                    }
                }
                Yii::$app->getSession()->set($skey, $lista);
            }
        } else {
            if (isset($_POST['seleccionados'])) {
                $seleccionados = $_POST['seleccionados'];
                if (!Yii::$app->getSession()->has($skey)) {
                    $lista = [];
                    foreach ($seleccionados as $item) {
                        array_push($lista, $item);
                    }
                } else {
                    $lista = Yii::$app->getSession()->get($skey);
                    foreach ($seleccionados as $item) {
                        if (!in_array($item, $lista)) {
                            array_push($lista, $item);
                        }
                    }
                }
                Yii::$app->getSession()->set($skey, $lista);
                $message = "Los RUBROS seleccionados han sido agregados todos a la sesión.";
            } else {
                Yii::$app->getSession()->remove($skey);
                $message = "Los RUBROS han sido eliminados de la sesión.";
            }
        }

        return $message;
    }

    private function setToSession($obligacion_id)
    {
        $obligations_ID = Yii::$app->session->get($this->obligacionesSeleccionadosSessionK());
        $obligations_ID[] = $obligacion_id;
        Yii::$app->session->set($this->obligacionesSeleccionadosSessionK(), array_unique($obligations_ID));
        $this->removeZero();
    }

    private function removeZero()
    {
        $session_data = Yii::$app->session->get($this->obligacionesSeleccionadosSessionK());
        if (isset($session_data) && in_array('0', $session_data)) {
            $key = array_search('0', $session_data);
            unset($session_data[$key]);
            if (empty($session_data) || is_null($session_data))
                Yii::$app->session->remove($this->obligacionesSeleccionadosSessionK());
            else
                Yii::$app->session->set($this->obligacionesSeleccionadosSessionK(), array_values(array_unique($session_data)));
        }
    }

    private function removeFromSession($obligacion_id)
    {
        $session_data = Yii::$app->session->get($this->obligacionesSeleccionadosSessionK());
        if (isset($session_data) && in_array($obligacion_id, $session_data)) {
            $key = array_search($obligacion_id, $session_data);
            unset($session_data[$key]);
            if (empty($session_data) || is_null($session_data))
                Yii::$app->session->remove($this->obligacionesSeleccionadosSessionK());
            else
                Yii::$app->session->set($this->obligacionesSeleccionadosSessionK(), array_unique($session_data));
            $this->removeZero();
        }
    }

    private function obligacionesSeleccionadosSessionK()
    {
        return 'obligaciones_seleccionados';
    }

    public function actionGuardarObligacionesEnSession($all = "false", $checked = "false")
    {
        if ($all === "false") // se selecciona 1 elemento
        {
            if (isset($_POST['id_registro_seleccionado'])) {
                if ($checked == 'true')
                    $this->setToSession($_POST['id_registro_seleccionado']);
                else
                    $this->removeFromSession($_POST['id_registro_seleccionado']);
            }
        } else {
            if (isset($_POST['seleccionados']))
                Yii::$app->getSession()->set($this->obligacionesSeleccionadosSessionK(), $_POST['seleccionados']);
            else
                Yii::$app->getSession()->remove($this->obligacionesSeleccionadosSessionK());
        }

        return json_encode(Yii::$app->session->get($this->obligacionesSeleccionadosSessionK()));
    }
}
