<?php

namespace backend\controllers;

use backend\models\Empresa;
use backend\modules\administracion\models\ChequeCliente;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\Venta;
use common\helpers\FlashMessageHelpsers;
use common\models\LoginForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends BaseController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
	                    'actions' => ['login', 'error', 'error-target', 'get-stat', 'get-chart-data'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     * @throws Exception
     */
    public function actionIndex()
    {
        //Notificaciones del sistema CONTABILIDAD
        if (\Yii::$app->session->has('core_empresa_actual_pc') &&
            \Yii::$app->session->get('core_empresa_actual_pc') != "" &&
	        class_exists('\backend\modules\contabilidad\MainModule')) {
	        $dataProvider = new ArrayDataProvider([
		        'allModels' => $this->getNotifications(),
		        'pagination' => ['pageSize' => 6],
	        ]);

	        $stadistic = $this->getStadistic(true);
        } else
	        $dataProvider = $stadistic = null;

        //En caso de que no haya empresa se redirecciona al create de la misma
        if (!Empresa::find()->exists()) {
            FlashMessageHelpsers::createWarningMessage('No existe ninguna empresa. Por favor cree una.');
            return $this->redirect(['empresa/create']);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
	        'stadistic' => $stadistic,
        ]);
    }

	public function actionGetStat() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		return $this->getStadistic(Yii::$app->request->queryParams['as_json']);
	}

	/**
	 * @param bool $as_json
     * @param string $_pjax
	 * @return array|\Exception|string
	 */
	private function getStadistic($as_json = false, $_pjax = null) {
		try {
			$periodo_contable = Yii::$app->session->get('core_empresa_actual_pc');
			$empresa = Yii::$app->session->get('core_empresa_actual');
			$month = Yii::$app->request->get('month');
            $today = $month ? date('Y-m-t', strtotime("${month}-05")) : date("Y-m-d");
            $first_day = date("Y-m-01", strtotime($today));
            $prev_month_first_day = date("Y-m-01", strtotime($first_day . "-1 months"));
            $prev_month_last_day = date("Y-m-t", strtotime($first_day . "-1 months"));

            $sql = <<<SQL
select fcic.factura_compra_id,
       nro_factura,
       ruc,
       ce.razon_social                           as razon_social,
       monto,
       porcentaje,
       (monto * porcentaje / (100 + porcentaje)) as imp,
       (monto * 100 / (100 + porcentaje))        as grav,
       total
from cont_factura_compra_iva_cuenta_usada as fcic
         left join cont_iva_cuenta as ivacta on fcic.iva_cta_id = ivacta.id
         left join core_iva ci on ivacta.iva_id = ci.id
         left join cont_factura_compra cfc on fcic.factura_compra_id = cfc.id
         left join cont_entidad ce on cfc.entidad_id = ce.id
where
     cfc.periodo_contable_id = $periodo_contable and cfc.empresa_id = $empresa
     and cfc.fecha_emision between [range]
order by porcentaje asc, cfc.id, cfc.nro_factura
SQL;
			$curr_month_qry = preg_replace('/\[range\]/', "'{$first_day}' and '{$today}'", $sql);
			$prev_month_qry = preg_replace('/\[range\]/', "'{$prev_month_first_day}' and '{$prev_month_last_day}'", $sql);

			$result_curr_month = Yii::$app->db->createCommand($curr_month_qry)->queryAll();
			$result_prev_month = Yii::$app->db->createCommand($prev_month_qry)->queryAll();

			$queryResult = ['curr-month' => $result_curr_month, 'prev-month' => $result_prev_month];

			$result = $as_json ? Json::encode($queryResult) : $queryResult;

			return $result;
		} catch (\Exception $exception) {
			return $exception;
		}
	}

    /**
     * @return string
     * @throws Exception
     */
    public function actionGetChartData() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $periodo_contable = Yii::$app->session->get('core_empresa_actual_pc');
        $empresa = Yii::$app->session->get('core_empresa_actual');
	    $today = date('Y-m-d');
	    $month = explode('-', $today)[1];
	    $year = explode('-', $today)[0];
	    $month_in_text = [
            1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun',
            7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'
        ];
	    $months_to_use = range(1, $month);
	    $result = [];
	    $result['x'] = [];
	    $result['y'] = [];
        $sql_template = <<<SQL
select fcic.factura_compra_id,
       nro_factura,
       ruc,
       ce.razon_social                           as razon_social,
       monto,
       porcentaje,
       (monto * porcentaje / (100 + porcentaje)) as imp,
       (monto * 100 / (100 + porcentaje))        as grav,
       total
from cont_factura_compra_iva_cuenta_usada as fcic
         left join cont_iva_cuenta as ivacta on fcic.iva_cta_id = ivacta.id
         left join core_iva ci on ivacta.iva_id = ci.id
         left join cont_factura_compra cfc on fcic.factura_compra_id = cfc.id
         left join cont_entidad ce on cfc.entidad_id = ce.id
where
     cfc.periodo_contable_id = $periodo_contable and cfc.empresa_id = $empresa
     and cfc.fecha_emision between [range]
order by porcentaje asc, cfc.id, cfc.nro_factura
SQL;
	    foreach ($months_to_use as $month) {
	        $from = "{$year}-{$month}-01";
	        $to = date('Y-m-t', strtotime($from));

	        $sql = preg_replace('/\[range\]/', "'{$from}' and '{$to}'", $sql_template);
            $resultSet = Yii::$app->db->createCommand($sql)->queryAll();

            array_push($result['x'], $month_in_text[$month]);
            array_push($result['y'], $this->get_gasto($resultSet));
        }

	    return Json::encode($result);
    }

    private function get_gasto($resultSet) {
        $monto = 0.0;
        foreach ($resultSet as $row) {
            $monto += floatval($row['monto']);
        }
        return $monto;
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getNotifications()
    {
        $modelos = array('venta' => 'cont_factura_venta', 'compra' => 'cont_factura_compra');
        $x = 7; //number of days
        $ahora = time();
        $past_stamp = $ahora - $x * 24 * 60 * 60;
        $future_stamp = $ahora + $x * 24 * 60 * 60;
        $hoy = date('Y-m-d', $ahora);
        $desde = date('Y-m-d', $past_stamp);
        $hasta = date('Y-m-d', $future_stamp);

        $resultado = [];

        foreach ($modelos as $key => $value) {
//            $consulta = (new Query())
//                ->select(['id AS id', 'fecha_vencimiento AS vencimiento',
//                    new Expression("'$key' AS tipo"),
//                    new Expression("IF(fecha_vencimiento>= $hoy, 'vencido', 'por vencer') AS estado"),
//                    new Expression("CONCAT('factura: ', nro_factura ) AS detalle")
//
//                ])
//                ->from($value)
//                ->where(['empresa_id' => \Yii::$app->session->get('core_empresa_actual_pc')])
//                ->andWhere(['>=', 'fecha_vencimiento', $desde])
//                ->andWhere(['<=', 'fecha_vencimiento', $hasta])
//                ->indexBy('id')
//                ->createCommand()
//                ->queryAll();
//            $resultado = array_merge($resultado, array_values($consulta));

            $query = ($key == 'compra') ? Compra::find() : Venta::find();
            $query->alias('factura');
	        $query->leftJoin('cont_entidad as entidad', 'factura.entidad_id = entidad.id');
            $tipo = ucfirst($key);
            $modelos = $query->select([
                'factura.id as id',
                'factura.fecha_vencimiento as fecha_vencimiento',
                'factura.estado as estado',
                new Expression("'$tipo' AS tipo"),
                new Expression("IF(fecha_vencimiento <= '$hoy', 'Vencido', 'Por vencer') AS estado"),
	            new Expression(($key == 'compra') ? "CONCAT('Factura: ', nro_factura) AS nro_factura" : "CONCAT('Factura: ',factura.prefijo, ('-'), factura.nro_factura) as nro_factura"),
	            'entidad.razon_social as razon_social',
	            'factura.entidad_id as entidad_id',
	            'factura.saldo as saldo',
            ])
                ->filterWhere(['factura.empresa_id' => Yii::$app->session->get('core_empresa_actual')])
                ->andFilterWhere(['>=', 'factura.fecha_vencimiento', $desde])
                ->andFilterWhere(['<=', 'factura.fecha_vencimiento', $hasta])
	            ->andFilterWhere(['!=', 'factura.saldo', 0])
                ->all();
            $resultado = array_merge($resultado, array_values($modelos));
        }

        return $resultado;

    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     * @return array
     */
    public function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionErrorTarget()
    {
        return $this->render('error_target');
    }
}
