<?php

namespace backend\controllers;

use backend\models\Empresa;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use kartik\form\ActiveForm;
use Yii;
use yii\base\Model;
use yii\filters\VerbFilter;
use yii\web\Response;

class SessionController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionSetEmpresaActual($submit = false)
    {
        $model = new EmpresaSession();
        $session = Yii::$app->session;

        //verifica si ya está asociada una empresa
        if (isset($session['core_empresa_actual'])) {
            $model->empresa_id = $session['core_empresa_actual'];
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            // Establecer empresa actual.
            $session->set('core_empresa_actual', $model->empresa_id);

            // Establecer periodo contable actual de la empresa actual.
            if (class_exists('\backend\modules\contabilidad\MainModule')) {
                $periodoModel = EmpresaPeriodoContable::findOne(['id' => $model->periodo_id]);
                if (isset($periodoModel) && $periodoModel->empresa_id != $model->empresa_id) {
                    $session->remove('core_empresa_actual');
                    $session->remove('core_empresa_actual_pc');
                } elseif (isset($periodoModel) && $periodoModel->empresa_id == $model->empresa_id) {
                    $session->set('core_empresa_actual_pc', $model->periodo_id);
                } else {
                    $session->remove('core_empresa_actual');
                }
            }
        }

        $sessionVariablesToRemove = array();
        //Administracion
        if (class_exists('\backend\modules\administracion\MainModule')) {
            $variablesAdministracion = \backend\modules\administracion\MainModule::getSessionVariablesDepEmpresa();
            if (!empty($variablesAdministracion)) {
                $sessionVariablesToRemove = array_merge($sessionVariablesToRemove, $variablesAdministracion);
            }
        }
        //Contabilidad
        if (class_exists('\backend\modules\contabilidad\MainModule')) {
            $variablesContabilidad = \backend\modules\contabilidad\MainModule::getSessionVariablesDepEmpresa();
            if (!empty($variablesContabilidad)) {
                $sessionVariablesToRemove = array_merge($sessionVariablesToRemove, $variablesContabilidad);
            }
        }

        foreach ($sessionVariablesToRemove as $variable) {
            Yii::$app->getSession()->remove($variable);
        }

        return $this->renderAjax('add-empresa-actual', [
            'model' => $model,
        ]);
    }


    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false;
    }
}

/**
 * @property int $empresa_id
 * @property int $periodo_id
 */
class EmpresaSession extends Model
{
    public $empresa_id;
    public $periodo_id;

    public function rules()
    {
        return [
            [['empresa_id', 'periodo_id'], 'required'],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['periodo_id'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'empresa_id' => Yii::t('app', 'Empresa'),
            'periodo_id' => Yii::t('app', 'Periodo Contable'),
        ];
    }

}
