<?php

namespace backend\controllers;

use backend\models\Operacion;
use backend\models\Rol;
use backend\models\search\RolSearch;
use common\helpers\FlashMessageHelpsers;
use common\helpers\PermisosHelpers;
use ErrorException;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * RolController implements the CRUD actions for Rol model.
 */
class RolController extends BaseController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'view',],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return PermisosHelpers::requerirMinimoRol('Admin') && PermisosHelpers::requerirEstado('Activo');
                        }
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return PermisosHelpers::requerirMinimoRol('SuperUsuario') && PermisosHelpers::requerirEstado('Activo');
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rol models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RolSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rol model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rol model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionCreate()
    {
        $model = new Rol();
        $model->rol_valor = 1;
        $tipoOperaciones = Operacion::find()->all();

        $array = [];
        foreach ($tipoOperaciones as $op) {
            array_push($array, ['id' => $op->id, 'text' => $op->nombre]);
        }
        $json_pick_left = json_encode($array);
        $json_pick_right = json_encode([]);

        if ($model->load(Yii::$app->request->post())) {

            //guardar relaciones con operacion
            try {
                $model->operaciones = $_POST['selected'];
            } catch (ErrorException $e) {
                FlashMessageHelpsers::createWarningMessage('Debe seleccionar por lo menos una operación!');
                goto retorno;
            }

            $model->save();

            FlashMessageHelpsers::createSuccessMessage('Rol creado de forma exitosa.');
            return $this->redirect(['index']);
        }

        retorno:;
        return $this->render('create', [
            'model' => $model,
            'json_pick_left' => $json_pick_left,
            'json_pick_right' => $json_pick_right
        ]);
    }

    /**
     * Updates an existing Rol model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tipoOperaciones = Operacion::find()->all();
        $model->operaciones = ArrayHelper::getColumn(
            $model->getRolOperaciones()->asArray()->all(), 'operacion_id'
        );

        $array_left = [];
        $array_right = [];
        foreach ($tipoOperaciones as $op) {
            if (!in_array($op->id, $model->operaciones)) {
                array_push($array_left, ['id' => $op->id, 'text' => $op->nombre]);
            } else {
                array_push($array_right, ['id' => $op->id, 'text' => $op->nombre]);
            }
        }
        $json_pick_left = json_encode($array_left);
        $json_pick_right = json_encode($array_right);

        if ($model->load(Yii::$app->request->post())) {
            try {
                $model->operaciones = $_POST['selected'];
            } catch (ErrorException $e) {
                FlashMessageHelpsers::createWarningMessage('Debe seleccionar por lo menos una operación!');
                goto retorno;
            }

            $model->save();

            FlashMessageHelpsers::createSuccessMessage('Rol actualizado de forma exitosa.');
            return $this->redirect(['index']);
        }

        retorno:;
        return $this->render('update', [
            'model' => $model,
            'json_pick_left' => $json_pick_left,
            'json_pick_right' => $json_pick_right
        ]);
    }

    /**
     * Deletes an existing Rol model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->eliminar();

        FlashMessageHelpsers::createSuccessMessage('Rol borrado de forma exitosa.');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Rol model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rol the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rol::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false;
    }
}
