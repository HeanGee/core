<?php

namespace backend\controllers;

use backend\models\Empresa;
use backend\models\search\UserSearch;
use backend\models\search\UsuarioEmpresa;
use common\models\User;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\base\ErrorException;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * UserEmpresaController implements the CRUD actions for UserEmpresa model.
 */
class UsuarioEmpresaController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserEmpresa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserEmpresa model.
     * @param string $usuario_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($usuario_id)
    {
        return $this->render('view', [
            'model' => User::find()->where(['id' => $usuario_id])->one(),
        ]);
    }

    /**
     * Creates a new UserEmpresa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate($usuario_id)
    {
        $model = User::findOne($usuario_id);
        $empresas_all = Empresa::find()->where(['activo' => 1])->select(['id', 'concat(nombre, " - ", ruc) as nombre'])->all();
        $user_empresas = $model->getUserEmpresas()->all();
        $array_left = [];
        $array_right = [];

        foreach ($empresas_all as $emp) {
            $flag = false;
            foreach ($user_empresas as $emp_prop) {
                if ($emp->id === $emp_prop->empresa->id) {
                    $flag = true;
                }
            }
            if ($flag) { // esta relacionado con el usuario
                array_push($array_right, ['id' => $emp->id, 'text' => $emp->nombre]);
            } else { // no esta relacionado con el usuario
                array_push($array_left, ['id' => $emp->id, 'text' => $emp->nombre]);
            }
        }
        $json_pick_left = json_encode($array_left);
        $json_pick_right = json_encode($array_right);

        if ($model->load(Yii::$app->request->post())) {
            //guardar relaciones con empresa
            try {
                $model->empresas = $_POST['selected'];
            } catch (ErrorException $e) {
                $model->empresas = [];
            }

            $t = UsuarioEmpresa::getDb()->beginTransaction();
            try {
                if (!$model->save())
                    throw new Exception('');

                //Guardamos relaciones con empresa
                UsuarioEmpresa::getDb()
                    ->createCommand()
                    ->delete('core_usuario_empresa', 'usuario_id = ' . (int)$model->id)
                    ->execute();

                if (isset($model->empresas)) {
                    foreach ($model->empresas as $id) {
                        $ue = new UsuarioEmpresa();
                        $ue->usuario_id = $model->id;
                        $ue->empresa_id = $id;
                        if (!$ue->save())
                            throw new Exception('');

                    }
                }
                FlashMessageHelpsers::createSuccessMessage('Asociación creada de forma exitosa.');
                $t->commit();
            } catch (\Exception $e) {
                FlashMessageHelpsers::createErrorMessage('No se puede guardar.');
                $t->rollBack();
            }

            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'json_pick_left' => $json_pick_left,
            'json_pick_right' => $json_pick_right
        ]);
    }

    /**
     * Updates an existing UserEmpresa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $usuario_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($usuario_id)
    {
        $url = Url::to(['create', 'usuario_id' => $usuario_id]);
        return $this->redirect($url);
    }

    /**
     * Deletes an existing UserEmpresa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $usuario_id
     * @param string $empresa_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($usuario_id, $empresa_id)
    {
        $this->findModel($usuario_id, $empresa_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserEmpresa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $usuario_id
     * @param string $empresa_id
     * @return UsuarioEmpresa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($usuario_id, $empresa_id)
    {
        if (($model = UsuarioEmpresa::findOne(['usuario_id' => $usuario_id, 'empresa_id' => $empresa_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false;
    }
}
