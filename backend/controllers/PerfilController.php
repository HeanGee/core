<?php

namespace backend\controllers;

use common\models\CambioPassForm;
use backend\models\Perfil;
use common\models\User;
use common\helpers\FlashMessageHelpsers;
use common\helpers\PermisosHelpers;
use common\helpers\RegistrosHelpers;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * PerfilController implements the CRUD actions for Perfil model.
 */
class PerfilController extends BaseController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'view',],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return PermisosHelpers::requerirMinimoRol('Admin') && PermisosHelpers::requerirEstado('Activo');
                        }
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return PermisosHelpers::requerirMinimoRol('SuperUsuario') && PermisosHelpers::requerirEstado('Activo');
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Perfil models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $searchModel = new PerfilSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
    }

    /**
     * Displays a single Perfil model.
     * @param string $id
     * @return mixed
     */
    public function actionView()
    {
        if ($ya_existe = RegistrosHelpers::userTiene('core_perfil')) {
            return $this->render('view', [
                'model' => $this->findModel($ya_existe),
            ]);
        } else {
            return $this->redirect(['create']);
        }
    }

    /**
     * Creates a new Perfil model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Perfil;
        $model->usuario_id = \Yii::$app->user->identity->id;
        if ($ya_existe = RegistrosHelpers::userTiene('core_perfil')) {
            return $this->redirect(['update', 'model' => $this->findModel($ya_existe)]);
        } elseif ($model->load(Yii::$app->request->post())) {
            if ($image = UploadedFile::getInstance($model, 'imagen_avatar')) {
                $model->avatar = Yii::$app->security->generateRandomString() . '.' . $image->extension;
            }

            if ($model->save()) {
                $model->refresh();
                $rutaCarpeta = Perfil::carpetaCheck($model->id);
                if (!empty($model->avatar) && !$image->saveAs($rutaCarpeta . $model->avatar)) {
                    $model->avatar = null;
                    $model->save();
                }

                Yii::$app->user->logout();
                //TODO enviar aviso de reinicio de sesion
                return $this->goHome();
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Perfil model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate()
    {

        if ($model = Perfil::find()->where(['usuario_id' =>
            Yii::$app->user->identity->id])->one()
        ) {
            $avatarTemp = $model->avatar;
            if ($model->load(Yii::$app->request->post())) {
                if ($image = UploadedFile::getInstance($model, 'imagen_avatar')) {
                    $model->avatar = Yii::$app->security->generateRandomString() . '.' . $image->extension;
                }
                if ($model->save()) {
                    $model->refresh();
                    $rutaCarpeta = Perfil::carpetaCheck($model->id);
                    if ($image) {
                        $image->saveAs($rutaCarpeta . $model->avatar);
                    }
                    if (empty($model->avatar)) {
                        $model->avatar = null;
                        $model->save();
                    }

                    //eliminar imagen vieja
                    if ($image != null && !empty($avatarTemp) && file_exists($rutaCarpeta . $avatarTemp)) {
                        unlink($rutaCarpeta . $avatarTemp);
                    }

                    Yii::$app->user->logout();
                    //TODO enviar aviso de reinicio de sesion
                    return $this->goHome();
                }
            }
            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect(['perfil/create']);
        }
    }

    /**
     * Deletes an existing Perfil model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        FlashMessageHelpsers::createSuccessMessage('El perfil a sido borrado.');
        return $this->redirect('index.php');
    }

    /**
     * Finds the Perfil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Perfil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Perfil::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCambiarpass($id)
    {
        $model = new CambioPassForm();
        $usuario = User::findIdentity($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->username = $usuario->username;
            if ($model->cambiarPass(false)) {
                FlashMessageHelpsers::createSuccessMessage('Contraseña cambiada exitosamente.');
                return $this->redirect(['update', 'id' => $id]);
            }
        }
        return $this->render('cambiar_pass', [
            'model' => $model, 'id' => $usuario->id, 'username' => $usuario->username,
        ]);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false;
    }
}
