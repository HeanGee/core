<?php

namespace backend\controllers;

use backend\models\Operacion;
use backend\models\search\OperacionSearch;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * OperacionController implements the CRUD actions for Operacion model.
 */
class OperacionController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    private function concepts()
    {
        $concepts = [];
        $concepts ['todos'] = "Todos";
        if (class_exists('\backend\modules\contabilidad\MainModule')) {
            $concepts ["^contabilidad"] = "Módulo de Contabilidad";

        }
        if (class_exists('\backend\modules\administracion\MainModule')) {
            $concepts ["^administracion||regex"] = "Módulo de Administración";
        }
        $concepts ["^barrio|^departamento|^ciudad||regex"] = "Ubicaciones";

        return $concepts;
    }

    /**
     * Lists all Operacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OperacionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'concepts' => self::concepts(),
        ]);
    }

    /**
     * Displays a single Operacion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Operacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Operacion();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Operacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Operacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Operacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Operacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Operacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false;
    }

    private static function getCoreOperations()
    {
        $controllerlist = [];
        if ($handle = opendir('../controllers')) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != ".." && substr($file, strrpos($file, '.') - 10) == 'Controller.php') {
                    $controllerlist[] = $file;
                }
            }
            closedir($handle);
        }
        asort($controllerlist);
        $operationsList = [];
        foreach ($controllerlist as $controller):
            $handle = fopen('../controllers/' . $controller, "r");
            if ($handle) {
                preg_match('/([a-z|A-Z]+)Controller.php/', $controller, $matches);
                $prefijo = strtolower($matches[1]);
                while (($line = fgets($handle)) !== false) {
                    if (preg_match('/public function action(.*?)\(/', $line, $display)):
                        if (strlen($display[1]) > 2):
                            $slices = preg_split('/(?=[A-Z])/', $display[1]);
                            foreach ($slices as $index => $value) {
                                $slices[$index] = strtolower($value);
                            }
                            $sufijo = implode('-', $slices);
                            $operationsList[] = "{$prefijo}{$sufijo}";
                        endif;
                    endif;
                }
            }
            fclose($handle);
        endforeach;

        return $operationsList;
    }

    private static function getModulesOperations()
    {
        $operationsList = [];
        if ($handle = opendir('../modules')) {
            while (false !== ($submodule = readdir($handle))) {
                if ($submodule != '.' && $submodule != '..') {
                    if ($moduleHandler = opendir("../modules/{$submodule}")) {
                        while (false !== ($folder = readdir($moduleHandler))) {
                            if ($folder != '.' && $folder != '..' && $folder == 'controllers') {
                                if ($controllerFolderHandle = opendir("../modules/{$submodule}/{$folder}")) {
                                    while (false !== ($controllerFile = readdir($controllerFolderHandle))) {
                                        if ($controllerFile != "." && $controllerFile != ".." && substr($controllerFile, strrpos($controllerFile, '.') - 10) == 'Controller.php') {
                                            $fileHandle = fopen("../modules/{$submodule}/{$folder}/{$controllerFile}", "r");
                                            if ($fileHandle) {
                                                preg_match('/([a-z|A-Z]+)Controller.php/', $controllerFile, $matches);
                                                $controllerName = $matches[1];
                                                while (($line = fgets($fileHandle)) !== false) {
                                                    if (preg_match('/public function action(.*?)\(/', $line, $display)):
                                                        if (strlen($display[1]) > 2):
                                                            $slices = preg_split('/(?=[A-Z])/', $display[1], null, PREG_SPLIT_NO_EMPTY);
                                                            foreach ($slices as $index => $value) {
                                                                $slices[$index] = strtolower($value);
//                                                                echo '|'.$slices[$index].'|<br/>';
                                                            }
//                                                            echo '....<br/>';
                                                            $actionNameParsed = implode('-', $slices);
                                                            $slices = preg_split('/(?=[A-Z])/', $controllerName, null, PREG_SPLIT_NO_EMPTY);
                                                            foreach ($slices as $index => $value) {
                                                                $slices[$index] = strtolower($value);
                                                            }
                                                            $controllerNameParsed = implode('-', $slices);
                                                            $operation = "{$submodule}-{$controllerNameParsed}-{$actionNameParsed}";
                                                            $operationsList[] = $operation;
                                                        endif;
                                                    endif;
                                                }
                                            }
                                            fclose($fileHandle);
                                        }
                                    }
                                    closedir($controllerFolderHandle);
                                }
                            }
                        }
                        closedir($moduleHandler);
                    }
                }
            }
            closedir($handle);
        }

        return $operationsList;
    }

    public function actionRegisterActions()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $operations = [];
            $opQuery = Operacion::find()->all();
            foreach ($opQuery as $item) {
                $operations[] = $item->nombre;
            }

            $coreOperations = self::getCoreOperations();
            $newOperationsCore = array_diff($coreOperations, $operations);
            sort($newOperationsCore);

            $modulesOperations = self::getModulesOperations();
            $newOperationsModules = array_diff($modulesOperations, $operations);
            sort($newOperationsModules);

            $saved = false;
            foreach ($newOperationsCore as $operation) {
                $model = new Operacion();
                $model->nombre = $operation;
                $model->operacion = $operation;
                if (!$model->save()) {
                    throw new Exception("Error validando operación del core: {$model->getErrorSummaryAsString()}");
                }
                $saved = true;
            }

            foreach ($newOperationsModules as $operation) {
                $model = new Operacion();
                $model->nombre = $operation;
                $model->operacion = $operation;
                if (!$model->save()) {
                    throw new Exception("Error validando operación de módulos: {$model->getErrorSummaryAsString()}");
                }
                $saved = true;
            }

            $transaction->commit();
            if ($saved)
                FlashMessageHelpsers::createSuccessMessage("Las operaciones se guardaron correctamente.");
            else
                FlashMessageHelpsers::createInfoMessage("No hubieron nuevas operaciones.");

        } catch (Exception $exception) {
            $transaction->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $this->redirect(['index']);
    }
}
