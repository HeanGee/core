<?php

namespace backend\controllers;

use backend\models\CotizacionArchivo;
use backend\models\Moneda;
use common\helpers\FlashMessageHelpsers;
use common\helpers\ValorHelpers;
use Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use backend\models\Cotizacion;
use backend\models\search\CotizacionSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CotizacionController implements the CRUD actions for Cotizacion model.
 */
class CotizacionController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cotizacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CotizacionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = ['defaultOrder' => ['fecha' => SORT_DESC]];
        $dataProvider->pagination->pageSize = 12;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cotizacion model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Cotizacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cotizacion();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->compra = $this->formatPostToFloat($model->compra);
            $model->venta = $this->formatPostToFloat($model->venta);

            // Verificamos que el modelo no exista en base de datos. En caso de que exista, hay que actualizar
            /** @var Cotizacion $cotizacion */
            $cotizacion = Cotizacion::find()->where(['fecha' => $model->fecha])->andWhere(['moneda_id' => $model->moneda_id])->one();

            if ($cotizacion) { // Actualizamos la cotización
                $cotizacion->compra = $model->compra;
                $cotizacion->venta = $model->venta;
                $cotizacion->save();
            } else { // Se crea uno nuevo
                $model->save();
            }

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    private function formatPostToFloat($number)
    {
        //Se convierte a una manera que maneje float
        if (strpos($number, '.') && strpos($number, ',')) {
            $number = str_replace(',', '.', str_replace('.', '', $number));
        } elseif (strpos($number, '.') && !strpos($number, ',')) {
            $number = str_replace('.', '', $number);
        } elseif (!strpos($number, '.') && strpos($number, ',')) {
            $number = str_replace(',', '.', $number);
        }
        return $number;
    }

    /**
     * Updates an existing Cotizacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->compra = $this->formatPostToFloat($model->compra);
            $model->venta = $this->formatPostToFloat($model->venta);
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cotizacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionChequeoExistencia($fecha, $moneda_id)
    {

        // Se verifica la existencia de una cotización en la {$fecha y $moneda}
        /** @var Cotizacion $cotizacion */
        $cotizacion = Cotizacion::find()->where(['fecha' => $fecha])->andWhere(['moneda_id' => $moneda_id])->one();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'message' => 'Éxito',
            'data' => $cotizacion,
            'code' => 200,
        ];
    }

    /**
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionCreatefile()
    {
        $model = new CotizacionArchivo();
        $mensaje = "";
        ini_set('memory_limit', '-1');
        if ($model->load(Yii::$app->request->post())) { // si ya se envio algun dato
            //Validar fecha
            if (!preg_match('/^\d{2}-\d{4}$/', trim($model->fecha))) {
                $mensaje = "<br /><br />Error: <br /> Formato de fecha inválido.";
                goto retorno;
            }

            //Separamos el mes y el año
            $fecha = explode('-', $model->fecha);
            $mes = $fecha[0];
            $anho = $fecha[1];

            $model->archivo = UploadedFile::getInstance($model, 'archivo');
            $archivo = file($model->archivo->tempName);
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $cotization = Cotizacion::find()->where(['>', 'fecha', "$anho-$mes-01"])->andFilterWhere(['<', 'fecha', "$anho-$mes-31"]);
                if ($cotization->exists()) {
                    foreach ($cotization->all() as $a_prev_cot) {
                        $a_prev_cot->delete();
                    }
                }
                $objPHPExcel = IOFactory::load($model->archivo->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $monedas = [];

                foreach ($sheetData as $rownro => $row) {
                    if ($rownro == 1) { // Cabecera DOLAR/REAL/YEN/EURO/LIBRA/PESO ARG.
                        if (empty($row['B']) or empty($row['D']) or empty($row['F']) or empty($row['H']) or empty($row['J']) or empty($row['L'])) { //Error
                            $mensaje = "<br /><br />Error: <br /> Archvivo incorrecto.";
                            goto retorno;
                        }
                        if (trim(strtoupper($row['B'])) != 'DÓLAR' or trim(strtoupper($row['D'])) != 'REAL' or
                            trim(strtoupper($row['F'])) != 'PESO ARG.' or trim(strtoupper($row['H'])) != 'YEN' or
                            trim(strtoupper($row['J'])) != 'EURO' or trim(strtoupper($row['L'])) != 'LIBRA') { //Error
                            $mensaje = "<br /><br />Error: <br /> Archivo incorrecto.";
                            goto retorno;
                        }

                        //verificamos que existan todas las monedas
                        //Dolar
                        $dolar = Moneda::find()->where(['UPPER(nombre)' => 'DÓLAR'])->one();
                        if ($dolar == null) {
                            $moneda = new Moneda();
                            $moneda->nombre = 'DÓLAR';
                            $moneda->plural = 'DÓLARES';
                            $moneda->simbolo = '$';
                            $moneda->tiene_decimales = 'si';
                            $moneda->save();
                            $monedas['dolar'] = $moneda->id;
                        } else {
                            $monedas['dolar'] = $dolar->id;
                        }
                        //Real
                        $real = Moneda::find()->where(['UPPER(nombre)' => 'REAL'])->one();
                        if ($real == null) {
                            $moneda = new Moneda();
                            $moneda->nombre = 'REAL';
                            $moneda->plural = 'REALES';
                            $moneda->simbolo = 'R$';
                            $moneda->tiene_decimales = 'si';
                            $moneda->save();
                            $monedas['real'] = $moneda->id;
                        } else {
                            $monedas['real'] = $real->id;
                        }
                        //Peso Arg.
                        $peso_arg = Moneda::find()->where(['UPPER(nombre)' => 'PESO ARG.'])->one();
                        if ($peso_arg == null) {
                            $moneda = new Moneda();
                            $moneda->nombre = 'PESO ARG.';
                            $moneda->plural = 'PESOS ARG.';
                            $moneda->simbolo = '$';
                            $moneda->tiene_decimales = 'si';
                            $moneda->save();
                            $monedas['peso_arg'] = $moneda->id;
                        } else {
                            $monedas['peso_arg'] = $peso_arg->id;
                        }
                        //Yen
                        $yen = Moneda::find()->where(['UPPER(nombre)' => 'YEN'])->one();
                        if ($yen == null) {
                            $moneda = new Moneda();
                            $moneda->nombre = 'YEN';
                            $moneda->plural = 'YENES';
                            $moneda->simbolo = '¥';
                            $moneda->tiene_decimales = 'si';
                            $moneda->save();
                            $monedas['yen'] = $moneda->id;
                        } else {
                            $monedas['yen'] = $yen->id;
                        }
                        //Euro
                        $euro = Moneda::find()->where(['UPPER(nombre)' => 'EURO'])->one();
                        if ($euro == null) {
                            $moneda = new Moneda();
                            $moneda->nombre = 'EURO';
                            $moneda->plural = 'EUROS';
                            $moneda->simbolo = '€';
                            $moneda->tiene_decimales = 'si';
                            $moneda->save();
                            $monedas['euro'] = $moneda->id;
                        } else {
                            $monedas['euro'] = $euro->id;
                        }
                        //Libra
                        $libra = Moneda::find()->where(['UPPER(nombre)' => 'LIBRA'])->one();
                        if ($libra == null) {
                            $moneda = new Moneda();
                            $moneda->nombre = 'LIBRA';
                            $moneda->plural = 'LIBRAS';
                            $moneda->simbolo = '£';
                            $moneda->tiene_decimales = 'si';
                            $moneda->save();
                            $monedas['libra'] = $moneda->id;
                        } else {
                            $monedas['libra'] = $libra->id;
                        }
                    } elseif ($rownro == 2) {// COMPRA / VENTA por moneda

                    } else {//Datos
                        //Si, fin de linea
                        if (!isset($row['A'])) {
                            break;
                        }

                        //Si, error
                        if (!isset($row['B']) or !isset($row['C']) or !isset($row['D']) or !isset($row['E']) or !isset($row['F']) or !isset($row['G']) or !isset($row['H']) or !isset($row['I']) or !isset($row['J']) or !isset($row['K']) or !isset($row['L']) or !isset($row['M'])) {
                            throw new Exception('Archivo incorrecto.');
                        }

                        //Primero verificamos si existe una cotizacion para la fecha.
                        //Si hay, se modifica
                        $cotizacion_dolar = Cotizacion::find()->where(['DAY(fecha)' => $row['A']])->andWhere(['MONTH(fecha)' => $mes])->andWhere(['YEAR(fecha)' => $anho])->andWhere(['moneda_id' => $monedas['dolar']])->one();
                        $cotizacion_euro = Cotizacion::find()->where(['DAY(fecha)' => $row['A']])->andWhere(['MONTH(fecha)' => $mes])->andWhere(['YEAR(fecha)' => $anho])->andWhere(['moneda_id' => $monedas['euro']])->one();
                        $cotizacion_yen = Cotizacion::find()->where(['DAY(fecha)' => $row['A']])->andWhere(['MONTH(fecha)' => $mes])->andWhere(['YEAR(fecha)' => $anho])->andWhere(['moneda_id' => $monedas['yen']])->one();
                        $cotizacion_peso_arg = Cotizacion::find()->where(['DAY(fecha)' => $row['A']])->andWhere(['MONTH(fecha)' => $mes])->andWhere(['YEAR(fecha)' => $anho])->andWhere(['moneda_id' => $monedas['peso_arg']])->one();
                        $cotizacion_real = Cotizacion::find()->where(['DAY(fecha)' => $row['A']])->andWhere(['MONTH(fecha)' => $mes])->andWhere(['YEAR(fecha)' => $anho])->andWhere(['moneda_id' => $monedas['real']])->one();
                        $cotizacion_libra = Cotizacion::find()->where(['DAY(fecha)' => $row['A']])->andWhere(['MONTH(fecha)' => $mes])->andWhere(['YEAR(fecha)' => $anho])->andWhere(['moneda_id' => $monedas['libra']])->one();
                        if (isset($cotizacion_dolar)) {
                            $cotizacion_dolar->compra = ValorHelpers::formatStringToFloat($row['B']);
                            $cotizacion_dolar->venta = ValorHelpers::formatStringToFloat($row['C']);
                            if (!$cotizacion_dolar->validate()) {
                                throw new Exception('Error al actualizar cotización para dolar.');
                            }
                            $cotizacion_dolar->update();
                        } else {
                            $cotizacion_dolar = new Cotizacion();
                            $cotizacion_dolar->moneda_id = $monedas['dolar'];
                            $cotizacion_dolar->fecha = date('y-m-d', strtotime($anho . '-' . $mes . '-' . $row['A']));
                            $cotizacion_dolar->compra = ValorHelpers::formatStringToFloat($row['B']);
                            $cotizacion_dolar->venta = ValorHelpers::formatStringToFloat($row['C']);
                            if (!$cotizacion_dolar->validate()) {
                                throw new Exception('Error al insertar cotización para dolar.');
                            }
                            $cotizacion_dolar->insert();
                        }

                        if (isset($cotizacion_euro)) {
                            $cotizacion_euro->compra = ValorHelpers::formatStringToFloat($row['J']);
                            $cotizacion_euro->venta = ValorHelpers::formatStringToFloat($row['K']);
                            if (!$cotizacion_euro->validate()) {
                                throw new Exception('Error al actualizar cotización para euro.');
                            }
                            $cotizacion_euro->update();
                        } else {
                            $cotizacion_euro = new Cotizacion();
                            $cotizacion_euro->moneda_id = $monedas['euro'];
                            $cotizacion_euro->fecha = date('y-m-d', strtotime($anho . '-' . $mes . '-' . $row['A']));
                            $cotizacion_euro->compra = ValorHelpers::formatStringToFloat($row['J']);
                            $cotizacion_euro->venta = ValorHelpers::formatStringToFloat($row['K']);
                            if (!$cotizacion_euro->validate()) {
                                throw new Exception('Error al insertar cotización para euro.');
                            }
                            $cotizacion_euro->insert();
                        }

                        if (isset($cotizacion_yen)) {
                            $cotizacion_yen->compra = ValorHelpers::formatStringToFloat($row['H']);
                            $cotizacion_yen->venta = ValorHelpers::formatStringToFloat($row['I']);
                            if (!$cotizacion_yen->validate()) {
                                throw new Exception('Error al actualizar cotización para yen.');
                            }
                            $cotizacion_yen->update();
                        } else {
                            $cotizacion_yen = new Cotizacion();
                            $cotizacion_yen->moneda_id = $monedas['yen'];
                            $cotizacion_yen->fecha = date('y-m-d', strtotime($anho . '-' . $mes . '-' . $row['A']));
                            $cotizacion_yen->compra = ValorHelpers::formatStringToFloat($row['H']);
                            $cotizacion_yen->venta = ValorHelpers::formatStringToFloat($row['I']);
                            if (!$cotizacion_yen->validate()) {
                                throw new Exception('Error al insertar cotización para yen.');
                            }
                            $cotizacion_yen->insert();
                        }

                        if (isset($cotizacion_real)) {
                            $cotizacion_real->compra = ValorHelpers::formatStringToFloat($row['D']);
                            $cotizacion_real->venta = ValorHelpers::formatStringToFloat($row['E']);
                            if (!$cotizacion_real->validate()) {
                                throw new Exception('Error al actualizar cotización para real.');
                            }
                            $cotizacion_real->update();
                        } else {
                            $cotizacion_real = new Cotizacion();
                            $cotizacion_real->moneda_id = $monedas['real'];
                            $cotizacion_real->fecha = date('y-m-d', strtotime($anho . '-' . $mes . '-' . $row['A']));
                            $cotizacion_real->compra = ValorHelpers::formatStringToFloat($row['D']);
                            $cotizacion_real->venta = ValorHelpers::formatStringToFloat($row['E']);
                            if (!$cotizacion_real->validate()) {
                                throw new Exception('Error al insertar cotización para real.');
                            }
                            $cotizacion_real->insert();
                        }

                        if (isset($cotizacion_peso_arg)) {
                            $cotizacion_peso_arg->compra = ValorHelpers::formatStringToFloat($row['F']);
                            $cotizacion_peso_arg->venta = ValorHelpers::formatStringToFloat($row['G']);
                            if (!$cotizacion_peso_arg->validate()) {
                                throw new Exception('Error al actualizar cotización para peso arg.');
                            }
                            $cotizacion_peso_arg->update();
                        } else {
                            $cotizacion_peso_arg = new Cotizacion();
                            $cotizacion_peso_arg->moneda_id = $monedas['peso_arg'];
                            $cotizacion_peso_arg->fecha = date('y-m-d', strtotime($anho . '-' . $mes . '-' . $row['A']));
                            $cotizacion_peso_arg->compra = ValorHelpers::formatStringToFloat($row['F']);
                            $cotizacion_peso_arg->venta = ValorHelpers::formatStringToFloat($row['G']);
                            if (!$cotizacion_peso_arg->validate()) {
                                throw new Exception('Error al insertar cotización para peso arg.');
                            }
                            $cotizacion_peso_arg->insert();
                        }

                        if (isset($cotizacion_libra)) {
                            $cotizacion_libra->compra = ValorHelpers::formatStringToFloat($row['L']);
                            $cotizacion_libra->venta = ValorHelpers::formatStringToFloat($row['M']);
                            if (!$cotizacion_libra->validate()) {
                                throw new Exception('Error al actualizar cotización para libra.');
                            }
                            $cotizacion_libra->update();
                        } else {
                            $cotizacion_libra = new Cotizacion();
                            $cotizacion_libra->moneda_id = $monedas['libra'];
                            $cotizacion_libra->fecha = date('y-m-d', strtotime($anho . '-' . $mes . '-' . $row['A']));
                            $cotizacion_libra->compra = ValorHelpers::formatStringToFloat($row['L']);
                            $cotizacion_libra->venta = ValorHelpers::formatStringToFloat($row['M']);
                            if (!$cotizacion_libra->validate()) {
                                throw new Exception('Error al insertar cotización para libra.');
                            }
                            $cotizacion_libra->insert();
                        }
                    }
                }
                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage('Cotizaciones agregadas de forma exitosa.');
                $mensaje = "Cotizaciones agregadas de forma exitosa.";
            } catch (Exception $e) {
                FlashMessageHelpsers::createErrorMessage($e->getMessage());
                $mensaje = '
                    <div class="alert alert-danger">
                      <strong>Error!</strong> ' . $e->getMessage() . '
                    </div>
                ';
                $transaction->rollBack();
            }
        }

        if ($mensaje != "") {
            $mensaje = "<br /><br />Mensajes <br /> 
                <div class='alert alert-success'>
                  <strong>Éxito!</strong>$mensaje
                </div>
            ";
        }

        retorno:;
        return $this->render('create_file', [
            'model' => $model, 'mensaje' => $mensaje
        ]);
    }

    /**
     * Finds the Cotizacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cotizacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cotizacion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Permitir acceso siempre a estas acciones
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $operacion = str_replace("/", "-", Yii::$app->controller->route);
        $permitirSiempre = [
            'cotizacion-chequeo-existencia'
        ];

        if (in_array($operacion, $permitirSiempre)) {
            return true;
        }

        return parent::beforeAction($action);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false;
    }
}
