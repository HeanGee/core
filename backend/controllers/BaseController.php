<?php

namespace backend\controllers;

use common\helpers\PermisosHelpers;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

abstract class BaseController extends Controller
{

    public function beforeAction($action)
    {
        if (!isset($action->actionMethod)) {
            echo $this->render('/site/error', ['name' => "Error de acceso", 'message' => 'No existe la página solicitada.']);
            return false;
        }

        $session = Yii::$app->session;
        try {
            if (!parent::beforeAction($action)) {
                return false;
            }
        } catch (BadRequestHttpException $e) {
        }

        $operacion = str_replace("/", "-", Yii::$app->controller->route);

        $permitirSiempre = ['site-captcha', 'site-signup', 'site-index', 'site-error', 'site-about', 'site-contact', 'site-login', 'site-logout', 'site-error-target'];

        if (in_array($operacion, $permitirSiempre)) {
            return true;
        }

        if (!PermisosHelpers::getAcceso($operacion)) {
            //Si la operacion corresponde a set-empresa-actual se hacer render ajax para mostrar error en modal.
            if (strpos($operacion, 'set-empresa-actual') !== false)
                echo $this->renderAjax('@app/views/site/error', ['name' => "Error de acceso", 'message' => 'No tienes permiso para acceder a esta sección']);
            else
                echo $this->render('@app/views/site/error', ['name' => "Error de acceso", 'message' => 'No tienes permiso para acceder a esta sección']);

            return false;
        }

        // Agregar verificación de empresa actual.
        if (is_bool($this->getNoRequierenEmpresa()) && !$this->getNoRequierenEmpresa()) {
            return true;
        }

        $noRequiereEmpresa = $this->getNoRequierenEmpresa();
        if (in_array($operacion, $noRequiereEmpresa)) { // no requiere verificacion de empresa
            return true;
        } else {
            if ($session['core_empresa_actual'] === null) {
                $this->redirect(['/site/index']);
                return false;
            }
        }

        // Verificar empresa y periodo actual
        if (!$session->has('core_empresa_actual')) {
            return false;
        }

        if (class_exists('\backend\modules\contabilidad\MainModule')) {
            if (!$session->has('core_empresa_actual_pc'))
                return false;

            $periodo = \backend\modules\contabilidad\models\EmpresaPeriodoContable::findOne(['id' => $session->get('core_empresa_actual_pc')]);
            if (!isset($periodo))
                return false;

            if ($session->get('core_empresa_actual') != $periodo->empresa_id)
                return false;
        }

        return true;
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    abstract function getNoRequierenEmpresa();
}
