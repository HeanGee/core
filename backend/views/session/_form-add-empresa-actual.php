<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 11/12/2017
 * Time: 13:15
 */

use common\models\User;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model \backend\controllers\EmpresaSession */

?>
    <div id='modal-add-empresa-actual' class="add-empresa-actual-form">

        <?php ActiveFormDisableSubmitButtonsAsset::register($this);

        $form = ActiveForm::begin([
            'id' => 'empresa_actual_form',
            'enableAjaxValidation' => true,
            'enableClientScript' => true,
            'enableClientValidation' => true,
            'options' => ['class' => 'disable-submit-buttons']
        ]);

        try {

            $attributes = [];

            $attributes['empresa_id'] = [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::class,
                'label' => 'Empresa',
                'options' => [
                    'data' => User::getEmpresaListaParaEmpresaActual(1),
                    'options' => [
                        'placeholder' => 'Seleccione empresa ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'pluginEvents' => [
                        'change' => "function(){
                            if (document.getElementById('empresasession-periodo_id') !== null){
                                $('#empresasession-periodo_id').val('').trigger('change');
                                $('#empresasession-periodo_id').select2('open');
                            } else {
                                if ($('#empresasession-empresa_id').val() !== ''){
                                    $('#boton_submit').click();
                                }
                            }
                        }"
                    ],
                ]
            ];

            if (class_exists('\backend\modules\contabilidad\MainModule')) {
                $attributes['periodo_id'] = [
                    'type' => Form::INPUT_WIDGET,
                    'label' => 'Periodo Contable',
                    'widgetClass' => Select2::class,
                    'options' => [
                        'initValueText' => !empty($model->id) ? $model->anho : '',
                        'options' => [
                            'placeholder' => 'Seleccione un periodo contable ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'ajax' => [
                                'url' => Url::to(['contabilidad/empresa-periodo-contable/get-pc-lista']),
                                'dataType' => 'json',
                                'data' => new JsExpression("function(params) { return {q:params.term,id:$('#empresasession-empresa_id').val()}; }")
                            ]
                        ],
                        'pluginEvents' => [
                            'change' => "function(){
                                if($('#empresasession-periodo_id').val().length > 0)
                                    $('#boton_submit').click();
                            }"
                        ],
                    ]
                ];
            }

            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 1,
                'attributes' => $attributes
            ]);
        } catch (Exception $e) {
            echo $e;
        }

        //        if (class_exists('\backend\modules\contabilidad\MainModule')) {
        //            $model_periodo = new \backend\modules\contabilidad\models\EmpresaPeriodoContable();
        //            echo $this->render('@backend/modules/contabilidad/views/empresa-periodo-contable/_form_set_pc_session', [
        //                'form' => $form,
        //            ]);
        //        } ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Seleccionar'), ['id' => 'boton_submit', 'data' => ['disabled-text' => 'Guardando...'], 'class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

        <?php
        //    js para guardao y actualización de grilla
        $this->registerJs('
        // obtener la id del formulario y establecer el manejador de eventos
        $("form#empresa_actual_form").on("beforeSubmit", function(e) {
            var form = $(this);
            $.post(
                form.attr("action")+"&submit=true",
                form.serialize()
            )
            .done(function(result) {
                form.parent().html(result.message);
                $.pjax.reload({container:"#empresa_pjax", async:false});
                $("#modal").modal("hide");      
                $("modal-body").html("");   
                $(window).unbind("beforeunload");
                location.reload();  // Se recarga la página  
            });
            return false;
        }).on("submit", function(e){
        
            var empresa_actual = document.getElementById("empresasession-empresa_id");
            var empresa_actual = empresa_actual.options[empresa_actual.selectedIndex].value;
            if( empresa_actual != "" ){
                localStorage.setItem("core-empresa-actual", empresa_actual);            
            }

            e.preventDefault();
            e.stopImmediatePropagation();
            return false;
        });
    ');
        ?>

    </div>

<?php
$script_onModalShown = <<<JS
$('#modal').on('shown.bs.modal', function () {
    $('#empresasession-empresa_id').select2('open');
});

// $('#campo_periodo_actual').on("select2:select", function (e) {
//     console.log('seleccionado periodo contable');
//     $('#boton_submit').click();
// });
JS;
$this->registerJs($script_onModalShown);
?>