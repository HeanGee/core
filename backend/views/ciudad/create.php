<?php


/* @var $this yii\web\View */
/* @var $model backend\models\Ciudad */

$this->title = 'Crear Ciudad';
$this->params['breadcrumbs'][] = ['label' => 'Ciudades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciudad-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
