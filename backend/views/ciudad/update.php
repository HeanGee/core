<?php

/* @var $this yii\web\View */
/* @var $model backend\models\Ciudad */

$this->title = 'Modificar Ciudad: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Ciudades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="ciudad-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
