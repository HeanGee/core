<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\PermisosHelpers;

/* @var $this yii\web\View */
/* @var $model backend\models\Ciudad */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Ciudades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciudad-view">


    <p>
        <?= PermisosHelpers::getAcceso('ciudad-update') ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= PermisosHelpers::getAcceso('ciudad-delete') ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'nombre',
            //'departamento_id_fk',
            [
                'attribute' => 'departamento_id',
                'value' => $model->departamento->nombre
            ],
            [
                'label' => 'Estado',
                'attribute' => 'active',
                'value' => ($model->active == 1 ? 'Activo' : 'Inactivo')
            ],
            'codigo_set'
        ],
    ]) ?>

</div>
