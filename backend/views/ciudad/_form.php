<?php

use backend\models\Departamento;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;

/* @var $this yii\web\View */
/* @var $model backend\models\Ciudad */
/* @var $form kartik\form\ActiveForm */
?>

<div class="ciudad-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'autoGenerateColumns' => false,
                'columns' => 2,
                'attributes' => [
                    'nombre' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => ['placeholder' => 'Nombre de ciudad'],
                    ],
                    'departamento_id' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => 'kartik\select2\Select2',
                        'options' => [
                            'data' => Departamento::getDepartamentoLista(true),
                            'options' => ['placeholder' => 'Seleccione departamento ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]
                    ],
                    'active' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => 'kartik\select2\Select2',
                        'options' => [
                            'data' => [1 => 'Activo', 0 => 'Inactivo'],
                            'value' => 0,
                            'pluginOptions' => [
                                'allowClear' => false,
                            ],
                        ]
                    ],
                    'codigo_set' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => ['placeholder' => 'Código Set'],
                    ],
                ]
            ],
            [
                'attributes' => [
                    'action' => [
                        'type' => Form::INPUT_RAW,
                        'value' => '<div style="margin-top: 20px">' .
                        \common\helpers\PermisosHelpers::getAcceso('ciudad-create') ? Html::submitButton('Guardar', ['class' => 'btn btn-primary']) : '' .
                            '</div>'
                    ]
                ]
            ]
        ]
    ]) ?>

    <?php ActiveForm::end(); ?>

</div>
