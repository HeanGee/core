<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\UsuarioEmpresa */

$this->title = 'Update User Empresa: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'User Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->usuario_id, 'url' => ['view', 'user_id' => $model->usuario_id, 'empresa_id' => $model->empresa_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usuario-empresa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
