<?php


/* @var $this yii\web\View */
/* @var $model backend\models\UsuarioEmpresa */

$this->title = 'Asociación a empresa';
$this->params['breadcrumbs'][] = ['label' => 'User Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-empresa-create">

    <?= $this->render('_form', [
        'model' => $model,
        'json_pick_left' => $json_pick_left,
        'json_pick_right' => $json_pick_right
    ]) ?>

</div>
