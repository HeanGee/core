<?php

use common\models\User;
use yii\helpers\Html;
use common\helpers\PermisosHelpers;

/* @var $this yii\web\View */
/* @var $model User */
$perfil = $model->getPerfil()->one();
$title = isset($perfil) ? $perfil->nombreCompleto : $model->username;
$this->title = 'Usuario: ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios por Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-empresa-view">

    <p>
        <?= PermisosHelpers::getAcceso('usuario-empresa-update') ? Html::a('Actualizar', ['update', 'usuario_id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
    </p>

    <label for="left_id"> Empresas a las cuales tiene acceso </label>
    <select style="height: 500px" disabled class='form-control' id='left_id' multiple>
        <?php
        foreach ($model->getEmpresaList() as $empresa) {
            echo "<option>" . $empresa['nombre'] . "</option>";
        }
        ?>
    </select>

    <?php
    echo Html::a('Cancelar', 'index.php?r=usuario-empresa/index',
        [
            'class' => 'btn btn-primary',
        ]
    );
    ?>
</div>
