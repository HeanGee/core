<?php

use common\models\User;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use common\helpers\PermisosHelpers;

/* @var $this yii\web\View */
/* @var $model backend\models\UsuarioEmpresa */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="usuario-empresa-form">

        <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>
        <?php $form = ActiveForm::begin([
            'id' => 'usuario-empresa-form',
            'options' => ['class' => 'disable-submit-buttons']
        ]); ?>

        <?php
        $lista_usuario = [];
        foreach (User::find()->all() as $user) {
            $lista_usuario[$user->id] = $user->perfil != null ? $user->perfil->nombreCompleto : $user->username;
        }
        ?>
        <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::class,
                    'hint' => 'Usuario',
                    'label' => 'Usuario',
                    'options' => [
                        'data' => $lista_usuario,
                        'options' => ['placeholder' => 'Seleccione usuario ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ], 'disabled' => true
                    ]
                ],
            ]
        ]); ?>

        <div style='display:none;'>
            <?php
            echo $form->field($model, 'id')->widget(Select2::classname(), [

                'data' => $lista_usuario,
                'options' => ['placeholder' => 'Por favor Seleccione Uno'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->label(''); ?>
        </div>

        <br>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Empresas</h3>
            </div>
            <div class="panel-body">

                <div id="pickList"></div>
            </div>
        </div>

        <div class="form-group">
            <?= PermisosHelpers::getAcceso('usuario-empresa-create') ? Html::submitButton('Guardar', ['data' => ['disabled-text' => 'Guardando...'], 'class' => 'btn btn-success']) : '' ?>
            <?= Html::a('Cancelar', 'index.php?r=usuario-empresa/index',
                [
                    'class' => 'btn btn-primary',
                ]
            ) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

<?php
$script = <<<JS
// Evitar que se haga submit con ENTER
$(document).ready(function () {
    $(window).keydown(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            return false;
        }
    });
});

// Picklist
(function ($) {

    $.fn.pickList = function (options) {
        var opts = $.extend({}, $.fn.pickList.defaults, options);

        this.fill = function () {
            var option_left = '';

            $.each(opts.data_left, function (key, val) {
                option_left += '<option data-id=' + val.id + '>' + val.text + '</option>';
            });
            this.find('.pickData').append(option_left);

            var option_right = '';

            $.each(opts.data_right, function (key, val) {
                option_right += '<option data-id=' + val.id + '>' + val.text + '</option>';
            });
            this.find('.pickListResult').append(option_right);
        };
        this.controll = function () {
            var pickThis = this;

            pickThis.find(".pAdd").on('click', function () {
                var p = pickThis.find(".pickData option:selected");
                p.clone().appendTo(pickThis.find(".pickListResult"));
                reload_left(p);
                p.remove();
            });

            pickThis.find(".pAddAll").on('click', function () {
                var p = pickThis.find(".pickData option");
                p.clone().appendTo(pickThis.find(".pickListResult"));
                reload_left(p);
                p.remove();
            });

            pickThis.find(".pRemove").on('click', function () {
                var p = pickThis.find(".pickListResult option:selected");
                p.clone().appendTo(pickThis.find(".pickData"));
                reload_right(p);
                p.remove();
            });

            pickThis.find(".pRemoveAll").on('click', function () {
                var p = pickThis.find(".pickListResult option");
                p.clone().appendTo(pickThis.find(".pickData"));
                reload_right(p);
                p.remove();
            });
        };

        this.getValues = function () {
            var objResult = [];
            this.find(".pickListResult option").each(function () {
                objResult.push({
                    id: $(this).data('id'),
                    text: this.text
                });
            });
            return objResult;
        };

        this.init = function () {
            var pickListHtml =
                "<div class='row'>" +
                "  <div class='col-sm-5'>" +
                "    <input class='input' id='input_left' type='text' placeholder='Filtrar'>" +
                "<label style='margin-left:15%'>Disponibles</label>" +
                "	 <select class='form-control pickListSelect pickData' id='left_id' multiple></select>" +
                " </div>" +
                " <div class='col-sm-2 pickListButtons'>" +
                " <br><br>" +
                "	<button type='button' class='pAdd btn btn-primary btn-sm'>" + opts.add + "</button>" +
                "   <button type='button' class='pAddAll btn btn-primary btn-sm'>" + opts.addAll + "</button>" +
                "	<button type='button' class='pRemove btn btn-primary btn-sm'>" + opts.remove + "</button>" +
                "	<button type='button' class='pRemoveAll btn btn-primary btn-sm'>" + opts.removeAll + "</button>" +
                " </div>" +
                " <div class='col-sm-5'>" +
                " 	 <input class='input' id='input_right' type='text' placeholder='Filtrar'>" +
                "       <label style='margin-left:15%'>Seleccionadas</label>" +
                "    <select class='form-control pickListSelect pickListResult' id='right_id' multiple></select>" +
                " </div>" +
                "</div>";

            this.append(pickListHtml);

            this.fill();
            this.controll();
        };

        this.init();
        return this;
    };

    $.fn.pickList.defaults = {
        add: 'Agregar',
        addAll: 'Agregar Todo',
        remove: 'Borrar',
        removeAll: 'Borrar Todo'
    };


}(jQuery));

var pick = $("#pickList").pickList({
    data_left: $json_pick_left,
    data_right: $json_pick_right
});

$("#getSelected").click(function () {
    console.log(pick.getValues());
});

//filtro de picklist
jQuery.fn.filterByText = function (textbox) {
    return this.each(function () {
        var select = this;

        // Orden de lista en select
        var my_options = $(select).find('option');
        var selected = $(select).val();
        my_options.sort(function (a, b) {
            if (a.text.toLowerCase() > b.text.toLowerCase()) return 1;
            if (a.text.toLowerCase() < b.text.toLowerCase()) return -1;
            return 0
        });
        $(select).empty().append(my_options);
        $(select).val(selected);

        // genera lista auxiliar con datos originales

        var options = [];
        $(select).find('option').each(function () {
            options.push({
                value: $(this).data('id'),
                text: $(this).text()
            });
        });
        $(select).data('options', options);

        // Evento capturado para filtrar
        $(textbox).bind('change keyup', function () {
            var options = $(select).empty().data('options');
            var search = $.trim($(this).val());
            var regex = new RegExp(search, "gi");
            $.each(options, function (i) {
                var option = options[i];
                if (option.text.match(regex) !== null) {
                    $(select).append(
                        $('<option  data-id=' + option.value + '>').text(option.text).val(option.value)
                    );
                }
            });
        });
    });
};

//

$(function () {
    $('#left_id').filterByText($('#input_left'));
});

$(function () {
    $('#right_id').filterByText($('#input_right'));
});

$(document).on('submit', '#usuario-empresa-form', function () {
    var selected = [];
    $('#right_id').find('option').each(function () {
        selected.push($(this)[0].getAttribute('data-id'));
    });

    $.each(selected, function (i, v) {
        var input = $("<input>").attr({"type": "hidden", "name": "selected[]"}).val(v);
        $('#usuario-empresa-form').append(input);
    });
    return true;
});

var reload_left = function (p) {
    var options_left = [];
    var options_right = [];
    options_left = $('#left_id').data('options');
    options_right = $('#right_id').data('options');
    $(p).each(function () {
        var id_or = $(this).data('id');

        $.each(options_left, function (i) { // cada elemento que habia originalmente
            var option = options_left[i];

            if (!(typeof option === 'undefined')) {
                if (id_or === option.value) {

                    options_right.push({
                        value: option.value,
                        text: option.text
                    });
                    options_left.splice(options_left.indexOf(option), 1);
                }
            }
        });
    });
};

var reload_right = function (p) {
    var options_right = [];
    options_right = $('#right_id').data('options');
    var options_left = [];
    options_left = $('#left_id').data('options');
    $(p).each(function () {
        var id_or = $(this).data('id');

        $.each(options_right, function (i) { // cada elemento que habia originalmente
            var option = options_right[i];

            if (!(typeof option === 'undefined')) {
                if (id_or === option.value) {
                    options_left.push({
                        value: option.value,
                        text: option.text
                    });
                    options_right.splice(options_right.indexOf(option), 1);
                }
            }
        });
    });
};
JS;

$this->registerJs($script);

$this->registerCss('.pickListButtons {
  padding: 10px;
  text-align: center;
}

.pickListButtons button {
    display: block;
    margin: auto;
    margin-bottom: 5px;
}

.pickListSelect {
  height: 200px !important;
}

.input {
    background-image: url(img/searchicon.png);
    background-position: 10px 12px;
    background-repeat: no-repeat;
    padding: 12px 20px 12px 42px;
    border: 1px solid #ddd;
    margin-bottom: 12px;
}');
?>