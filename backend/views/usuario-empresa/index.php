<?php

use common\helpers\PermisosHelpers;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UsuarioEmpresa */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios por empresa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-empresa-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    $template = '';
    $template = $template . (PermisosHelpers::getAcceso('usuario-empresa-view') ? '{view} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('usuario-empresa-update') ? '{update} &nbsp&nbsp&nbsp' : '');
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'usuario',
                'label' => 'Usuario',
                'format' => 'raw',
                'value' => function ($data) {
                    $url = "index.php?r=usuario-empresa/create&usuario_id=" . $data->id;
                    return Html::a($data->perfil ? $data->perfil->nombreCompleto : $data->username, $url, ['title' => 'Accesos']);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'header' => 'Acciones',
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'update') {
                        $url = 'index.php?r=usuario-empresa/create&usuario_id=' . $model->id;
                        return $url;
                    } else if ($action === 'view') {
                        $url = 'index.php?r=usuario-empresa/view&usuario_id=' . $model->id;
                        return $url;
                    }
                    return '';
                }
            ],
        ],
    ]); ?>
</div>
