<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\PermisosHelpers;

/* @var $this yii\web\View */
/* @var $model backend\models\Departamento */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Departamentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departamento-view">


    <p>
        <?= PermisosHelpers::getAcceso('departamento-update') ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= PermisosHelpers::getAcceso('departamento-delete') ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'numero',
            'nombre',
            'codigo_set'
        ],
    ]) ?>

</div>
