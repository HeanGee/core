<?php

use common\helpers\PermisosHelpers;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DepartamentoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Departamentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departamento-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= PermisosHelpers::getAcceso('departamento-create') ? Html::a('Crear Departamento', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>

    <?php
    $template = '';
    $template = $template . (PermisosHelpers::getAcceso('departamento-view') ? '{view} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('departamento-update') ? '{update} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('departamento-delete') ? '{delete} &nbsp&nbsp&nbsp' : '');
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'numero',
            'nombre',
            [
                'label' => 'Estado',
                'attribute' => 'active',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    return $model->active == 1 ? 'Activo' : 'Inactivo';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [1 => 'Activo', 0 => 'Inactivo', 3 => 'Todos'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => false],
                ],
                'filterInputOptions' => ['placeholder' => 'Filtro de Estado'],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'header' => 'Acciones',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<script>


</script>