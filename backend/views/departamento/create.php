<?php


/* @var $this yii\web\View */
/* @var $model backend\models\Departamento */

$this->title = 'Crear Departamento';
$this->params['breadcrumbs'][] = ['label' => 'Departamentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departamento-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
