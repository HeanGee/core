<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;

/* @var $this yii\web\View */
/* @var $model backend\models\Departamento */
/* @var $form kartik\form\ActiveForm */
?>

<div class="departamento-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'autoGenerateColumns' => false,
                'columns' => 2,
                'attributes' => [
                    'numero' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => ['placeholder' => 'Numero de departamento'],
                    ],
                    'nombre' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => ['placeholder' => 'Nombre de departamento'],
                    ],
                    'active' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => 'kartik\select2\Select2',
                        'options' => [
                            'data' => [1 => 'Activo', 0 => 'Inactivo'],
                            //'options' => ['placeholder' => 'Seleccione Estado'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'value' => 0,
                            ],
                        ]
                    ],
                    'codigo_set' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => ['placeholder' => 'Código Set'],
                    ],
                ]
            ],
            [
                'attributes' => [
                    'action' => [
                        'type' => Form::INPUT_RAW,
                        'value' => '<div style="margin-top: 20px">' .
                        \common\helpers\PermisosHelpers::getAcceso('departamento-create') ? Html::submitButton('Guardar', ['class' => 'btn btn-primary']) : '' .
                            '</div>'
                    ]
                ]
            ]
        ]
    ]) ?>

    <?php ActiveForm::end(); ?>

</div>
