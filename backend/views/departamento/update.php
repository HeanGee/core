<?php

/* @var $this yii\web\View */
/* @var $model backend\models\Departamento */

$this->title = 'Modificar Departamento';
$this->params['breadcrumbs'][] = ['label' => 'Departamentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="departamento-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
