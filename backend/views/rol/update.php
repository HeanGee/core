<?php

use backend\models\Rol;

/* @var $this yii\web\View */
/* @var $model Rol */

$this->title = 'Rol: ' . $model->rol_nombre;
$this->params['breadcrumbs'][] = ['label' => 'Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $model->rol_nombre;
?>
<div class="rol-update">

    <?=
    $this->render('_form', [
        'model' => $model,
        'json_pick_left' => $json_pick_left,
        'json_pick_right' => $json_pick_right
    ])
    ?>

</div>
