<?php

use backend\models\Rol;
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\PermisosHelpers;

/* @var $this yii\web\View */
/* @var $model Rol */

$this->title = 'Rol: ' . $model->rol_nombre;
$this->params['breadcrumbs'][] = ['label' => 'Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rol-view">
    <p>
        <?= PermisosHelpers::getAcceso('rol-update') ? Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= PermisosHelpers::getAcceso('rol-delete') ?
            Html::a('Borrar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', '¿Está seguro de eliminar este elemento?'),
                    'method' => 'post',
                ],
            ]) : ''
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'rol_nombre',
            'rol_valor',
        ],
    ])
    ?>
    <label for="left_id"> Operaciones Permitidas </label>
    <select style="height: 400px" disabled class='form-control' id='left_id' multiple>
        <?php
        foreach ($model->operacionesPermitidasList as $operacionPermitida) {
            echo "<option>" . $operacionPermitida['nombre'] . "</option>";
        }
        ?>
    </select>
    <?php
    echo Html::a('Cancelar', 'index.php?r=rol/index',
        [
            'class' => 'btn btn-primary',
        ]
    );
    ?>
</div>
