<?php

use backend\models\Rol;

/* @var $this yii\web\View */
/* @var $model Rol */

$this->title = Yii::t('app', 'Crear Rol');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rol-create">

    <?=
    $this->render('_form', [
        'model' => $model,
        'json_pick_left' => $json_pick_left,
        'json_pick_right' => $json_pick_right
    ])
    ?>

</div>
