<?php

use common\helpers\PermisosHelpers;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\RolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Roles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rol-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= PermisosHelpers::getAcceso('rol-create') ? Html::a('Nuevo Rol', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>
    <?php Pjax::begin();
    $template = '';
    $template = $template . (PermisosHelpers::getAcceso('rol-view') ? '{view} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('rol-update') ? '{update} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('rol-delete') ? '{delete} &nbsp&nbsp&nbsp' : '');
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'rol_nombre',
            'rol_valor',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'header' => 'Acciones',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
