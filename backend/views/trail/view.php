<?php

/** @var yii\web\View $this */

/** @var bedezign\yii2\audit\models\AuditTrail $model */

use bedezign\yii2\audit\Audit;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = Yii::t('audit', 'Trail #{id}', ['id' => $model->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Registros'), 'url' => ['entry/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Registro #' . $model->entry_id), 'url' => ['entry/view', 'id' => $model->entry_id]];
$this->params['breadcrumbs'][] = 'Rastro #' . $model->id;

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        [
            'label' => $model->getAttributeLabel('user_id'),
            'value' => Audit::getInstance()->getUserIdentifier($model->user_id),
            'format' => 'raw',
        ],
        [
            'attribute' => 'entry_id',
            'value' => $model->entry_id ? Html::a($model->entry_id, ['entry/view', 'id' => $model->entry_id]) : '',
            'format' => 'raw',
        ],
        'action',
        'model',
        'model_id',
        'field',
        'created',
    ],
]);

echo Html::tag('h2', Yii::t('audit', 'Difference'));
echo $model->getDiffHtml();

echo Html::tag('br');
echo Html::a('Atras ', ['entry/view', 'id' => $model->entry_id], ['class' => 'btn btn-success']);
echo '&nbsp';
echo Html::a('Regresar a la lista de Registros', ['entry/index'], ['class' => 'btn btn-info']);
