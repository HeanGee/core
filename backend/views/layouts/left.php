<?php

use common\helpers\PermisosHelpers;

$es_admin = PermisosHelpers::requerirMinimoRol('Admin');
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <!--        <form action="#" method="get" class="sidebar-form">-->
        <!--            <div class="input-group">-->
        <!--                <input type="text" name="q" class="form-control" placeholder="Search..."/>-->
        <!--                <span class="input-group-btn">-->
        <!--                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i-->
        <!--                                class="fa fa-search"></i>-->
        <!--                    </button>-->
        <!--                </span>-->
        <!--            </div>-->
        <!--        </form>-->
        <!-- /.search form -->
        <?php
        $menuItems[] = ['label' => 'Inicio',
            'url' => 'index.php',
            'icon' => 'fa fa-gear',
        ];
        $items = [];
        if (Yii::$app->user->identity) {
            if (PermisosHelpers::getAcceso('empresa-index')) {
                array_push($items, ['label' => 'Empresas', 'url' => ['/empresa/index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'empresa']);
            }
            if ((class_exists('\backend\modules\administracion\MainModule') or class_exists('\backend\modules\contabilidad\MainModule')) and PermisosHelpers::getAcceso('iva-index')) {
                array_push($items, ['label' => 'Iva', 'url' => ['/iva/index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'iva']);
            }
            $itemsCotizacion = [];
            if (PermisosHelpers::getAcceso('moneda-index')) {
                array_push($itemsCotizacion, ['label' => 'Monedas', 'url' => ['/moneda/index'], 'icon' => 'fa fa-btc', 'active' => \Yii::$app->controller->id == 'moneda']);
            }
            if (PermisosHelpers::getAcceso('cotizacion-index')) {
                array_push($itemsCotizacion, ['label' => 'Cotizaciones', 'url' => ['/cotizacion/index'], 'icon' => 'fa fa-money', 'active' => \Yii::$app->controller->id == 'cotizacion' and \Yii::$app->controller->route == "cotizacion/index"]);
            }
            if (PermisosHelpers::getAcceso('cotizacion-createfile')) {
                array_push($itemsCotizacion, ['label' => 'Carga desde archivo', 'url' => ['/cotizacion/createfile'], 'icon' => 'fa fa-file-text', 'active' => \Yii::$app->controller->id == 'cotizacion' and \Yii::$app->controller->route == "cotizacion/createfile"]);
            }
            if ((class_exists('\backend\modules\administracion\MainModule') or class_exists('\backend\modules\contabilidad\MainModule')) && !empty($itemsCotizacion)) {
                array_push($items, ['label' => 'Cotización', 'url' => ['#'], 'icon' => 'fa fa-money', 'items' => $itemsCotizacion]);
            }
            $itemsSeguridad = [];
            if (PermisosHelpers::getAcceso('usuario-index')) {
                array_push($itemsSeguridad, ['label' => 'Usuarios', 'url' => ['/usuario/index'], 'icon' => 'fa fa-user', 'active' => \Yii::$app->controller->id == 'usuario']);
            }
            if (PermisosHelpers::getAcceso('usuario-empresa-index')) {
                array_push($itemsSeguridad, ['label' => 'Usuarios por Empresa', 'url' => ['/usuario-empresa/index'], 'icon' => 'fa fa-user', 'active' => \Yii::$app->controller->id == 'usuario-empresa']);
            }
            if (PermisosHelpers::getAcceso('rol-index')) {
                array_push($itemsSeguridad, ['label' => 'Roles', 'url' => ['/rol/index'], 'icon' => 'fa fa-users', 'active' => \Yii::$app->controller->id == 'rol']);
            }
            if (PermisosHelpers::getAcceso('operacion-index')) {
                array_push($itemsSeguridad, ['label' => 'Operaciones', 'url' => ['/operacion/index'], 'icon' => 'fa fa-key', 'active' => \Yii::$app->controller->id == 'operacion']);
            }
            if (PermisosHelpers::getAcceso('estado-index')) {
                array_push($itemsSeguridad, ['label' => 'Estados', 'url' => ['/estado/index'], 'icon' => 'fa fa-heartbeat', 'active' => \Yii::$app->controller->id == 'estado']);
            }
            if (PermisosHelpers::getAcceso('')) {
                array_push($items, ['label' => 'Seguridad', 'url' => ['#'], 'icon' => 'fa fa-building', 'items' => $itemsSeguridad]);
            }
            $itemsUbicacion = [];
            if (PermisosHelpers::getAcceso('departamento-index')) {
                array_push($itemsUbicacion, ['label' => 'Departamentos', 'url' => ['/departamento/index'], 'icon' => 'fa fa-map-o', 'active' => \Yii::$app->controller->id == 'departamento']);
            }
            if (PermisosHelpers::getAcceso('ciudad-index')) {
                array_push($itemsUbicacion, ['label' => 'Ciudades', 'url' => ['/ciudad/index'], 'icon' => 'fa fa-map-o', 'active' => \Yii::$app->controller->id == 'ciudad']);
            }
            if (PermisosHelpers::getAcceso('barrio-index')) {
                array_push($itemsUbicacion, ['label' => 'Barrios', 'url' => ['/barrio/index'], 'icon' => 'fa fa-map-o', 'active' => \Yii::$app->controller->id == 'barrio']);
            }
            if (!empty($itemsUbicacion)) {
                array_push($items, ['label' => 'Ubicación', 'url' => ['#'], 'icon' => 'fa fa-building', 'items' => $itemsUbicacion,]);
            }

            $itemsParametro = [];
            if (PermisosHelpers::getAcceso('parametro-sistema-index')) {
                array_push($itemsParametro, ['label' => 'Index', 'url' => ['/parametro-sistema/index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'parametro-sistema' && Yii::$app->controller->action->id == 'index' && Yii::$app->controller->module->id == 'app-backend']);
            }
            if (PermisosHelpers::getAcceso('parametro-sistema-special-index'))
                array_push($itemsParametro, ['label' => 'Parámetros Base', 'url' => ['/parametro-sistema/special-index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'parametro-sistema' && Yii::$app->controller->action->id == 'special-index']);

            $itemsAuditoria = [];
            if (PermisosHelpers::getAcceso('entry-index')) {
                array_push($itemsAuditoria, ['label' => 'Registros', 'url' => ['/entry/index'], 'icon' => 'fa fa-area-chart', 'active' => \Yii::$app->controller->id == 'entry' || \Yii::$app->controller->id == 'trail']);
            }
            if (Yii::$app->user->identity->getId() == 1)
                array_push($itemsAuditoria, ['label' => 'Audit Trails', 'url' => ['/entry/audit-index'], 'icon' => 'fa fa-area-chart', 'active' => \Yii::$app->controller->id == 'entry' && \Yii::$app->controller->action->id == 'audit-index']);

            //Administracion
            if (class_exists('\backend\modules\administracion\MainModule')) {
                $itemsAdministracion = \backend\modules\administracion\MainModule::getMenuItems();
                if (!empty($itemsAdministracion)) {
                    $menuItems[] = [
                        'icon' => 'fa fa-gear',
                        'label' => 'Administración',
                        'items' => $itemsAdministracion,
                        'url' => '#'
                    ];
                }
            }
            //Contabilidad
            if (class_exists('\backend\modules\contabilidad\MainModule')) {
                $itemsContabilidad = \backend\modules\contabilidad\MainModule::getMenuItems();
                if (!empty($itemsContabilidad)) {
                    $menuItems[] = [
                        'icon' => 'fa fa-gear',
                        'label' => 'Contabilidad',
                        'items' => $itemsContabilidad,
                        'url' => '#'
                    ];
                }
            }
            //Calendario
            if (class_exists('\backend\modules\calendario\MainModule')) {
                $itemsCalendario = \backend\modules\calendario\MainModule::getMenuItems();
                if (!empty($itemsCalendario)) {
                    $menuItems[] = [
                        'icon' => 'fa fa-gear',
                        'label' => 'Calendario',
                        'items' => $itemsCalendario,
                        'url' => '#'
                    ];
                }
            }

            if (!empty($items)) {
                $menuItems[] = [
                    'icon' => 'fa fa-gear',
                    'label' => 'Sistema',
                    'items' => $items,
                    'url' => '#'
                ];
            }

            if (!empty($itemsAuditoria)) {
                $menuItems[] = [
                    'icon' => 'fa fa-gear',
                    'label' => 'Auditoria',
                    'items' => $itemsAuditoria,
                    'url' => '#'
                ];
            }

            if (!empty($itemsParametro)) {
                $menuItems[] = [
                    'icon' => 'fa fa-gear',
                    'label' => 'Parámetros',
                    'items' => $itemsParametro,
                    'url' => '#'
                ];
            }
        }

        try {
            echo dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => $menuItems,
                ]
            );
        } catch (Exception $e) {
        }
        ?>
    </section>
</aside>
