<?php

use yii\widgets\Breadcrumbs;

?>
    <div class="content-wrapper">
        <section class="content-header">
            <?php if (isset($this->blocks['content-header'])) { ?>
                <h1><?= $this->blocks['content-header'] ?></h1>
            <?php } else { ?>
                <h1>
                    <?php
                    if ($this->title !== null) {
                        echo \yii\helpers\Html::encode($this->title);
                    } else {
                        echo \yii\helpers\Inflector::camel2words(
                            \yii\helpers\Inflector::id2camel($this->context->module->id)
                        );
                        echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                    } ?>
                </h1>
            <?php } ?>

            <?=
            Breadcrumbs::widget(
                [
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]
            ) ?>
        </section>

        <section class="content" style="background: #ffffff;">
            <!--        --><? //= Alert::widget() ?>
            <?= $content ?>
        </section>
    </div>

<?php
if (Yii::$app->controller->module->id == 'administracion') {
    $version = \backend\modules\administracion\helpers\ParametroSistemaHelpers::getValorByNombre('system_version');
} elseif (Yii::$app->controller->module->id == 'contabilidad') {
    $version = \backend\modules\contabilidad\helpers\ParametroSistemaHelpers::getValorByNombre('system_version');
} else {
    $version = \common\helpers\ParametroSistemaHelpers::getValorByNombre('system_version');
}

$version = $version == '' ? '1.0' : $version;
$anho = date('Y');
?>
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> <?= $version ?>
        </div>
        <strong>Copyright <?= $anho ?> <a href="http://www.ingenio.com.py">INgenio S.A. </a></strong> Todos los derechos
        reservados
        | Desarrollado para <a href="http://www.jmg.com.py/">Consultora Contable JMG</a>
    </footer>

    <!-- Control Sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class='control-sidebar-bg'></div>


<?php

$CSS = <<<CSS
/*This is modifying the btn-primary colors but you could create your own .btn-something class as well*/
.btn-primary{
    color: #000000;
    background-color: #e3e3e3;
    border-color: #357ebd; /*set the color you want here*/
}
.btn-primary:focus {
    color: #000000;
    background-color: #fafafa;
    border-color: #357ebd; /*set the color you want here*/
}
.btn-primary:hover, .btn-primary:active, .btn-primary.active, .open > .dropdown-toggle.btn-primary {
    color: #fff;
    background-color: #3c8dbc;
    border-color: #357ebd; /*set the color you want here*/
}

.btn-success {
    color: #000000;
    background-color: #e3e3e3;
    border-color: #008d4c; /*set the color you want here*/
}

.btn-success:focus {
    color: #000000;
    background-color: #fafafa;
    border-color: #008d4c; /*set the color you want here*/
}
.btn-success:hover, .btn-success:active, .btn-success.active, .open > .dropdown-toggle.btn-success {
    color: #fff;
    background-color: #00a65a;
    border-color: #008d4c; /*set the color you want here*/
}

.btn-danger {
    color: #000000;
    background-color: #e3e3e3;
    border-color: #d73925; /*set the color you want here*/
}
.btn-danger:focus {
    color: #000000;
    background-color: #fafafa;
    border-color: #d73925; /*set the color you want here*/
}
.btn-danger:hover, .btn-danger:active, .btn-danger.active, .open > .dropdown-toggle.btn-danger {
    color: #fff;
    background-color: #dd4b39;
    border-color: #d73925; /*set the color you want here*/
}
/*.btn-danger:focus {*/
    /*color: #fff;*/
    /*font-weight: bold;*/
    /*background-color: #ff6653;*/
    /*border-color: #e53e22; !*set the color you want here*!*/
/*}*/

.btn-info {
    color: #000000;
    background-color: #e3e3e3;
    border-color: #00acd6; /*set the color you want here*/
}
.btn-info:focus {
    color: #000000;
    background-color: #fafafa;
    border-color: #00acd6; /*set the color you want here*/
}
.btn-info:hover, .btn-info:active, .btn-info.active, .open > .dropdown-toggle.btn-info {
    color: #fff;
    background-color: #00c0ef;
    border-color: #00acd6; /*set the color you want here*/
}

.btn-warning {
    color: #000000;
    background-color: #e3e3e3;
    border-color: #e08e0b; /*set the color you want here*/
}
.btn-warning:focus {
    color: #000000;
    background-color: #fafafa;
    border-color: #e08e0b; /*set the color you want here*/
}
.btn-warning:hover, .btn-warning:active, .btn-warning.active, .open > .dropdown-toggle.btn-warning {
    color: #fff;
    background-color: #f39c12;
    border-color: #e08e0b; /*set the color you want here*/
}

/* Para mouseover sobre las tablas */

.kv-grid-table tbody tr:hover {
    background-color: #ebffdd;
}

.table tbody tr:hover {
    background-color: #ebffdd;
}

CSS;

$this->registerCss($CSS);

?>