<?php

use backend\models\Empresa;
use backend\models\Perfil;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use common\helpers\ParametroSistemaHelpers;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\Pjax;


/* @var $this \yii\web\View */
/* @var $content string */

$perfil = Perfil::find()->where(['usuario_id' => Yii::$app->user->id])->one();
?>

<header class="main-header">
    <?php $nombre_sistema = ParametroSistemaHelpers::getValorByNombre('core_nombre_sistema') == null ? 'SISTEMA' : ParametroSistemaHelpers::getValorByNombre('core_nombre_sistema'); ?>
    <?= Html::a('<span class="logo-mini">JMG</span><span class="logo-lg">' . $nombre_sistema . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <?php
        Pjax::begin(['id' => 'empresa_pjax']); ?>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li class="dropdown messages-menu">
                    <!--                        <a href="#" id="empresa_actual_id">-->
                    <!--                            Empresa:-->
                    <!--                        </a>-->
                    <?php
                    /** @var Empresa $empresa */

                    $session = Yii::$app->session;
                    $empresa = Empresa::find()->where(['id' => $session->get('core_empresa_actual')])->one();
                    $empresaNombre = 'Sin empresa seleccionada';
                    $empresa_id = null;
                    if ($empresa) {
                        $empresa_id = $empresa->id;
                        $empresaNombre = "{$empresa_id} - {$empresa->nombre}";

                        // Concatenar periodo contable si existe modulo de contabilidad.
                        if (class_exists('\backend\modules\contabilidad\MainModule')) {
                            if (Yii::$app->session->has('core_empresa_actual_pc')) {
                                $periodo_cont = EmpresaPeriodoContable::findOne(['id' => Yii::$app->session->get('core_empresa_actual_pc')]);
                                if ($periodo_cont) {
                                    if ($periodo_cont->empresa_id == $empresa->id) {
                                        $empresaNombre .= ' | ' . $periodo_cont->anho;
                                        if ($periodo_cont->descripcion != '')
                                            $empresaNombre .= ' - ' . $periodo_cont->descripcion;
                                    }
                                }
                            }
                        }
                    }

                    //                    echo Html::a($empresaNombre . ' &nbsp&nbsp<span class="glyphicon glyphicon-pencil"></span>', '#', [
                    echo Html::a($empresaNombre, '#', [
                        'id' => 'empresa_actual_id',
                        'title' => Yii::t('app', 'Cambiar Empresa Actual'),
                        'empresa-id' => $empresa_id,
                        'data-toggle' => 'modal',
                        'data-target' => '#modal',
                        'data-pjax' => '0',
                    ]);
                    ?>
                </li>

                <?php if ($session->has('core_empresa_actual')): ?>
                    <li class="dropdown messages-menu">
                        <a id="edit-current-empresa" href="#" class="dropdown-toggle" data-toggle="dropdown"
                           title="Modificar Datos Empresa Actual">
                            <span class="glyphicon glyphicon-pencil"></span>
                            <span class="label label-success"></span>
                        </a>
                    </li>
                <?php endif ?>

                <!-- Messages: style can be found in dropdown.less-->
                <!--                <li class="dropdown messages-menu">-->
                <!--                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
                <!--                        <i class="fa fa-envelope-o"></i>-->
                <!--                        <span class="label label-success"></span>-->
                <!--                    </a>-->
                <!--                </li>-->


                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php if (Yii::$app->session->get('usuario') == null || $perfil == null || $perfil->avatar == null) { ?>
                            <img src="../web/img/avatar_neutro.png" class="user-image" alt="User Image"/>
                        <?php } else { ?>
                            <img src="<?= Perfil::getInitialPreviewImage($perfil->id) . $perfil->avatar; ?>"
                                 class="user-image" alt="Imagen de Usuario"/>
                        <?php } ?>
                        <span class="hidden-xs"><?= $perfil == null ? Yii::$app->user->identity->username : $perfil->nombreCompleto . '( ' . Yii::$app->user->identity->username . ' )' ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?php if (Yii::$app->session->get('usuario') == null || $perfil == null || $perfil->avatar == null) { ?>
                                <img src="../web/img/avatar_neutro.png" class="user-image" alt="User Image"/>

                            <?php } else { ?>
                                <img src="<?= Perfil::getInitialPreviewImage($perfil->id) . $perfil->avatar; ?>"
                                     class="img-circle" alt="User Image"/>
                            <?php } ?>
                            <p>
                                <?= Yii::$app->user->identity->username ?>
                                <small>Miembro
                                    desde <?= Yii::$app->formatter->asDAte(Yii::$app->user->identity->created_at) ?></small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <li class="user-footer" style="background: #8aa4af">
                            <div class="pull-left">
                                <a data-pjax="0" href="<?= Url::toRoute(['/perfil/update']); ?>"
                                   class="btn btn-info">Perfil</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Cerrar Sesion',
                                    '#',
                                    ['id' => 'logout_id', 'class' => 'btn btn-adn']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <!--                <li>
                                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                                </li>-->
            </ul>
        </div>
        <?php Pjax::end(); ?>
    </nav>
</header>
<?php
$url = Url::to(['/session/set-empresa-actual']);
$empresa_actual_id = \Yii::$app->session->has('core_empresa_actual') ?
    \Yii::$app->session->get('core_empresa_actual') : null;
$urlDelete = Url::to(['/site/logout']);
// para evitar el redirect contínuo cada vez que se cumpla la condición en el JS
$redirected = Yii::$app->request->get('redirected', 0);
$url_editEmpresaActual = Json::htmlEncode(\Yii::t('app', Url::to(['/empresa/update', 'id' => $empresa_actual_id])));

$baseUrl = Yii::$app->request->baseUrl;
$q = Yii::$app->request->getQueryParam('q');
$html = <<<HTML
<ul>
<li>Para <strong>Tipos de Documentos</strong> escribir `tipodoc`</li>
<li>Para <strong>Tipos de Documentos SET</strong> escribir `tipodocset`</li>
<li>Para <strong>Plan de Cuentas</strong> escribir `plancuenta`</li>
<li>Para <strong>Iva Cuenta</strong> escribir `ivacuenta`</li>
<li>Para <strong>Activo Fijo</strong> escribir `activofijo`</li>
<li>Para <strong>Activo Biológico</strong> escribir `activobio`</li>
<li>Para <strong>Presentación de Ivas</strong> escribir `presentacioniva</li>
<li>Para <strong>Recibos de Compra</strong> escribir `recibocompra</li>
</ul>
Para otros enlaces, puede utilizar esquema similar.
HTML;
$html = \backend\helpers\HtmlHelpers::trimmedHtml($html);

$script = <<<JS
$(document).on('click', '#logout_id', (function () {
    //eliminamos la empresa actual del localstorage
    localStorage.removeItem("core-empresa-actual");
    $.post(
        "$urlDelete",
        function () {
        }
    )
}));

$(document).on('click', '#empresa_actual_id', (function () {
    $.get(
        "$url",
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal();
            $('.modal-header').css('background', '#3c8dbc'); //Color del header
            $('.modal-title').html("Seleccione una empresa para trabajar"); // Título para el header del modal
        }
    )
}));

$(document).on('click', '#edit-current-empresa', (function () {
    if ("$empresa_actual_id" === "") {
        alert("Aún no se ha especificado una empresa actual.");
        return false;
    }
    window.open($url_editEmpresaActual);
}));

function checkTasks() {
    //Si hay mensaje flash se muestra

    //$.pjax.reload({container:"#flash_message_id", async:false});

    // Verificación
    var empresa_header = document.getElementById("empresa_actual_id").getAttribute("empresa-id");
    var empresa_localStorage = localStorage.getItem('core-empresa-actual');
    let redirected = $redirected;

    if (!redirected && empresa_header !== empresa_localStorage && empresa_header !== null) {
        $(window).unbind("beforeunload");
        window.location.replace("index.php?redirected=1");
    }

    setTimeout(checkTasks, 10000);
}

checkTasks(); // Start task checking


//map = {
//    'usuarioempresa': 'usuario-empresa%2Findex',
//    'tipodoc': 'tipo-documento%2Findex',
//    'tipodocset': 'tipo-documento-set%2Findex',
//    'plancuenta': 'plan-cuenta%2Findex',
//    'ivacuenta': 'iva-cuenta%2Findex',
//    'activofijo': 'activo-fijo%2Findex',
//    'activobio': 'activo-biologico%2Findex',
//    'presentacioniva': 'presentacion-iva%2Findex',
//    'recibocompra': 'recibo%2Findex&operacion=compra',
//    'reciboventa': 'recibo%2Findex&operacion=venta',
//    'retencioncompra': 'retencion%2Findex&operacion=compra',
//    'retencionventa': 'retencion%2Findex&operacion=venta',
//    'plantillacompra': 'plantilla-compraventa%2Findex&tipo=compra',
//    'plantillaventa': 'plantilla-compraventa%2Findex&tipo=venta'
//};
//// $('input[name="q"]').on('keyup', function (evt) {
//let q = !('$q' in map) ? '$q' : map['$q'];
//let core_url = '$baseUrl' + '/index.php?r=' + q + ((q.indexOf('index') !== -1) ?  "" : '%2Findex'),
//    cont_url = '$baseUrl' + '/index.php?r=contabilidad%2F' + q + ((q.indexOf('index') !== -1) ?  "" : '%2Findex'),
//    core_link = $('a[href="' + core_url + '"]'),
//    cont_link = $('a[href="' + cont_url + '"]');
//
//if (core_link.length)
//    core_link[0].click();
//else if (cont_link.length)
//    cont_link[0].click();
//
//applyPopOver($('button[type="submit"][name="search"][class*="btn-flat"]'), 'Sugerencias', "$html", {'placement': 'right', 'theme-color': 'info-l'});
//
//$('button[type="submit"][name="search"][class*="btn-flat"]').mouseout(function() {
//   $('div.popover').remove(); 
//});
JS;
$this->registerJs($script);

$colors = [
    'info' => \backend\helpers\HtmlHelpers::InfoColorHex(),
    'success' => \backend\helpers\HtmlHelpers::SuccessColorHex(),
    'warning' => \backend\helpers\HtmlHelpers::WarningColorHex(),
    'danger' => \backend\helpers\HtmlHelpers::DangerColorHex(),
    'primary' => \backend\helpers\HtmlHelpers::PrimaryColorHex(),
    'info-l' => \backend\helpers\HtmlHelpers::InfoColorHex(true),
    'success-l' => \backend\helpers\HtmlHelpers::SuccessColorHex(true),
    'warning-l' => \backend\helpers\HtmlHelpers::WarningColorHex(true),
    'danger-l' => \backend\helpers\HtmlHelpers::DangerColorHex(true),
    'primary-l' => \backend\helpers\HtmlHelpers::PrimaryColorHex(true),
];
$colors = Json::encode($colors);
$script_header = <<<JS
/**
 * 
 * @param {jquery} element Elemento JQuery sobre el cual se aplica el PopOver.
 * @param {string} title Titulo del PopOver.
 * @param {html|string} content Contenido del PopOver. Puede ser un contenido Html.
 * @param {Object} properties Propiedades de estilizacion visual. Ejemplos de este parametro:
 * 
 *  {
 *      title-css': {
 *          'background-color': '#ffffff',
 *          'color': '#ffffff',
 *          'text-align': 'center|left|right',
 *          'font-weight': 'bold|italic'
 *      },
 *      'theme-color': '#ffffff',
 *      'html': true|false,
 *      'container': 'body|...', // normalmente body, pero para otros casos, no se ha probado bien.
 *      'placement': 'top|bottom|left|right',
 *      'popover-css': {
 *          'max-width': '30%',
 *          ... any other config properties menctioned in official bootstrap popover but not tested.
 *      }
 *  }
 */
var applyPopOver = function (element, title = 'Default Title', content = 'Default Contents...', properties = {}) {
    // let cssTitleDefault = new Object({'background-color': '#d9edf7', 'color': '#31708f', 'text-align': 'center', 'font-weight': 'bold'}); // default de yii2
    let cssTitleDefault = new Object({
        'background-color': ('theme-color' in properties) ? ((properties['theme-color'] in $colors) ? ('#' + ($colors)[properties['theme-color']]) : '#5cb85c') : '#5cb85c',
        'color': '#ffffff',
        'text-align': 'center',
        'font-weight': 'bold'
    }); // default personalizado en verde
    let popOverConfigs = {'title': title, 'content': content}; // setea title y content si o si.
    Object.entries(properties).forEach(([key, value]) => {      // agrega propiedades recibidas en properties.
        popOverConfigs[key] = value;
    });
    let defaultTemplate = ['<div class="popover" role="tooltip">',
        '<div class="arrow"></div>',
        '<h3 class="popover-title"></h3>',
        '<div class="popover-content"></div>',
        '</div>',
        '</div>'].join('');
    element.popover({
        'title': popOverConfigs['title'],
        'content': popOverConfigs['content'],
        'html': ('html' in popOverConfigs) ? popOverConfigs['html'] : true,
        'placement': ('placement' in popOverConfigs) ? popOverConfigs['placement'] : 'bottom',
        'container': ('container' in popOverConfigs) ? popOverConfigs['container'] : 'body',
        'template': ('template' in popOverConfigs) ? popOverConfigs['template'] : defaultTemplate,
    }).mouseover(function () {
        // cerrar primero los abiertos
        $('div.popover').each(function (e) {
            $(this).remove();
        });

        // abrir este actual
        $(this).popover('show');

        // aplicar css
        if ('title-css' in popOverConfigs)
            $(".popover-title").css(popOverConfigs['title-css']);
        else
            $(".popover-title").css(cssTitleDefault);

        if ('popover-css' in popOverConfigs)
            $(".popover").css(popOverConfigs['popover-css']);
    })/*.mouseout(function () {
        $(this).popover('hide');
    })*/;

    // Si el cliente quiere que se esconda con el mouseout, comentar toda esta linea
    // y reactivar la linea de mouseover original que esta comentado ahora.
    $(document).on('click', function (e) {
        // $.closest(selector) devuelve el padre mas cercano que cumple con el selector.
        if (!$(e.target).closest("div.popover").length) { // si se hizo click en area que no es el popover, cierra
            $('div.popover').each(function (e) {
                $(this).remove();
            });
        }
    }).on('keyup', function (evt) {
        if (evt.key === 'Escape')
            $('div.popover').each(function (e) {
                $(this).remove();
            });
    });
};

var setPopOver = function (element, title = 'Default Title', content = 'Default Contents...', html = true, placement = 'bottom', container = 'body', css = null) {
    let input_id = element.prop('id');
    element.popover({
        'title': title,
        'content': content,
        'html': html,
        'placement': placement,
        'container': container
    }).mouseover(function () {
        $(this).popover('show');
        if (typeof css !== 'undefined')
            $(".popover-title").css(css);
    }).mouseout(function () {
        $(this).popover('hide');
    });
};
JS;
$this->registerJs($script_header, \yii\web\View::POS_HEAD);

if (class_exists('\backend\modules\contabilidad\MainModule')) {
    $script = <<<JS
// Para que el boton de ok gane foco, cuando se abre el modal de confirmacion.
// El modal de confirmacion, que es lanzado en los updates de compra, venta, notas, retenciones, recibos.
$(document).on('shown.bs.modal', function () {
    let confirmDialogOkBtn = $(this).find('.modal-footer .btn.btn-warning');
    if (confirmDialogOkBtn.length) {
        if (confirmDialogOkBtn[0].innerText.trim() === 'De acuerdo') {
            console.log('es modal de confirmacion y hay el boton de ok.');
            confirmDialogOkBtn.trigger('focus');
        }
    }
});
JS;
    $this->registerJs($script);
}

?>

<script type='text/javascript'>

</script>
