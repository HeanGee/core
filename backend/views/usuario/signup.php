<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model SignupForm */

use common\models\SignupForm;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use common\models\User as Usuario;
use kartik\builder\Form;
use common\helpers\PermisosHelpers;

$this->title = 'Nuevo Usuario';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <p>Por favor, complete los siguientes campos para crear el usuario:</p>

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?><!--  Desabilita el botón de submit-->
    <?php $form = ActiveForm::begin(
        [
            'id' => 'form-signup',
            'options' => ['class' => 'disable-submit-buttons'],
        ]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'username' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Usuario', 'autofocus' => true]],
            'email' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Correo']],
        ]
    ]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'rol_id' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::class,
                'options' => [
                    'data' => Usuario::getRolLista(),
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno'
                    ]
                ],
                'label' => 'Rol',
                'hint' => 'Rol',
            ],
        ]
    ]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'password' => [
                'type' => Form::INPUT_PASSWORD, 'options' => ['placeholder' => 'Contraseña'],
                'label' => 'Contraseña'
            ],
        ]
    ]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'password_repeat' => [
                'type' => Form::INPUT_PASSWORD, 'options' => ['placeholder' => 'Repita Contraseña'],
                'label' => 'Repita Contraseña'
            ],
        ]
    ]); ?>

    <div class="form-group">
        <?= PermisosHelpers::getAcceso('usuario-signup') ?
            Html::submitButton('Crear',
                [
                    'class' => 'btn btn-success',
                    'data' => [
                        'disabled-text' => 'Guardando ...'
                    ],
                    'name' => 'signup-button'
                ]
            ) : '' ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
