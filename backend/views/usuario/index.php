<?php

use common\helpers\PermisosHelpers;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <?php
    //    echo Collapse::widget([
    //        'items' => [
    //            // equivalente a lo de arriba
    //            [
    //                'label' => 'Buscar',
    //                'content' => $this->render('_search', ['model' => $searchModel]),
    //            // open its content by default
    //            //'contentOptions' => ['class' => 'in']
    //            ],
    //        ]
    //    ]);
    ?>
    <p>
        <?= PermisosHelpers::getAcceso('usuario-signup') ? Html::a('Nuevo Usuario', ['signup'], ['class' => 'btn btn-success']) : '' ?>
    </p>

    <?php
    $template = '';
    $template = $template . (PermisosHelpers::getAcceso('usuario-view') ? '{view} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('usuario-update') ? '{update} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('usuario-delete') ? '{delete} &nbsp&nbsp&nbsp' : '');
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'username',
                'format' => 'raw',
                'value' => function ($data) {
                    $url = Url::to(['usuario/view', 'id' => $data->id]);
                    return Html::a($data->username, $url);
                }
            ],
            [
                'attribute' => 'perfil_nombre',
                'format' => 'raw',
                'value' => function ($data) {
                    $url = Url::to(['perfil/view', 'id' => $data->perfilId]);
                    return Html::a($data->perfil ? $data->perfil->nombre : 'ninguno', $url);
                }
            ],
            'email:email',
            'rolNombre',
//            'tipoUsuarioNombre',
            'estadoNombre',
            'created_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'header' => 'Acciones',
            ],
            // 'updated_at',
        ],
    ]);
    ?>

</div>