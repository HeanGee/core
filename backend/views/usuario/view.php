<?php

use common\models\User;
use common\helpers\PermisosHelpers;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model user */

$this->title = $model->username;
$show_this_nav = PermisosHelpers::requerirMinimoRol('SuperUsuario');

$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p>

        <?php
        if (PermisosHelpers::getAcceso('usuario-update')) {
            echo Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
        }
        ?>

        <?php
        if (PermisosHelpers::getAcceso('usuario-delete')) {
            echo Html::a('Borrar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', '¿Está seguro de eliminar este usuario?'),
                    'method' => 'post',
                ],
            ]);
        }
        ?>

    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Perfil',
                'format' => 'raw',
                'valueColOptions' => ['style' => 'width:30%'],
                'value' => Html::a('ver infomación', \yii\helpers\Url::to(['perfil/view', 'id' => $model->id]))
            ],
            //'username',
            'email:email',
            'rolNombre',
            'estadoNombre',
            'created_at',
            'updated_at',
        ],
    ])
    ?>
    <h1>Actividad</h1>
</div>
