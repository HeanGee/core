<?php

use common\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\PermisosHelpers;

/**
 * @var yii\web\View $this
 * @var User $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="user-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= PermisosHelpers::getAcceso('usuario-cambiarpass') ? Html::a('Cambiar contraseña', ['/usuario/cambiarpass', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
    <br><br>
    <?php $form->field($model, 'username')->textInput(['maxlength' => 255, 'readonly' => 'readonly'])->label('Usuario'); ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => 255])->label('Correo electrónico') ?>
    <?= $form->field($model, 'rol_id')->dropDownList($model->rolLista, ['prompt' => 'Por Favor Elija Uno'])->label('Rol'); ?>
    <?= $form->field($model, 'estado_id')->dropDownList($model->estadoLista, ['prompt' => 'Por Favor Elija Uno']); ?>
    <br/>
    <?php
    if ($model->isNewRecord) {
        echo $form->field($model, 'nuevopass')->passwordInput(['placeholder' => 'Nueva contraseña', 'required' => 'required'])->label('Contraseña');
        echo $form->field($model, 'repetirpass')->passwordInput(['placeholder' => 'Repite la contraseña', 'required' => 'required'])->label('Repita Contraseña');
    }
    ?>

    <div class="form-group">
        <?= PermisosHelpers::getAcceso('usuario-update') ? Html::submitButton('Actualizar', ['class' => 'btn btn-success']) : '' ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>