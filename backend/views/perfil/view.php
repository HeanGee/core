<?php

use backend\models\Perfil;
use common\helpers\PermisosHelpers;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var Perfil $model
 */

$this->title = 'Usuario: ' . $model->usuario->username;
?>
<div class="profile-view">
    <p>
        <?php if (PermisosHelpers::getAcceso('perfil-update')) {
            echo Html::a('Actualizar', ['update', 'id' => $model->id],
                ['class' => 'btn btn-primary']);
        } ?>
        <?php if (PermisosHelpers::getAcceso('perfil-delete')) {
            echo Html::a('Borrar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Esta seguro que desea borrar este item?'),
                    'method' => 'post',
                ],
            ]);
        } ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            ['attribute' => 'userLink', 'format' => 'raw'],
            'nombre',
            'apellido',
            'fecha_nacimiento',
            'genero.genero_nombre',
            'created_at',
            'updated_at',
        ],
    ]) ?>
</div>