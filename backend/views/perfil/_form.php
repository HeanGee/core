<?php

use backend\models\Perfil;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\file\FileInput;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Perfil */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="perfil-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?><!--  Desabilita el botón de submit-->
    <?php $form = ActiveForm::begin(); ?>
    <?= \common\helpers\PermisosHelpers::getAcceso('perfil-cambiarpass') ? Html::a('Cambiar contraseña', ['/perfil/cambiarpass', 'id' => $model->usuario_id], ['class' => 'btn btn-danger']) : '' ?>
    <br><br>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'nombre' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Nombre', 'autofocus' => true]],
            'apellido' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Apeliido']],
        ]
    ]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'fecha_nacimiento' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => DateControl::class,
                'hint' => 'Ingrese fecha de nacimiento (dd/mm/aaaa)',
                'options' => [
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
                    'language' => 'es',
                    'saveFormat' => 'php:Y-m-d H:i:s',
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                            'format' => 'php:d-m-Y'
                        ]
                    ],
                ]
            ],
            'genero_id' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::class,
                'options' => [
                    'data' => $model->generoLista,
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno'
                    ]
                ],
                'hint' => 'Seleccione género',
            ],
        ]
    ]); ?>
    <?php echo $form->field($model, 'imagen_avatar')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],

        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png'],
            'initialPreview' => !$model->isNewRecord ? [Perfil::getInitialPreviewImage($model->id) . $model->avatar] : '',
            'initialPreviewAsData' => true,
            'initialPreviewConfig' => [
                'caption' => $model->avatar,
            ],
            'previewFileType' => ['jpg', 'gif', 'png'],
            'maxFileCount' => 1,
            'uploadAsync' => false, // setting importante para que funcione el listener que está en el JS
            'uploadThumbUrl' => null,
            'showUploadedThumbs' => false,
            'msgFilesTooMany' => 'Solamente se puede levantar 1 archivo por vez...',
            'msgUploadThreshold' => 'Procesando...',
            'msgUploadBegin' => 'Inicializando...',
            'msgUploadEnd' => 'Éxito...',
            'dropZoneTitle' => 'Arrastre y suelte aquí su archivo...',
            'browseLabel' => 'Buscar',
            'removeTitle' => 'Remover archivo',
            'removeLabel' => 'Remover',
            'uploadLabel' => 'Subir',
            'msgPlaceholder' => 'Seleccione un archivo...',
            'msgZoomTitle' => 'Vista en detalle',
            'msgZoomModalHeading' => 'Vista en Detalle',
            'showRemove' => false,
            'showUpload' => false,
            'fileActionSettings' => [
                'zoomTitle' => 'Vista en detalle',
                'removeTitle' => 'Remover archivo',
            ],
            'previewZoomButtonTitles' => [
                'toggleheader' => 'Sacar título',
                'fullscreen' => 'Pantalla completa',
                'close' => 'Cerrar',
                'borderless' => 'Modo sin bordes'
            ],
        ],
    ])->label('Avatar'); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

    <?php
    $isNewRecord = 0;
    if ($model->isNewRecord) {
        $isNewRecord = 1;
    }
    $script = <<<JS
$(document).ready(function () {
    document.getElementsByClassName("kv-file-remove")[0].style = "display:none";
    if (!$isNewRecord) {
        document.getElementsByClassName('file-caption-name')[0].value = "$model->avatar"
    }
});
JS;
    $this->registerJs($script);

    ?>
</div>