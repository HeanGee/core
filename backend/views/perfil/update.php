<?php

use backend\models\Perfil;

/* @var $this yii\web\View */
/* @var $model Perfil */

$this->title = 'Actualizar Perfil: ' . $model->getNombreCompleto();
?>
<div class="perfil-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
