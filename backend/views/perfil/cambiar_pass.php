<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model CambioPassForm */
/* @var $id integer */
/* @var $username string */

use common\models\CambioPassForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Cambio de contraseña: '.$username;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-signup">
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <?= $form->errorSummary($model) ?>
<!--            <?//= $form->field($model, 'password_old')->passwordInput(['required'=>'required'])->label("Contraseña actual") ?>-->
            <?= $form->field($model, 'password')->passwordInput()->label("Contraseña") ?>
            <?= $form->field($model, 'password_repeat')->passwordInput()->label("Repita la contraseña") ?>
            <div class="form-group">
                <?= Html::submitButton('Cambiar la contraseña', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
