<?php


/* @var $this yii\web\View */
/* @var $model backend\models\Perfil */

$this->title = Yii::t('app', 'Crear Perfil');
?>
<div class="perfil-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
