<?php

use common\helpers\PermisosHelpers;
use yii\bootstrap\Collapse;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \backend\models\search\PerfilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Perfiles';
?>
<div class="profile-index">

    <?php echo Collapse::widget([

        'items' => [
            // equivalent to the above
            [
                'label' => 'Search',
                'content' => $this->render('_search', ['model' => $searchModel]),
                // open its content by default
                //'contentOptions' => ['class' => 'in']
            ],
        ]
    ]);

    $template = '';
    $template = $template . (PermisosHelpers::getAcceso('perfil-view') ? '{view} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('perfil-update') ? '{update} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('perfil-delete') ? '{delete} &nbsp&nbsp&nbsp' : '');
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            ['attribute' => 'perfilIdLink', 'format' => 'raw'],
            ['attribute' => 'userLink', 'format' => 'raw'],
            'nombre',
            'apellido',
            'fecha_nacimiento',
            'generoNombre',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'header' => 'Acciones',
            ],

            // 'created_at',
            // 'updated_at',
            // 'user_id',


        ],
    ]); ?>

</div>