<?php

use backend\models\Departamento;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\Barrio */
/* @var $form kartik\form\ActiveForm */
?>

<div class="barrio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'autoGenerateColumns' => false,
                'columns' => 6,
                'attributes' => [
                    'nombre' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => ['placeholder' => 'Nombre de barrio'],
                        'columnOptions' => ['colspan' => '3']
                    ],
                    'active' => [
                        'type' => Form::INPUT_WIDGET,
                        'columnOptions' => ['colspan' => '3'],
                        'widgetClass' => 'kartik\select2\Select2',
                        'options' => [
                            'data' => [1 => 'Activo', 0 => 'Inactivo'],
                            'pluginOptions' => [
                                'allowClear' => false,
                                'value' => 0,
                            ],
                        ]
                    ]
                ]
            ],
            [
                'attributes' => [
                    'departamento_id' => [
                        'type' => Form::INPUT_RAW,
                        'value' => $form->field($model->ciudad == NULL ? $model : $model->ciudad, 'departamento_id')->widget(Select2::className(), [
                            'data' => Departamento::getDepartamentoLista(true),
                            'options' => ['placeholder' => 'Seleccione un Departamento ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            'pluginEvents' => [
                                'change' => "function(){
                                    $('#barrio-ciudad_id').val('').trigger('change');
                                }"
                            ]
                        ])
                    ],
                    'ciudad_id' => [
                        'type' => Form::INPUT_RAW,
                        'value' => $form->field($model, 'ciudad_id')->widget(Select2::className(), [
                            'options' => ['placeholder' => 'Seleccione una Ciudad ...'],
                            'initValueText' => !empty($model->ciudad->nombre) ? $model->ciudad->nombre : '',
                            'pluginOptions' => [
                                'allowClear' => true,
                                'ajax' => [
                                    'url' => Url::to(['ciudad/get-ciudad-lista']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression("function(params) { return {q:params.term,id:$('#barrio-departamento_id').val()}; }")
                                ]
                            ]
                        ])
                    ]
                ]
            ],
            [
                'attributes' => [
                    'codigo_set' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => ['placeholder' => 'Código Set'],
                        'columnOptions' => ['colspan' => '3']
                    ],
                ]
            ],
            [
                'attributes' => [
                    'action' => [
                        'type' => Form::INPUT_RAW,
                        'value' => '<div style="margin-top: 20px">' .
                        \common\helpers\PermisosHelpers::getAcceso('barrio-create') ? Html::submitButton('Guardar', ['class' => 'btn btn-primary']) : '' .
                            '</div>'
                    ]
                ]
            ]
        ]
    ]) ?>

    <?php ActiveForm::end(); ?>

</div>
