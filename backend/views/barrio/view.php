<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\PermisosHelpers;

/* @var $this yii\web\View */
/* @var $model backend\models\Barrio */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Barrios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barrio-view">


    <p>
        <?= PermisosHelpers::getAcceso('barrio-update') ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= PermisosHelpers::getAcceso('barrio-delete') ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'nombre',
            [
                'attribute' => 'ciudad_id',
                'value' => $model->ciudad->nombre
            ],
            [
                'attribute' => 'departamento_id',
                'value' => $model->ciudad->getDepartamentoNombre()
            ],
            [
                'label' => 'Estado',
                'attribute' => 'active',
                'value' => ($model->active == 1 ? 'Activo' : 'Inactivo')
            ],
            'codigo_set'
        ],
    ]) ?>

</div>
