<?php

use backend\models\search\BarrioSearch;
use common\helpers\PermisosHelpers;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel BarrioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Barrios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barrio-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= PermisosHelpers::getAcceso('barrio-create') ? Html::a('Crear Barrio', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>

    <?php
    $template = '';
    $template = $template . (PermisosHelpers::getAcceso('barrio-view') ? '{view} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('barrio-update') ? '{update} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('barrio-delete') ? '{delete} &nbsp&nbsp&nbsp' : '');
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'nombre',
            [
                'label' => 'Ciudad',
                'attribute' => 'ciudad',
                'value' => 'ciudad.nombre'
            ],
            [
                'label' => 'Departamento',
                'attribute' => 'departamento',
                'value' => 'ciudad.departamento.nombre'
            ],
            //'active',
            [
                'label' => 'Estado',
                'attribute' => 'active',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    return $model->active == 1 ? 'Activo' : 'Inactivo';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [1 => 'Activo', 0 => 'Inactivo', 3 => 'Todos'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => false],
                ],
                'filterInputOptions' => ['placeholder' => 'Filtro de Estado'],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'header' => 'Acciones',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
