<?php

/* @var $this yii\web\View */
/* @var $model backend\models\Barrio */

$this->title = 'modificar Barrio: '.$model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Barrios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="barrio-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
