<?php


/* @var $this yii\web\View */
/* @var $model backend\models\Barrio */

$this->title = 'Crear Barrio';
$this->params['breadcrumbs'][] = ['label' => 'Barrios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barrio-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
