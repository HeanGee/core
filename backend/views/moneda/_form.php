<?php

use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use common\helpers\PermisosHelpers;

/* @var $this yii\web\View */
/* @var $model backend\models\Moneda */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="moneda-form">

    <?php $form = ActiveForm::begin(); ?>
    <?=
    FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'autoGenerateColumns' => true,
                'columns' => 12,
                'attributes' => [
                    'simbolo' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => ['placeholder ' => 'Símbolo'],
                    ],
                    'nombre' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => ['placeholder ' => 'Nombre'],
                    ],
                ]
            ],
            [
                'attributes' => [
                    'plural' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => ['placeholder ' => 'Plural'],
                    ],
                    'tiene_decimales' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => SwitchInput::className(),
                        'hint' => 'Cheque al portador?',
                        'options' => [
                            'pluginOptions' => [
                                'onText' => 'Si',
                                'offText' => 'No',
                                'offColor' => 'danger',
                                'size' => 'small',
                                'handleWidth' => '60',
                                'labelWidth' => '60',
                            ]
                        ]
                    ],
                ]
            ]
        ]
    ]);
    ?>

    <div class="form-group">
        <?= PermisosHelpers::getAcceso('moneda-create') ? Html::submitButton($model->isNewRecord ? Yii::t('app', 'Guardar') : Yii::t('app', 'Guardar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) : '' ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
