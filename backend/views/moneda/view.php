<?php

use kartik\detail\DetailView;
use yii\helpers\Html;
use common\helpers\PermisosHelpers;

/* @var $this yii\web\View */
/* @var $model backend\models\Moneda */

$this->title = 'Moneda: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monedas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="moneda-view">

    <p>
        <?= Html::a(Yii::t('app', 'Ir a Monedas'), ['index'], ['class' => 'btn btn-info']) ?>
        <?= PermisosHelpers::getAcceso('moneda-create') ? Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= PermisosHelpers::getAcceso('moneda-create') ? Html::a(Yii::t('app', 'Borrar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'condensed' => true,
        'hover' => true,
        'mode' => DetailView::MODE_VIEW,
        'enableEditMode' => false,
        'fadeDelay' => true,
        'panel' => [
            'heading' => "Datos",
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'simbolo',
            'nombre',
            'plural',
            [
                'attribute' => 'tiene_decimales',
                'format' => 'raw',
                'value' => '<label class="label label-' . (($model->tiene_decimales == 'si') ? 'success">Sí' : 'danger">No') . '</label>',
                'valueColOptions' => ['style' => 'width:30%'],
            ],
        ],
    ]) ?>

</div>
