<?php

use common\helpers\PermisosHelpers;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MonedaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Monedas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="moneda-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= PermisosHelpers::getAcceso('moneda-create') ? Html::a(Yii::t('app', 'Nueva Moneda'), ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>
    <?php
    $template = '';
    $template = $template . (PermisosHelpers::getAcceso('moneda-view') ? '{view} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('moneda-update') ? '{update} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('moneda-delete') ? '{delete} &nbsp&nbsp&nbsp' : '');
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'hover' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'simbolo',
            'nombre',
            'plural',
            [
                'attribute' => 'tiene_decimales',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<label class="label label-' . (($model->tiene_decimales == 'si') ? 'success">Sí' : 'danger">No') . '</label>';
                },
                'contentOptions' => ['style' => 'text-align:center;'],
                'label' => 'Tiene Decimales',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'tiene_decimales',
                    'data' => ['' => 'Todos', 'si' => 'SI', 'no' => 'NO'],
                    'hideSearch' => true,
                    'pluginOptions' => [
//                        'width' => '120px'
                    ]
                ])
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'header' => 'Acciones',
            ],
        ],
    ]); ?>
</div>
