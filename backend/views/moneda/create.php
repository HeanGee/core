<?php


/* @var $this yii\web\View */
/* @var $model backend\models\Moneda */

$this->title = Yii::t('app', 'Nueva Moneda');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monedas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="moneda-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
