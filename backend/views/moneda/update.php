<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Moneda */

$this->title = 'Moneda: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monedas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="moneda-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
