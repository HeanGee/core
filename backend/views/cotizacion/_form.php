<?php

use backend\models\Moneda;
use common\helpers\PermisosHelpers;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\datecontrol\DateControl;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\builder\Form;
use yii\web\View;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Cotizacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cotizacion-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'disable-submit-buttons'],
    ]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'moneda_id' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::class,
                'options' => [
                    'data' => ArrayHelper::map(Moneda::find()->asArray()->all(), 'id', 'nombre'),
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                        'onchange' => '
                            if( document.getElementById(\'cotizacion-fecha\').value != "" ){
                                cargaValores();
                            }
                        '
                    ]
                ],
                'hint' => 'Seleccione una divisa',
            ],
            'fecha' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => DateControl::class,
                'hint' => 'Ingrese la fecha (dd/mm/aaaa)',
                'options' => [
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
                    'language' => 'es',
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                        ],
                        'options' => [
                            'onchange' => '
                                    cargaValores();
                                '
                        ]
                    ],

                ]
            ],
        ]
    ]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'compra' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => MaskedInput::class,
                'options' => [
                    'clientOptions' => [
                        'rightAlign' => false,
                        'alias' => 'decimal',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'autoGroup' => true
                    ],
                    'options' => [
                        'class' => 'form-control',
                    ]
                ],
                'label' => 'Compra'
            ],
            'venta' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => MaskedInput::class,
                'options' => [
                    'clientOptions' => [
                        'rightAlign' => false,
                        'alias' => 'decimal',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'autoGroup' => true
                    ],
                    'options' => [
                        'class' => 'form-control',
                    ]
                ],
                'label' => 'Venta'
            ],
        ]
    ]); ?>

    <div class="form-group">
        <?= PermisosHelpers::getAcceso('cotizacion-create') ? Html::submitButton('Guardar', ['class' => 'btn btn-success']) : '' ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    $this->registerJs('
    
        // Función que guarda de manera temporal los campos ya cargados de manera a no perderlos
        function cargaValores(){
            $.get("index.php?r=cotizacion/chequeo-existencia&moneda_id=" + 
            document.getElementById(\'cotizacion-moneda_id\').value + "&fecha=" +
            document.getElementById(\'cotizacion-fecha\').value, 
            function(data, status){
                if (data.data != null) {
                    document.getElementById(\'cotizacion-venta\').value = parseInt(data.data.venta);
                    document.getElementById(\'cotizacion-compra\').value = parseInt(data.data.compra);  
                } else {
                    document.getElementById(\'cotizacion-venta\').value = "";
                    document.getElementById(\'cotizacion-compra\').value = "";
                }
            }).fail(function() {
                console.log("Error al obtener datos de cotización.");
            });
            
            document.getElementById(\'cotizacion-venta\').disabled = false;
            document.getElementById(\'cotizacion-compra\').disabled = false;
        };
    ', View::POS_END);

    if ($model->isNewRecord) {
        $this->registerJs('
        // En caso de que se tenga algún valor cargado no se debe bloquear el campo
        if( document.getElementById(\'cotizacion-venta\').value != "" ){
            document.getElementById(\'cotizacion-venta\').disabled = false;
        }
        
        if( document.getElementById(\'cotizacion-compra\').value != "" ){
            document.getElementById(\'cotizacion-compra\').disabled = false;
        }
', View::POS_END);
    }
    ?>
</div>
