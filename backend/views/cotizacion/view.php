<?php

use common\helpers\PermisosHelpers;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Cotizacion */

$this->title = 'Cotización para: ' . $model->moneda->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Cotizaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cotizacion-view">

    <p>
        <?= PermisosHelpers::getAcceso('cotizacion-update') ? Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= PermisosHelpers::getAcceso('cotizacion-delete') ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'moneda.nombre',
            'compra',
            [
                'label' => 'Venta',
                'value' => $model->moneda->tiene_decimales == 'si' ? number_format($model->venta, 2, ',', '.') : number_format($model->venta, 0, ',', '.')
            ],
            [
                'label' => 'Compra',
                'value' => $model->moneda->tiene_decimales == 'si' ? number_format($model->compra, 2, ',', '.') : number_format($model->compra, 0, ',', '.')
            ],
            [
                'label' => 'Fecha',
                'value' => date('d-m-Y', strtotime($model->fecha))
            ],
        ],
    ]) ?>

    <?php
    $css = <<<CSS
table.detail-view th {
        width: 25%;
}
CSS;
    $this->registerCss($css);
    ?>
</div>
