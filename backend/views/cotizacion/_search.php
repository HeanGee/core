<?php

use kartik\datecontrol\DateControl;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CotizacionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cotizacion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'fecha' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => DateControl::class,
                'hint' => 'Ingrese la fecha (dd/mm/aaaa)',
                'options' => [
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
                    'language' => 'es',
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                        ]
                    ],

                ]
            ],
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Filtrar', ['class' => 'btn btn-primary']) ?>
<!--        <?//= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>-->
    </div>

    <?php ActiveForm::end(); ?>

</div>
