<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Cotizacion */

$this->title = 'Editar Cotización: ' . $model->moneda->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Cotizacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="cotizacion-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
