<?php

use common\helpers\PermisosHelpers;
use kartik\date\DatePicker;
use kartik\datecontrol\DateControl;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model \backend\models\CotizacionArchivo */
/* @var $form yii\widgets\ActiveForm */
/* @var $mensaje String */

$this->title = 'Carga de cotizaciones';
?>

<div class="movimiento-form">

    <?php $form = ActiveForm::begin(['id' => 'subir_arch_form', 'options' => ['enctype' => 'multipart/form-data']]);

    try {
        $submit = PermisosHelpers::getAcceso('cotizacion-createfile') ? Html::submitButton('Enviar', ['class' => 'btn btn-primary']) : '';
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'attributes' => [
                        'fecha' => [

                            'type' => Form::INPUT_RAW,
                            'value' => $form->field($model, 'fecha')->widget(DatePicker::className(), [
                                'options' => ['placeholder' => 'Fecha (MM-AAAA)'],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'startView' => 'months',
                                    'minViewMode' => 'months',
                                    'format' => 'mm-yyyy',
                                    'language' => 'es',
                                ],
                            ])
                        ],
                        'archivo' => [
                            'type' => Form::INPUT_FILE,
                            'value' => $form->field($model, 'archivo')->widget(FileInput::classname(),
                                [
                                    'language' => 'es',
                                    'pluginOptions' => ['showPreview' => false],
                                ]
                            ),
                            'label' => 'Archivo (.csv, .xls, .xlsx)'
                        ],
                    ],
                ],
                [
                    'attributes' => [
                        'action' => [
                            'type' => Form::INPUT_RAW,
                            'value' => '<div style="margin-top: 20px">' .
                                $submit .
                                '</div>'
                        ],
                    ],
                ]
            ]
        ]);
    } catch (Exception $e) {
    }
    ?>

    <?php ActiveForm::end(); ?>
    <?php
    if ($mensaje != "") {
        echo $mensaje;
    } else {
        ?>
        <br/>
        <h3>Documentos de interes</h3>
        <h4><a href="../views/cotizacion/archivos/ejemplo.xlsx">Ejemplo</a></h4>
    <?php } ?>
</div>
