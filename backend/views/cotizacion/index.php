<?php

use common\helpers\PermisosHelpers;
use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CotizacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cotizaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cotizacion-index">

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    $template = '';
    $template = $template . (PermisosHelpers::getAcceso('cotizacion-view') ? '{view} &nbsp&nbsp ' : '');
    $template = $template . (PermisosHelpers::getAcceso('cotizacion-update') ? '{update} &nbsp&nbsp ' : '');
    $template = $template . (PermisosHelpers::getAcceso('cotizacion-delete') ? '{delete}' : '');
    ?>
    <p>
        <?= PermisosHelpers::getAcceso('cotizacion-create') ? Html::a('Nueva Cotización', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'striped' => true,
        'hover' => true,
        'columns' => [
            [
                'attribute' => 'fecha',
                'group' => true,
                'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
                'groupedRow' => true,                    // move grouped column to a single grouped row
                'filter' => false,
                'groupEvenCssClass' => 'kv-grouped-row'
            ],
            'moneda.nombre',
            [
                'filter' => false,
                'attribute' => 'compra',
                'value' => function ($model) {
                    return $model->moneda->tiene_decimales == 'si' ? number_format($model->compra, 2, ',', '.') : number_format($model->compra, 0, ',', '.');
                },
            ],
            [
                'filter' => false,
                'attribute' => 'venta',
                'value' => function ($model) {
                    return $model->moneda->tiene_decimales == 'si' ? number_format($model->venta, 2, ',', '.') : number_format($model->venta, 0, ',', '.');
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'header' => 'Acciones',
            ],
        ],
    ]); ?>
</div>
