<?php

use backend\models\TipoUsuario;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model TipoUsuario */

$this->title = Yii::t('app', 'Crear Tipo de Usuario');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipos de Usuario'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-usuario-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
