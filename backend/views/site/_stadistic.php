<?php

use backend\assets\StadisticAsset;
use kartik\datecontrol\DateControl;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
use yiister\adminlte\components\AdminLTE;

/* @var $stadistic array */
/* @var $this View */

StadisticAsset::register($this);
?>

<div class="row">
    <div class="col-lg-3 col-xs-6">
        <?php
        echo '<label class="control-label">Birth Date</label>';
        echo DateControl::widget([
            'type' => DateControl::FORMAT_DATE,
            'ajaxConversion' => false,
            'widgetOptions' => [
                'pluginOptions' => [
                    'orientation' => 'bottom left',
                    'autoclose' => true,
                    'startView' => 'year',
                    'minViewMode' => 'months',
                ]
            ],
            'name' => 'month-gasto',
            'displayFormat' => 'php:M-Y',
            'saveFormat' => 'php:Y-m',
        ]);
        ?>
    </div>
</div>

<br/>

<?php Pjax::begin(['id' => 'chart-pjax']); ?>

<div class="row">
    <div class="col-lg-3 col-xs-6">
        <?=
        yiister\adminlte\widgets\InfoBox::widget(
            [
                "color" => AdminLTE::BG_AQUA,
                "icon" => "fa fa-dollar",
                "text" => "GRAVADAS AL 10%",
                "number" => "0",
                "filled" => true,
                "progress" => 0,
                "progressDescription" => "",
            ]);
        ?>
    </div>
    <div class="col-lg-3 col-xs-6">
        <?=
        yiister\adminlte\widgets\InfoBox::widget(
            [
                "color" => AdminLTE::BG_YELLOW,
                "icon" => "fa fa-dollar",
                "text" => "GRAVADAS AL 5%",
                "number" => "0",
                "filled" => true,
                "progress" => 0,
                "progressDescription" => "",
            ]
        )
        ?>
    </div>
    <div class="col-lg-3 col-xs-6">
        <?=
        yiister\adminlte\widgets\InfoBox::widget(
            [
                "color" => AdminLTE::BG_RED,
                "icon" => "fa fa-dollar",
                "text" => "Exentas",
                "number" => "0",
                "filled" => true,
                "progress" => 0,
                "progressDescription" => "",
            ]
        )
        ?>
    </div>
    <div class="col-lg-3 col-xs-6">
        <?=
        yiister\adminlte\widgets\InfoBox::widget(
            [
                "color" => AdminLTE::BG_GREEN,
                "icon" => "fa fa-dollar",
                "text" => "TOTAL    ",
                "number" => "0",
                "filled" => true,
                "progress" => 0,
                "progressDescription" => "",
            ]
        )
        ?>
    </div>
</div>

<div class="row prev">
    <div class="col-lg-3 col-xs-6">
        <?= $this->render('_boxes', [
            'box_color' => 'info',
            'box_type' => '',
            'iva' => '10%',
        ]) ?>
    </div>
    <div class="col-lg-3 col-xs-6">
        <?= $this->render('_boxes', [
            'box_color' => 'warning',
            'box_type' => '',
            'iva' => '5%',
        ]) ?>
    </div>
    <div class="col-lg-3 col-xs-6">
        <?= $this->render('_boxes', [
            'box_color' => 'danger',
            'box_type' => '',
            'iva' => '0%',
        ]) ?>
    </div>
    <div class="col-lg-3 col-xs-6">
        <?= $this->render('_boxes', [
            'box_color' => 'success',
            'box_type' => '',
            'iva' => 'Total',
        ]) ?>
    </div>
</div>

<?php Pjax::end(); ?>

<div class="row" style="horiz-align: center">
    <div class="col-lg-6 col-xs-6">
        <canvas id="myChart"></canvas>
    </div>
</div>

<script>
    var url = "<?= Url::to(['/site/get-stat']) ?>";
    var char_data_url = "<?= Url::to(['/site/get-chart-data']) ?>";
</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>