<?php

/* @var $box_color string */
/* @var $box_type string */
/* @var $iva string */

$box_color = $box_color ? $box_color : 'default';
$box_type = $box_type ? "box-{$box_type}" : '';
?>

<div class="box <?= $box_type ?> box-<?= $box_color ?>">
    <div class="box-header with-border">
        <h3 class="box-title">Default Box Example</h3>
        <div class="box-tools pull-right">
            <!-- Buttons, labels, and many other things can be placed here! -->
            <!-- Here is a label for example -->
            <span class="label label-<?= $box_color ?>"><?= $iva ?></span>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        The body of the box
    </div>
    <!-- /.box-body -->
    <!--    <div class="box-footer">-->
    <!--        The footer of the box-->
    <!--    </div>-->
    <!-- box-footer -->
</div>
