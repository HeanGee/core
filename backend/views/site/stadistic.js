$(document).ready(() => {
    render_cards();
    render_chart();
});

const render_chart = () => {
    $.ajax({
        type: 'get',
        url: char_data_url,
        success: (result) => {
            draw_chart(result);
        },
    });
};

const getRandomColor = () => {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
};

const draw_chart = (data) => {
    data = JSON.parse(data); console.log(getRandomColor());
    let ctx = document.getElementById('myChart').getContext('2d');
    let colors = [];
    for (var i = 1; i < 8; i++) colors.push(getRandomColor());
    let myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: data.x,
            datasets: [{
                label: 'Gasto Mensual',
                data: data.y,
                backgroundColor: colors,
                borderColor: colors,
                borderWidth: 1,
                lineTension: 0,
                pointRadius: 7,
                //pointStyle: 'triangle', //'circle','cross','crossRot','dash','line','rect','rectRounded','rectRot','star','triangle'
                fill: false,
            }],
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            onClick: function(c,i) {
                e = i[0];
                console.log(e._index);
                var x_value = this.data.labels[e._index];
                var y_value = this.data.datasets[0].data[e._index];
                console.log(x_value);
                console.log(y_value);

                $('#w5-disp').val(`${x_value}-${new Date().getFullYear()}`).trigger('change');
            },
        }
    });
};

const on_click_bar_event = () => {};

$('input[name="month-gasto"]').on('change', function(evt) {
    render_cards(this.value);
});

const render_cards = (month) => {
    $.ajax({
        type: 'get',
        url: url,
        data: {
            as_json: true,
            month: month,
        },
        success: (result) => {
            update_plays(process_result(result));
        },
    });
};

const process_result = (result) => {
    let tax_5 = 0.0, tax_10 = 0.0, grav_5 = 0.0, grav_10 = 0, exent = 0.0, porcentaje = 0, imp = 0.0, grav = 0.0,
        monto = 0.0,
        result_data = {prev: null, curr: null},
        total;

    let data = JSON.parse(result);  // data has two keys: 'curr-month' and 'prev-month'
    console.log(data);

    // Prev month
    data['prev-month'].forEach((element, key) => {
        porcentaje = element['porcentaje'];
        grav = element['grav'];
        imp = element['imp'];
        monto = element['monto'];
        switch (porcentaje) {
            case '5':
                tax_5 += parseFloat(imp);
                grav_5 += parseFloat(grav);
                break;
            case '10':
                tax_10 += parseFloat(imp);
                grav_10 += parseFloat(grav);
                break;
            case null:
                exent += parseFloat(monto);
                break;
        }
    });

    total = tax_5 + tax_10 + grav_5 + grav_10 + exent;
    result_data['prev'] = {
        'tax 5': tax_5,
        'grav 5': grav_5,
        'tax 10': tax_10,
        'grav 10': grav_10,
        'exent': exent,
        'total': tax_10 + tax_5 + grav_10 + grav_5 + exent,
        'total tax': tax_5 + tax_10,
        'total grav': grav_5 + grav_10,
        '% 5': (tax_5 + grav_5) / total * 100,
        '% 10': (tax_10 + grav_10) / total * 100,
        '% ext': exent / total * 100,
        '% total': monto ? 100 : 0,
    };

    // Current month
    tax_5 = 0.0, tax_10 = 0.0, grav_5 = 0.0, grav_10 = 0, exent = 0.0, porcentaje = 0, imp = 0.0, grav = 0.0, monto = 0.0;
    data['curr-month'].forEach((element, key) => {
        porcentaje = element['porcentaje'];
        grav = element['grav'];
        imp = element['imp'];
        monto = element['monto'];
        switch (porcentaje) {
            case '5':
                tax_5 += parseFloat(imp);
                grav_5 += parseFloat(grav);
                break;
            case '10':
                tax_10 += parseFloat(imp);
                grav_10 += parseFloat(grav);
                break;
            case null:
                exent += parseFloat(monto);
                break;
        }
    });

    total = tax_5 + tax_10 + grav_5 + grav_10 + exent;
    result_data['curr'] = {
        'tax 5': tax_5,
        'grav 5': grav_5,
        'tax 10': tax_10,
        'grav 10': grav_10,
        'exent': exent,
        'total': tax_10 + tax_5 + grav_10 + grav_5 + exent,
        'total tax': tax_5 + tax_10,
        'total grav': grav_5 + grav_10,
        '% 5': monto ? (tax_5 + grav_5) / total * 100 : 0,
        '% 10': monto ? (tax_10 + grav_10) / total * 100 : 0,
        '% ext': monto ? exent / total * 100 : 0,
        '% total': monto ? 100 : 0,
    };

    return result_data;
};

const update_plays = (data) => {
    // Current cards
    let box_5 = $('div.bg-yellow'), box_10 = $('div.bg-aqua'), box_total = $('div.bg-green'), box_ext = $('div.bg-red');

    box_10.find('span.info-box-number').text(nf(data['curr']['grav 10'] + data['curr']['tax 10']));
    box_10.find('div.progress-bar').css('width', `${data['curr']['% 10']}%`);
    box_10.find('span.progress-description').text("con " + nf(data['curr']['tax 10']) + " Gs. de IVA");

    box_5.find('span.info-box-number').text(nf(data['curr']['grav 5'] + data['curr']['tax 5']));
    box_5.find('div.progress-bar').css('width', `${data['curr']['% 5']}%`);
    box_5.find('span.progress-description').text("con " + nf(data['curr']['tax 5']) + " Gs. de IVA");

    box_ext.find('span.info-box-number').text(nf(data['curr']['exent']));
    box_ext.find('div.progress-bar').css('width', `${data['curr']['% ext']}%`);
    box_ext.find('span.progress-description').text("--");

    box_total.find('span.info-box-number').text(nf(data['curr']['total grav'] + data['curr']['total tax'] + data['curr']['exent']));
    box_total.find('div.progress-bar').css('width', `${data['curr']['% total']}%`);
    box_total.find('span.progress-description').text("con " + nf(data['curr']['total tax'])  + " Gs. de IVA");


    // Prev cards
    box_5 = $('div.box-warning'), box_10 = $('div.prev').find('div.box-info'),
    box_total = $('div.box-success'), box_ext = $('div.box-danger');

    box_10.find('h3.box-title').text(nf(data['prev']['grav 10'] + data['prev']['tax 10']));
    box_10.find('div.box-body').text("con " + nf(data['prev']['tax 10']) + " Gs. de IVA");

    box_5.find('h3.box-title').text(nf(data['prev']['grav 5'] + data['prev']['tax 5']));
    box_5.find('div.box-body').text("con " + nf(data['prev']['tax 5']) + " Gs. de IVA");

    box_ext.find('h3.box-title').text(nf(data['prev']['exent']));
    box_ext.find('div.box-body').text("--");

    box_total.find('h3.box-title').text(nf(data['prev']['total grav'] + data['prev']['total tax'] + data['prev']['exent']));
    box_total.find('div.box-body').text("con " + nf(data['prev']['total tax'])  + " Gs. de IVA");
};

const nf = (number) => {
    return new Intl.NumberFormat("de-DE").format(Math.round(number));
};
