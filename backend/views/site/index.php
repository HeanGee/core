<?php

use yii\helpers\Url;
use kartik\helpers\Html;
use kartik\grid\GridView;
use yiister\adminlte\widgets\Box;
use common\helpers\PermisosHelpers;

/**
 * @var yii\web\View $this
 * @var $stadistic array
 */
$this->title = 'Administracion de sistema';
?>

<?php
$es_admin = PermisosHelpers::requerirMinimoRol('Admin');

echo "<div class='row'>";
// <!--ADMINISTRACIÓN-->
// <!--Solo se agrega si el modulos está presente.-->
//Cheques a cobrar en el sistema de ADMINISTRACIÓN.
/** @var \yii\data\ArrayDataProvider $chequesVencerProvider */
if (class_exists('\backend\modules\administracion\MainModule') &&
    PermisosHelpers::getAcceso('administracion-cheque-cliente-realizar-cobro')) {
    $chequeVencerData = \backend\modules\administracion\models\ChequeCliente::getChequesCobrarDashboard();
    echo $this->renderFile($chequeVencerData[0], ['chequesVencerProvider' => $chequeVencerData[1]]);
}
echo "</div>";

$cards = [];
if ($dataProvider !== null) {
	ob_start();
    Box::begin([
        "header" => "Notificaciones",
	    "type" => Box::TYPE_INFO,
        "filled" => true,
    ]);
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'rowOptions' => function ($model) {
                if ($model->estado == 'Vencido') {
                    return ['class' => 'danger'];
                }
                return [];
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'tipo',
                [
                    'attribute' => 'nro_factura',
                    'format' => 'raw',
                    'value' => function ($model) {
                        if (class_exists('\backend\modules\contabilidad\MainModule')) {
                            $url = ($model->tipo == 'Compra') ?
                                Url::to(['contabilidad/compra/view', 'id' => $model->id]) :
                                Url::to(['contabilidad/venta/view', 'id' => $model->id]);
                            return Html::a($model->nro_factura, $url);
                        }
                        return $model->nro_factura;
                    }
                ],
	            [
		            'attribute' => 'razon_social',
		            'format' => 'raw',
		            'value' => function ($model) {
			            if (class_exists('\backend\modules\contabilidad\MainModule')) {
				            $url = Url::to(['contabilidad/entidad/view', 'id' => $model->entidad_id, 'target' => '_blank']);
				            return Html::a($model->razon_social, $url);
			            }
			            return $model->razon_social;
		            },
	            ],
                'estado',
                'fecha_vencimiento:date',
	            [
		            'attribute' => 'saldo',
		            'format' => ['decimal', 0],
		            'hAlign' => 'right',
	            ],
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    Box::end();
    $cards[] = ob_get_clean();
}

// CONTABILIDAD
if (class_exists("\backend\modules\contabilidad\MainModule")) {
    $dataForRender = \backend\modules\contabilidad\models\Prestamo::getDataToRenderFromCore();
    if ($dataForRender['query']->exists()) {
        $today = date("Y-m-d");
        $lastDate = date('Y-m-t');
        $slices = explode('-', $today);
        $slices2 = explode('-', $lastDate);
        $day = (int)$slices[2];
        $lastDay = (int)$slices2[2];
        if ($day <= 5 || $lastDay == $day) {
            ob_start();
            echo $this->renderFile($dataForRender['view'], ['query' => $dataForRender['query']]);
            $cards[] = ob_get_clean();
        }
    }
}

$cant_card = sizeof($cards);
$rows = ceil($cant_card / 3);
$last_card = $cant_card % 3;
$col = ($cant_card == 0 ? (12 / 3) : (12 / $cant_card));
$last_card_col = ($last_card > 0) ? (12 / $last_card) : 12;
$i = 1;
$row_clossed = true;
foreach ($cards as $k => $card) {
    if ($i <= $rows && $row_clossed) {
        $row_clossed = false;
        echo "<div class='row'>";
        $i++;
    }
    if ($k < ($cant_card - $last_card))
        echo "<div class='col-lg-{$col} col-xs-6'>";
    else {
        echo "<div class='col-lg-{$last_card_col} col-xs-6'>";
    }
    echo $card;
    echo "</div>";
    if (($k + 1) % 3 == 0) {
        $row_clossed = true;
        echo "</div>";
    }
}

if (!$row_clossed) {
    echo "</div>";
}

if (class_exists('\backend\modules\calendario\MainModule')) {
    echo backend\modules\calendario\helpers\CalendarioViewHelper::NotificationsForCalendario();
}

echo is_null($stadistic) ? '' : $this->render('_stadistic', ['stadistic' => $stadistic]);
?>
    <div class="body-content"></div>
<?php
$url = Url::to(['/session/set-empresa-actual']);

$script = <<<JS
$( document ).ready(function() {
     $.get(
        "$url",
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal();
            $('.modal-header').css('background', '#3c8dbc'); //Color del header
            $('.modal-title').html("Seleccione una empresa para trabajar"); // Título para el header del modal
        }
     )
});
JS;

if (Yii::$app->session->get('core_empresa_actual') === null) {
    $this->registerJs($script);
}

$js = <<<JS
$(document).on('click', '.tiene_modal', (function () {
    let boton = $(this);
    let title = boton.attr('title');
    let boton_id = boton.attr('id');
    let url = boton.data('url');
    $.get(
        url,
        function (data) {
            if (data !== "") {
                let modal = $(boton.data('target'));
                $('.modal-body', modal).html(data);
                $('.modal-header', modal).css('background', '#3c8dbc');
                $('.modal-title', modal).html(title);
                modal.modal();
            } else {
                $.pjax.reload({container: "#flash_message_id", async: false});
                $("#modal").modal("hide");
                $("modal-body").html("");
            }
        }
    );
}));
JS;

$this->registerJs($js);
?>