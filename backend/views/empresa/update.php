<?php

/* @var $this yii\web\View */
/* @var $model backend\models\Empresa */
/* @var $show_tab string */

$this->title = 'Modificar empresa '.$model->razon_social;
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="empresa-update">

    <!--h1><?php //echo Html::encode($this->title) ?></h1-->

    <?= $this->render('_form', [
        'model' => $model,
        'show_tab' => $show_tab
    ]) ?>

</div>
