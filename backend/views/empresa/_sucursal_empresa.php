<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 11/21/17
 * Time: 6:03 PM
 */

use backend\models\Sucursal;
use kartik\dialog\Dialog;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/* @var $this yii\web\View */

$sucursalesProvider = new ActiveDataProvider([
    'query' => Sucursal::find()->where("empresa_id = '{$model->id}'")->indexBy('id'),
    'pagination' => [
        'pageSize' => 20,
    ],
]);

echo GridView::widget([
    'dataProvider' => $sucursalesProvider,
    'pjax' => true,
    'pjaxSettings' => [
        'options' => [
            'id' => 'sucursales_grid'
        ],
        'loadingCssClass' => false,
    ],
//    'filterModel' => $searchModel,
    'panel' => [
        'type' => 'primary',
        'heading' => 'Sucursales'
    ],
    'striped' => true,
    'hover' => true,

    'columns' => [
        ['class' => \kartik\grid\SerialColumn::className()],

        //'id',
        'nombre',
        //'departamento_id_fk',
        [
            'label' => 'Ciudad',
            'attribute' => 'ciudad_id',
            'value' => 'ciudad.nombre'
        ],
        //'barrio_id',
        //'direccion',
        //'telefono',
        [
            'label' => 'Encargado',
            'attribute' => 'usuario_id',
            'value' => !empty($usuario) ? (!empty($usuario->perfil) ? $usuario->perfil->getNombreCompleto() : $usuario->username) : null
        ],
        [
            'class' => \kartik\grid\ActionColumn::className(),
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::button('<i class="glyphicon glyphicon-eye-open"></i>', [
                        'type' => 'button',
                        'title' => Yii::t('app', 'Ver Detalles'),
                        'class' => 'tiene_modal btn-sin-estilo',
                        'data-toggle' => 'modal',
                        'data-target' => '#modal',
                        'data-url' => Url::to(['view-sucursal', 'sucursalId' => $model->id]),
                        'data-pjax' => '0',
                        'data-title' => "Detalle de: <em><b>{$model->nombre}</b></em>"]);
                },
                'update' => function ($url, $model) {
                    return Html::button('<i class="glyphicon glyphicon-pencil"></i>', [
                        'type' => 'button',
                        'title' => Yii::t('app', 'Editar'),
                        'class' => 'tiene_modal btn-sin-estilo',
                        'data-toggle' => 'modal',
                        'data-target' => '#modal',
                        'data-url' => Url::to(['edit-sucursal', 'sucursalId' => $model->id]),
                        'data-pjax' => '0',
                        'data-title' => "Editar: <em><b>{$model->nombre}</b></em>"]);
                },
                'delete' => function ($url, $model) {
                    return Html::a(
                        Html::tag('i', '', ['class' => 'glyphicon glyphicon-trash', 'title' => 'Eliminar Sucursal']),
                        Url::to(['delete-sucursal', 'sucursalId' => $model->id]),
                        [
                            'id' => 'sucursal-delete-action',
                            'data' => [
                                'pjax' => 0
                            ]
                        ]);
                }
            ]
        ]
    ],
    'toolbar' => [
        [
            'content' => Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                'id' => 'popup_nueva_sucursal_id',
                'type' => 'button',
                'title' => Yii::t('app', 'Agregar Sucursal'),
                'class' => 'btn btn-success tiene_modal',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => Url::to(['add-sucursal', 'empresaId' => $model->id]),
                'data-pjax' => '0',
                'data-title' => 'Agregar'])
        ],
    ]
]);

?>

<?php

// TODO: Eliminar en revisiones futuras.
//    Modal::begin([
//        'id' => 'modal-sucursal',
//        'options' => ['tabindex' => false],
//        'header' => '<h4 style="color: white; font-family: Helvetica Neue,Helvetica,Arial,sans-serif" class="modal-title">Modal</h4>',
//    ]);
//    echo "<div class='well'></div>";
//
//    Modal::end();

    echo Dialog::widget();

    $script =
<<<JS
        $(document).on('click', '.tiene_modal', (function() {
            var boton = $(this);
            var title = boton.data('title');
            $.get(
                boton.data('url'),
                function (data) {
                    var modal = $(boton.data('target'));
                    $('.modal-body', modal).html(data);
                    modal.modal();
                    $('.modal-header', modal).css('background','#3c8dbc');
                    if(title)
                        $('.modal-title', modal).html(title);
                }
            );
        }));
        $(document).on('click', '#sucursal-delete-action', function(e) {
            var delete_btn = $(this);
            krajeeDialog.confirm("¿Está seguro de eliminar esta sucursal?", function (result) {
                if (result) {
                    $.post(
                        delete_btn.attr('href')
                    )
                    .done(function(result) {
                        $.pjax.reload({container:"#sucursales_grid", async:false});
                        $.pjax.reload({container:"#flash_message_id", async:false});
                    });
                }
            });
            e.preventDefault();
            e.stopImmediatePropagation();
            return false;
        });
JS;
    $this->registerJs($script);

    $css = "
    .btn-sin-estilo {
        border: 0;
        padding: 0 1px;
        background-color: inherit;
        color: #337ab7;
    }
    .btn-sin-estilo:hover, 
    .btn-sin-estilo:active, 
    .btn-sin-estilo:focus { color: #72afd2; }
    ";
    $this->registerCss($css);

?>