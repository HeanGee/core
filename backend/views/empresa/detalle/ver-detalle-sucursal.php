<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 11/21/17
 * Time: 8:15 PM
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $modelo_sucursal backend\models\Sucursal */
?>
<div class="sucursal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $no_definido = '<span class="not-set">(no definido)</span>';

    echo DetailView::widget([
        'model' => $modelo_sucursal,
        'attributes' => [
            //'id',
            'nombre',
            [
                'attribute' => 'usuario_id',
                'format' => 'raw',
                'value' => !empty($modelo_sucursal->encargado) ? (!empty($modelo_sucursal->encargado->perfil) ? $modelo_sucursal->encargado->perfil->getNombreCompleto() : $modelo_sucursal->encargado->username) : $no_definido
            ],
            [
                'attribute' => 'departamento_id',
                'format' => 'raw',
                'value' => !empty($modelo_sucursal->departamento->nombre) ? $modelo_sucursal->departamento->nombre : $no_definido
            ],
            [
                'attribute' => 'ciudad_id',
                'format' => 'raw',
                'value' => !empty($modelo_sucursal->ciudad->nombre) ? $modelo_sucursal->ciudad->nombre : $no_definido
            ],
            [
                'attribute' => 'barrio_id',
                'format' => 'raw',
                'value' => !empty($modelo_sucursal->barrio->nombre) ? $modelo_sucursal->barrio->nombre : $no_definido
            ],
            'direccion',
            'telefono',
            'numero_patronal_ips',
            'numero_patronal_mtess',
            'horario_lunes',
            'horario_martes',
            'horario_miercoles',
            'horario_jueves',
            'horario_viernes',
            'horario_sabados',
            'horario_domingos',
        ],
    ]);

    echo $this->render('../_auditoria_empresa', ['modelo' => $modelo_sucursal]);

    ?>

</div>
