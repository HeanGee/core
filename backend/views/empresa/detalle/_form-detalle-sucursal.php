<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 11/21/17
 * Time: 6:25 PM
 */

use backend\models\Departamento;
use backend\models\Empresa;
use common\models\User;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\Sucursal */
/* @var $empresaId int */

?>
<div class="sucursal-empresa-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php

    /* valores por defecto para crear */
    $empresa_model = Empresa::findOne($empresaId);

    $form = ActiveForm::begin([
        'id' => 'sucursal_detalle_form',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]); ?>

    <?= FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows'=>[
            [
                'attributes' => [
                    'nombre' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => ['placeholder '=> 'Nombre de la sucursal...'],
                    ],
                    'usuario_id' => [
                        'type' => Form::INPUT_RAW,
                        'value' => $form->field($model, 'usuario_id')->widget(Select2::className(), [
                            'data' => \yii\helpers\ArrayHelper::map(User::find()->all(), 'id', 'username'),
                            'options' => ['placeholder' => 'Seleccione un encargado ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ])
                    ]
                ]
            ],
            [
                'attributes' => [
                    'departamento_id' => [
                        'type' => Form::INPUT_RAW,
                        'value' => $form->field($model, 'departamento_id')->widget(Select2::className(), [
                            'data' => Departamento::getDepartamentoLista(true),
                            'options' => ['placeholder' => 'Seleccione un Departamento...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            'pluginEvents' => [
                                'change' => "function(){
                                    $('#sucursal-ciudad_id').val('').trigger('change');
                                    $('#sucursal-barrio_id').val('').trigger('change');
                                }"
                            ]
                        ])
                    ],
                    'ciudad_id' => [
                        'type' => Form::INPUT_RAW,
                        'value' => $form->field($model, 'ciudad_id')->widget(Select2::className(), [
                            'options' => ['placeholder' => 'Seleccione una Ciudad...'],
                            'id' => 'sucursal_ciudad_select2',
                            'initValueText' => !empty($model->ciudad->nombre) ? $model->ciudad->nombre : '',
                            'pluginOptions' => [
                                'allowClear' => true,
                                'ajax' => [
                                    'url' => Url::to(['ciudad/get-ciudad-lista']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression("function(params) { return {q:params.term,id:$('#sucursal-departamento_id').val()}; }")
                                ]
                            ]
                        ])
                    ],
                    'barrio_id' => [
                        'type' => Form::INPUT_RAW,
                        'value' => $form->field($model, 'barrio_id')->widget(Select2::className(), [
                            'options' => ['placeholder' => 'Seleccione un Barrio/Localidad...'],
                            'id' => 'sucursal_barrio_select2',
                            'initValueText' => !empty($model->barrio->nombre) ? $model->barrio->nombre : '',
                            'pluginOptions' => [
                                'allowClear' => true,
                                'ajax' => [
                                    'url' => Url::to(['barrio/get-barrio-lista']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression("function(params) { return {q:params.term,id:$('#sucursal-ciudad_id').val()}; }")
                                ]
                            ]
                        ])
                    ]
                ]
            ],
            [
                'attributes' => [
                    'direccion' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => ['placeholder '=> 'Dirección (calle, número, etc...)'],
                    ],

                    'telefono' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => ['placeholder '=> 'Teléfonos (separados por coma...)'],
                    ]
                ]
            ]
        ]
    ]);

    $texto = !empty($model->id) ? 'Edit' : 'Agreg';

    ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', "{$texto}ar"), ['data' => ['disabled-text' => "{$texto}ando Detalle ..."], 'class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end();

    $script =
<<<JS
    $("form#sucursal_detalle_form").on("beforeSubmit", function(e) {
        var form = $(this);
        $.post(
            form.attr("action")+"&submit=true",
            form.serialize()
        )
        .done(function(result) {
            form.parent().html(result.message);
            $.pjax.reload({container:"#sucursales_grid", async:false});
            $.pjax.reload({container:"#flash_message_id", async:false});
            $("#modal").modal("hide");      
            $("modal-body").empty();         
        });
        return false;
    }).on("submit", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        return false;
    });
JS;
    $this->registerJs($script);
    ?>

</div>
