<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 11/21/17
 * Time: 6:24 PM
 */

/* @var $this yii\web\View */
/* @var $model backend\models\Sucursal */
/* @var $empresaId int */

?>
<div class="sucursal-empresa-create">

    <?= $this->render('_form-detalle-sucursal', [
        'model' => $model,
        'empresaId' => $empresaId
    ]) ?>

</div>
