<?php

use backend\models\Departamento;
use common\helpers\PermisosHelpers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
/* @var $show_tab string */
/* @var $model \backend\models\Empresa */
?>

<div class="empresa-form">

    <?php $form = ActiveForm::begin(['id' => 'formulario-empresa']);

    !empty($model->id) || $model->activo = true;

    try {
        $datos_generales = FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'razon_social' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder ' => 'Razón Social de la Empresa...'],
                            'columnOptions' => ['colspan' => '4']
                        ],
                        'nombre' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder ' => 'Nombre de Pila...'],
                            'columnOptions' => ['colspan' => '4']
                        ],
                        'ruc' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder' => 'RUC de la Empresa...'],
//                            'columnOptions' => ['colspan' => '2']
                        ],
                        'digito_verificador' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder' => 'Dígito Verificador...']
                        ],
                        'tipo' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => Select2::class,
                            'hint' => 'Tipo',
                            'label' => 'Tipo',
                            'options' => [
                                'data' => \backend\models\Empresa::getTipos(),
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                                'disabled' => false,
                            ],
                            'columnOptions' => ['colspan' => '2']
                        ],
                    ]
                ],
                [
                    'autoGenerateColumns' => true,
                    'contentBefore' => '<legend id="legend_repre_legal" class="text-info"><small>Representante Legal</small></legend>',
                    'attributes' => [
                        'primer_nombre' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder ' => 'Primer Nombre...', 'class' => 'form-control campos-para-fisica'],
                        ],
                        'segundo_nombre' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder ' => 'Segundo Nombre...', 'class' => 'form-control campos-para-fisica'],
                        ],
                        'primer_apellido' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder' => 'Primer Apellido...', 'class' => 'form-control campos-para-fisica'],
                        ],
                        'segundo_apellido' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder' => 'Segundo Apellido...', 'class' => 'form-control campos-para-fisica']
                        ],
                        'cedula' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder' => 'Cédula...', 'class' => 'form-control campos-para-fisica']
                        ],
                        'cedula_dv' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder' => 'DV Cédula', 'class' => 'form-control campos-para-fisica', 'readonly' => true]
                        ],
                    ]
                ],
                [
                    'attributes' => [
                        'actividades_comerciales' => [
                            'type' => Form::INPUT_TEXTAREA,
                            'options' => ['placeholder ' => 'Actividades Comerciales']
                        ]
                    ]
                ],
                [
                    'attributes' => [
                        'observaciones' => [
                            'type' => Form::INPUT_TEXTAREA,
                            'options' => ['placeholder ' => 'Observaciones...']
                        ]
                    ]
                ],
                [
                    'attributes' => [
                        'activo' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => SwitchInput::className(),
                            'options' => [
                                'pluginOptions' => [
                                    'onText' => 'Si',
                                    'offText' => 'No',
                                    'offColor' => 'danger',
                                    'size' => 'small',
                                    'handleWidth' => '60',
                                    'labelWidth' => '60',
                                ]
                            ]
                        ],
                    ]
                ],
//                [
//                    'attributes' => [
//                        'action' => [
//                            'type' => Form::INPUT_RAW,
//                            'value' => '<div style="margin-top: 20px">' .
//                                Html::submitButton('Guardar', ['class' => 'btn btn-primary']) .
//                                '</div>'
//                        ],
//                    ],
//                ]
            ]
        ]);
    } catch (Exception $e) {
        echo $e;
    }

    try {
        $localizacion = FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'attributes' => [
                        'direccion' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder ' => 'Dirección (calle, número, etc...)'],
                        ]
                    ]
                ],
                [
                    'attributes' => [
                        'departamento_id' => [
                            'type' => Form::INPUT_RAW,
                            'value' => $form->field($model, 'departamento_id')->widget(Select2::className(), [
                                'data' => Departamento::getDepartamentoLista(true),
                                'options' => ['placeholder' => 'Seleccione un Departamento ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'pluginEvents' => [
                                    'change' => "function(){
                                        $('#empresa-ciudad_id').val('').trigger('change');
                                        $('#empresa-barrio_id').val('').trigger('change');
                                    }"
                                ]
                            ])
                        ],
                        'ciudad_id' => [
                            'type' => Form::INPUT_RAW,
                            'value' => $form->field($model, 'ciudad_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione una Ciudad ...'],
                                'initValueText' => !empty($model->ciudad->nombre) ? $model->ciudad->nombre : '',
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => Url::to(['ciudad/get-ciudad-lista']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression("function(params) { return {q:params.term,id:$('#empresa-departamento_id').val()}; }")
                                    ]
                                ],
                                'pluginEvents' => [
                                    'change' => "function(){
                                        $('#empresa-barrio_id').val('').trigger('change');
                                    }"
                                ]
                            ])
                        ],
                        'barrio_id' => [
                            'type' => Form::INPUT_RAW,
                            'value' => $form->field($model, 'barrio_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione un Barrio/Localidad ...'],
                                'initValueText' => !empty($model->barrio->nombre) ? $model->barrio->nombre : '',
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => Url::to(['barrio/get-barrio-lista']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression("function(params) { return {q:params.term,id:$('#empresa-ciudad_id').val()}; }")
                                    ]
                                ]
                            ])
                        ]
                    ]
                ],
                [
                    'attributes' => [
                        'telefonos' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder ' => 'Teléfonos (separados por coma...)'],
                        ]
                    ]
                ],
                [
                    'attributes' => [
                        'coordenadas' => [
                            'type' => Form::INPUT_RAW,
                            'value' => $form->field($model, 'coordenadas')->widget(
                                'kolyunya\yii2\widgets\MapInputWidget',
                                [
                                    'key' => 'AIzaSyCunsTCK8jz984dhPuRfkuJSuRoMqqP-mI', /* TODO: borrar esto antes del proximo commit */
                                    'latitude' => -25.237936594571143,
                                    'longitude' => -57.711312561035156,
                                    'zoom' => 12,
                                    'width' => '100%',
                                    'height' => '420px',
                                    'pattern' => '%latitude%,%longitude%',
                                    'mapType' => 'roadmap',
                                    'animateMarker' => true,
                                    'alignMapCenter' => true,
                                    'enableSearchBar' => true,
                                    'editMode' => true

                                ]
                            )
                        ]
                    ]
                ],
//                [
//                    'attributes' => [
//                        'action' => [
//                            'type' => Form::INPUT_RAW,
//                            'value' => '<div style="margin-top: 20px">' .
//                                Html::submitButton('Guardar', ['class' => 'btn btn-primary']) .
//                                '</div>'
//                        ],
//                    ],
//                ]
            ]
        ]);
    } catch (Exception $e) {
        echo $e;
    }

    $items = [
        [
            'label' => '<i class="fa fa-info"></i> Datos Generales',
            'content' => $datos_generales,
            'active' => $show_tab == 'general',
            'headerOptions' => ['class' => 'tab_general', 'id' => 'tab_general']
        ],
        [
            'label' => '<i class="fa fa-map-marker"></i> Localización',
            'content' => $localizacion,
            'active' => $show_tab == 'localizacion',
            'headerOptions' => ['class' => 'tab_map', 'id' => 'tab_map']
        ]
    ];

    if ($model->id) {
        $items[] = [
            'label' => '<i class="fa fa-building-o"></i> Sucursales',
            'content' => $this->render('_sucursal_empresa', ['model' => $model]),
            'headerOptions' => ['id' => 'tab_sucursales']
        ];

        /** Mostrar tab de periodo contable si existe modulo contabilidad. */
        if (class_exists('\backend\modules\contabilidad\MainModule')) {
            $items[] = [
                'label' => '<i class="fa fa-calendar"></i> Contabilidad',
                'content' => $this->render('@backend/modules/contabilidad/views/empresa-periodo-contable/_form_add_periodo', [
                    'model' => $model,
                    'form' => $form
                ]),
                'active' => $show_tab == 'periodo_cont',
                'headerOptions' => ['class' => 'tab_map', 'id' => 'tab_map']
            ];
        }
    }

    try {

        echo TabsX::widget([
            'items' => $items,
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'id' => 'tabs'
        ]);
    } catch (Exception $e) {
        echo $e;
    }

    ?>

    <div class="form-group" style="margin-top: 20px" style="margin-left: 20px">
        <?= PermisosHelpers::getAcceso('empresa-create') ? Html::submitButton('Guardar', ['class' => 'btn btn-success']) : '' ?>
    </div>

    <?php ActiveForm::end();

    $_url_empresa_edit = Url::to(['empresa/update', 'id' => $model->id]);
    $digVerifUrl = \yii\helpers\Json::htmlEncode(\Yii::t('app', Url::to(['get-digito-verificador'])));
    $script =
        <<<JS
$('#formulario-empresa').on('afterValidate', function (event, messages, errorAttributes) {
    var scrollPosision = null;
    $(errorAttributes).each(function (key, attribute) {
        // Special behavior for unchecked "Accept agreement" input
        console.log(attribute.name);
        if (attribute.name === 'razon_social' ||
            attribute.name === 'nombre' ||
            attribute.name === 'ruc' ||
            attribute.name === 'digito_verificador' ||
            attribute.name === 'actividades_comerciales' ||
            attribute.name === 'observaciones' ||
            attribute.name === 'activo')
        {
            console.log('en verificacion de 1 pestaña');
            $('#tabs').find('> li.tab_general').find('a').click();
            return false;
        }
        else if (attribute.name === 'direccion' ||
                   attribute.name === 'departamento_id'||
                   attribute.name === 'ciudad_id' ||
                   attribute.name === 'telefonos')
        {
            console.log('en verificacion de 2 pestaña');
            $('#tabs').find('> li.tab_map').find('a').click();
            return false;
        }
        else {
            console.log('en verificacion de 3 pestaña');
            console.log('putin 1');
            if ($('#tab_pc').length) { // length==1, implica que existe módulo contabilidad, o 0 si no existe.
                console.log('putin2');
                if (attribute.name === 'anho' ||
                   attribute.name === 'activo')
                {
                    $('#tabs').find('> li.tab_pc').find('a').click();
                    console.log('putin 3');
                }
            }
        }
        // Save the position of the first errored element
        scrollPosision = scrollPosision || $(attribute.input).offset().top;
    });
});

$('#empresa-tipo').on('change', function() {
    ocultarCamposNombresApellidos();
});

$(window).on('load', function () {
    var divMapa = $('.kolyunya-map-input-widget');
    var mapaWidget = mapInputWidgetManager.getWidget(divMapa.prop('id'));
    $('#tab_map').on('click', function () {
        setTimeout(function () {
            if (divMapa.is(':visible') && !divMapa.data('resized')) {
                mapaWidget.resize();
                var point = mapaWidget.getInitialPoint();
                if (point) {
                    mapaWidget.panTo(point);
                    mapaWidget.setZoom(15);
                }
                divMapa.data('resized', true);
            }
        }, 500);
    });
});

function ocultarCamposNombresApellidos() {
    // console.log('ocultarCamposNombresApellidos');
    // let display = $('#empresa-tipo').val() === 'personal' ? '' : 'none';
    // $(':input.campos-para-fisica').parent().parent().parent().css('display', display);
    //
    // if (display === 'none') {
    //     $('#empresa-segundo_nombre').parent().parent().parent().find('input').val('').trigger('change');
    // }
}

function showHideLegendRepreLegal(){
    if ($('#empresa-tipo').val() === 'juridica'){
        $('#legend_repre_legal').show();
    } else {
        $('#legend_repre_legal').hide();
    }
}

$(document).ready(function () {
    
    ocultarCamposNombresApellidos();
    showHideLegendRepreLegal();
    
    $('#empresa-ruc').on('change keyup', function () {
        $.ajax({
            url: $digVerifUrl,
            type: 'post',
            data: {
                numero: $(this).val()
            },
            success: function (data) {
                $('#empresa-digito_verificador').val(data);
            }
        });
    });
    
    $('#empresa-tipo').on('change', function () {
        showHideLegendRepreLegal();
    });
    
    /* recargar el pjax para limpiar la URL por si se haya usado el filtro del Historial de Sucursal */
    $('#modal').on('hidden.bs.modal', function () {
        $('.modal-body', $(this)).html("");
        if ($('#tab_sucursales').hasClass('active')) {
            $.pjax.reload({container: "#sucursales_grid", async: false, url: "$_url_empresa_edit"});
        }
    });
});
JS;

    $this->registerJs($script, View::POS_END);

    ?>

</div>
