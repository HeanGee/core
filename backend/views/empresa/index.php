<?php

use common\helpers\PermisosHelpers;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\EmpresaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Empresas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= PermisosHelpers::getAcceso('empresa-create') ? Html::a('Crear Empresa', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>

    <?php
    $template = '';
    $template = $template . (PermisosHelpers::getAcceso('empresa-view') ? '{view} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('empresa-update') ? '{update} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('empresa-delete') ? '{delete} &nbsp&nbsp&nbsp' : '');

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'razon_social',
            'nombre',
            'ruc',
            [
                'label' => 'Ciudad',
                'attribute' => 'ciudad_nombre',
                'value' => 'ciudad.nombre'
            ],
            [
                'attribute' => 'activo',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<label class="label label-' . (($model->activo == '1') ? 'success">Activo' : 'danger">Inactivo') . '</label>';
                },
                'contentOptions' => ['style' => 'text-align:center;'],
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'activo',
                    'data' => [-1 => 'Todos', 1 => 'Activo', 0 => 'Inactivo'],
		    'hideSearch' => true,
		    'pluginOptions' => [
                        'width' => '120px'
                    ]
                ])
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'header' => 'Acciones',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
