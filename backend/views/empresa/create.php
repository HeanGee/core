<?php


/* @var $this yii\web\View */
/* @var $model backend\models\Empresa */
/* @var $show_tab string */

$this->title = 'Crear Empresa';
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-create">

    <?= $this->render('_form', [
        'model' => $model,
        'show_tab' => $show_tab
    ]) ?>

</div>
