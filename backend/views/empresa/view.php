<?php

use kartik\detail\DetailView;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use common\helpers\PermisosHelpers;

/* @var $this yii\web\View */
/* @var $model backend\models\Empresa */

$this->title = $model->razon_social;
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$form = new ActiveForm();
?>
<div class="empresa-view">

    <p>
        <?= Html::a('Ir a Empresas', ['index'], ['class' => 'btn btn-info']) ?>
        <?= PermisosHelpers::getAcceso('empresa-update') ? Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= PermisosHelpers::getAcceso('empresa-delete') ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de querer borrar esta Empresa?',
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?php

    $attributes['general'] = [
        [
            'columns' => [
                [
                    'attribute' => 'razon_social',
                    'displayOnly' => true,
                    'valueColOptions' => ['style' => 'width:30%']
                ],
                [
                    'attribute' => 'nombre',
                    'displayOnly' => true,
                    'valueColOptions' => ['style' => 'width:30%']
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'ruc',
                    'displayOnly' => true,
                    'valueColOptions' => ['style' => 'width:30%']
                ],
                [
                    'attribute' => 'digito_verificador',
                    'displayOnly' => true,
                    'valueColOptions' => ['style' => 'width:30%']
                ],
            ],
        ],
        [
            'attribute' => 'actividades_comerciales',
            'format' => 'raw',
            'value' => '<span class="text-justify"><em>' . $model->actividades_comerciales . '</em></span>',
            'type' => DetailView::INPUT_TEXTAREA,
            'options' => ['rows' => 4]
        ],
        [
            'attribute' => 'activo',
            'format' => 'raw',
            'value' => $model->activo == 1 ? '<span class="label label-success">Activo</span>' : '<span class="label label-danger">Inactivo</span>',
        ],
        [
            'attribute' => 'observaciones',
            'format' => 'raw',
            'value' => '<span class="text-justify"><em>' . $model->observaciones . '</em></span>',
            'type' => DetailView::INPUT_TEXTAREA,
            'options' => ['rows' => 4]
        ],
    ];

    $attributes['localizacion'] = [
        [
            'columns' => [
                [
                    'attribute' => 'direccion',
                    'displayOnly' => true,
                    'valueColOptions' => ['style' => 'width:30%']
                ],
                [
                    'attribute' => 'departamento_id',
                    'displayOnly' => true,
                    'valueColOptions' => ['style' => 'width:30%'],
                    'value' => $model->departamento->nombre
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'ciudad_id',
                    'displayOnly' => true,
                    'valueColOptions' => ['style' => 'width:30%'],
                    'value' => $model->ciudad->nombre
                ],
                [
                    'attribute' => 'barrio_id',
                    'displayOnly' => true,
                    'valueColOptions' => ['style' => 'width:30%'],
                    'value' => $model->barrio->nombre
                ],
            ],
        ],
        'telefonos',
        [
            'attribute' => 'coordenadas',
            'format' => 'raw',
            'value' => !empty($model->coordenadas) ? $form->field($model, 'coordenadas')->label(false)->widget(
                'kolyunya\yii2\widgets\MapInputWidget',
                [
                    'key' => 'AIzaSyCunsTCK8jz984dhPuRfkuJSuRoMqqP-mI', /* TODO: borrar esto antes del proximo commit */
                    'latitude' => -25.237936594571143,
                    'longitude' => -57.711312561035156,
                    'zoom' => 15,
                    'width' => '100%',
                    'height' => '420px',
                    'pattern' => '%latitude%,%longitude%',
                    'mapType' => 'roadmap',
                    'animateMarker' => true,
                    'alignMapCenter' => true,
                    'enableSearchBar' => false,
                    'editMode' => false

                ]
            ) : ''
        ]
    ];

    $attributes['sucursales'] = [];
    foreach ($model->sucursales as $sucursal) {
        $attributes['sucursales'][] = [
            'columns' => [
                [
                    'label' => 'Nombre',
                    'format' => 'raw',
                    'valueColOptions' => ['style' => 'width:30%'],
                    'value' => $sucursal != null ? $sucursal->nombre : ''
                ],
                [
                    'label' => 'Ciudad',
                    'format' => 'raw',
                    'valueColOptions' => ['style' => 'width:30%'],
                    'value' => $sucursal->ciudad != null ? $sucursal->ciudad->nombre : ''
                ],
            ]
        ];
    }

    ?>

    <?php
    echo DetailView::widget([
        'model' => $model,
        'condensed' => true,
        'hover' => true,
        'mode' => DetailView::MODE_VIEW,
        'enableEditMode' => false,
        'fadeDelay' => true,
        'panel' => [
            'heading' => 'Información General',
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => $attributes['general']
    ]);

    echo DetailView::widget([
        'model' => $model,
        'condensed' => true,
        'hover' => true,
        'mode' => DetailView::MODE_VIEW,
        'enableEditMode' => false,
        'fadeDelay' => true,
        'panel' => [
            'heading' => 'Localización',
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => $attributes['localizacion']
    ]);

    echo DetailView::widget([
        'model' => $model,
        'condensed' => true,
        'hover' => true,
        'mode' => DetailView::MODE_VIEW,
        'enableEditMode' => false,
        'fadeDelay' => true,
        'panel' => [
            'heading' => 'Sucursales',
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => $attributes['sucursales']
    ]);

    //    echo $this->render('_auditoria_empresa', ['modelo' => $model]);

    ?>

</div>
