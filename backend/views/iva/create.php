<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Iva */

$this->title = 'Crear nuevo I.V.A.';
$this->params['breadcrumbs'][] = ['label' => 'Ivas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iva-create">

    <?= Html::a('Regresar a la Lista de I.V.A.', ['index'], ['class' => 'btn btn-info']) ?>
    <?= Html::tag('br/') ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
