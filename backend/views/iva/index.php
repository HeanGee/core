<?php

use common\helpers\PermisosHelpers;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\IvaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista de I.V.A';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iva-index">

    <?php Pjax::begin(); ?>

    <?php
    if (PermisosHelpers::getAcceso('iva-create')) {
        echo Html::tag('p');
        echo Html::a('Crear nueva I.V.A.', ['create'], ['class' => 'btn btn-success']);
        echo Html::tag('/p');
    }
    ?>

    <?php
    try {
        $template = '';
        $template = $template . (PermisosHelpers::getAcceso('iva-view') ? '{view} &nbsp&nbsp&nbsp' : '');
        $template = $template . (PermisosHelpers::getAcceso('iva-update') ? '{update} &nbsp&nbsp&nbsp' : '');
        $template = $template . (PermisosHelpers::getAcceso('iva-delete') ? '{delete} &nbsp&nbsp&nbsp' : '');
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'porcentaje',
                    'value' => 'porcentajetextual',
                ],
                [
                    'label' => 'Estado',
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->estado == '1') ? 'success">Activo' : 'danger">Inactivo') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => [0 => 'Inactivo', 1 => 'Activo', 2 => 'Todos'],
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => false],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Filtro de Estado'],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $template,
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center'],
                    'header' => 'Acciones',
                ],
            ],
        ]);
    } catch (Exception $e) {
    } ?>

    <?php Pjax::end(); ?>
</div>
