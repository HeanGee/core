<?php

use common\helpers\PermisosHelpers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Iva */
/* @var $form kartik\form\ActiveForm */
?>

<div class="iva-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    try {
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'autoGenerateColumns' => true,
                    'attributes' => [
                        'porcentaje' => [
                            'type' => Form::INPUT_RAW,
//                            'columnOptions' => ['colspan' => '3'],
                            'value' => $form->field($model, 'porcentaje')->widget(NumberControl::className(), [
                                'name' => 'porcentaje',
                                'maskedInputOptions' => [
                                    'prefix' => '',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'digits' => 0
                                ],
                                'displayOptions' => [
                                    'class' => 'form-control kv-monospace',
                                    'placeholder' => 'Porcentaje del IVA...'
                                ]
                            ])
                        ],
                        'estado' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => Select2::class,
                            'options' => ['data' => ['1' => 'Activo', '0' => 'Inactivo']],
//                            'columnOptions' => ['colspan' => '3'],
                        ],
                    ],
                ],
                [
                    'attributes' => [
                        'action' => [
                            'type' => Form::INPUT_RAW,
                            'value' => '<div style="margin-top: 20px">' .
                            PermisosHelpers::getAcceso('iva-create') ? Html::submitButton('Guardar', ['class' => 'btn btn-primary']) : '' .
                                '</div>'
                        ]
                    ]
                ]
            ],
        ]);
    } catch (\Exception $e) {
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
