<?php

use kartik\detail\DetailView;
use yii\helpers\Html;
use common\helpers\PermisosHelpers;

/* @var $this yii\web\View */
/* @var $model backend\models\Iva */

$this->title = "I.V.A. " . $model->porcentaje . '%';
$this->params['breadcrumbs'][] = ['label' => 'Ivas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iva-view">

    <p>
        <?= Html::a('Regresar a la Lista de I.V.A.', ['index'], ['class' => 'btn btn-info']) ?>
        <?= PermisosHelpers::getAcceso('iva-update') ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= PermisosHelpers::getAcceso('iva-delete') ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => "Datos",
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                //            'id',
                'porcentaje',
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => '<label class="label label-' . (($model->estado == '1') ? 'success">Activo' : 'danger">Inactivo') . '</label>',
                ],
            ],
        ]);
    } catch (Exception $e) {
    } ?>

</div>
