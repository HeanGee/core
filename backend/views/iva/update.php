<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Iva */

$this->title = 'Modificar I.V.A. ' . $model->porcentaje.'%';
$this->params['breadcrumbs'][] = ['label' => 'Ivas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="iva-update">

    <?= Html::a('Ver I.V.A.', ['view', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    <?= Html::tag('br/') ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
