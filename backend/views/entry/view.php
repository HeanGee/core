<?php

/** @var View $this */
/** @var Panel[] $panels */
/** @var Panel $activePanel */

/** @var AuditEntry $model */

use bedezign\yii2\audit\Audit;
use bedezign\yii2\audit\components\panels\Panel;
use bedezign\yii2\audit\models\AuditEntry;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

foreach ($panels as $panel) {
    $panel->registerAssets($this);
}

$this->title = Yii::t('audit', 'Entry #{id}', ['id' => $model->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Registros'), 'url' => ['entry/index']];
$this->params['breadcrumbs'][] = 'Registro #' . $model->id;

?>
    <div class="row">
        <div class="col-md-6">
            <?php
            echo Html::tag('h2', Yii::t('audit', 'Request') . ' ' . Html::a('Regresar a la lista de Registros', ['entry/index'], ['class' => 'btn btn-info']), ['id' => 'entry', 'class' => 'hashtag']);

            if ($model->request_method == 'CLI') {
                $attributes = [
                    'route',
                    'request_method',
                ];
            } else {
                $attributes = [
                    [
                        'label' => $model->getAttributeLabel('user_id'),
                        'value' => Audit::getInstance()->getUserIdentifier($model->user_id),
                        'format' => 'raw',
                    ],
                    'ip',
                    'route',
                    'request_method',
                    [
                        'label' => $model->getAttributeLabel('ajax'),
                        'value' => $model->ajax ? Yii::t('audit', 'Yes') : Yii::t('audit', 'No'),
                    ],
                ];
            }

            echo DetailView::widget([
                'model' => $model,
                'attributes' => $attributes
            ]);
            ?>
        </div>
        <div class="col-md-6">
            <?php
            echo Html::tag('h2', Yii::t('audit', 'Profiling'), ['id' => 'entry', 'class' => 'hashtag', 'style' => '    padding-top: 0px; margin-top: 0px']);

            $attributes = [
                ['attribute' => 'duration', 'format' => 'decimal'],
                ['attribute' => 'memory_max', 'format' => 'shortsize'],
                'created',
            ];

            echo DetailView::widget([
                'model' => $model,
                'attributes' => $attributes
            ]);
            ?>
        </div>
    </div>

<?php Pjax::begin(['id' => 'audit-panels', 'timeout' => 0]); ?>
    <div class="row">
        <div class="col-md-2">
            <div class="list-group">
                <?php
                foreach ($panels as $id => $panel) {
                    $label = '<i style="float: right;" class="glyphicon glyphicon-chevron-right"></i>' . $panel->getLabel();
                    echo Html::a($label, ['view', 'id' => $model->id, 'panel' => $id], [
                        'class' => $panel === $activePanel ? 'list-group-item active' : 'list-group-item',
                    ]);
                }
                ?>
            </div>
        </div>
        <div class="col-md-10">
            <?php if ($activePanel) { ?>
                <?= $activePanel->getDetail(); ?>
                <input type="hidden" name="panel" value="<?= $activePanel->id ?>"/>
            <?php } ?>
        </div>
    </div>
<?php Pjax::end(); ?>