<?php

use bedezign\yii2\audit\Audit;
use common\models\User;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Registros');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audit-entry-index">

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'toolbar' => [],
        'hover' => true,
        'panel' => [
            'type' => 'info',
            'heading' => 'Auditoría',
            'footerOptions' => ['class' => ''],
            'beforeOptions' => ['class' => ''],
            'afterOptions' => ['class' => '']
        ],
        'columns' => [
            'id',
            [
                'class' => DataColumn::className(),
                'attribute' => 'model',
                'value' => 'model',
                /*'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'model' => $searchModel,
                    'attribute' => 'state',
                    'data' => CallChecker::getEnumValues('state', false),
                    'pluginOptions' => [
                        'placeholder' => "All",
                        'allowClear' => true,
                    ],
                ],*/
            ],
            [
                'class' => DataColumn::className(),
                'attribute' => 'model_id',

            ],
            [
                'class' => DataColumn::className(),
                'attribute' => 'field',

            ],
            [
                'class' => DataColumn::className(),
                'attribute' => 'old_value',

            ],
            [
                'class' => DataColumn::className(),
                'attribute' => 'new_value',

            ],
            [
                'class' => DataColumn::className(),
                'attribute' => 'user_id',
                'value' => function ($data) {
                    return User::findOne(Audit::getInstance()->getUserIdentifier($data->user_id))->username;
                },
                'label' => 'Usuario',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()
                    ->orderBy('username')
                    ->asArray()
                    ->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Seleccione usuario'],
            ],
            [
                'class' => DataColumn::className(),
                'attribute' => 'action',
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'model' => $searchModel,
                    'attribute' => 'action',
                    'data' => ['create' => "CREAR", 'update' => "MODIFICAR", 'delete' => "BORRAR"],
                    'pluginOptions' => [
                        'placeholder' => "All",
                        'allowClear' => true,
                    ],
                ],
            ],
            [
                'class' => DataColumn::className(),
                'attribute' => 'created',
                'format' => ['date', 'php:l, d-M-Y, H:i:s A'],
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'model' => $searchModel,
                    'attribute' => 'created',
                    'language' => 'es',
                    'pickerButton' => false,
                    'options' => [
                        //'style' => 'width:180px',
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true,
                        'weekStart' => '0',
                    ]
                ],
                'options' => ['width' => '150px'],
            ],
        ],
    ]); ?>
</div>
