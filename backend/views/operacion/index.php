<?php

use common\helpers\PermisosHelpers;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OperacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Operaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacion-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="btn-toolbar">
        <p>
            <?= PermisosHelpers::getAcceso('operacion-create') ? Html::a('Nueva Operación', ['create'], ['class' => 'btn btn-success']) : '' ?>
            <?= PermisosHelpers::esSuperUsuario() ?
                Html::a(
                    'Actualizar lista de operaciones',
                    Url::to(['register-actions']),
                    [
                        'class' => 'btn btn-warning pull-right',
                        'data' => [
                            'disabled-text' => 'Registrando ...',
                            'method' => 'post',
                        ]
                    ]
                ) : null ?>
        </p>
    </div>
    <?php
    $template = '';
    $template = $template . (PermisosHelpers::getAcceso('operacion-view') ? '{view} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('operacion-update') ? '{update} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('operacion-delete') ? '{delete} &nbsp&nbsp&nbsp' : '');
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'nombre',
            [
                'attribute' => 'operacion',
                'value' => 'operacion',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'operacion',
                    'data' => $concepts,
                    'pluginOptions' => [
//                            'width' => '90px',
                        'allowClear' => false,
                    ],
//                    'contentOptions' => ['style' => 'padding:8px 0px 0px 0px; text-align:center;']
                    'initValueText' => 'todos',
                ]),
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'header' => 'Acciones',
            ],
        ],
    ]); ?>
</div>
