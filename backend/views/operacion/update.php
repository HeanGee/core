<?php

use backend\models\Operacion;

/* @var $this yii\web\View */
/* @var $model Operacion */

$this->title = 'Actualizar Operacion: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Operaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="operacion-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
