<?php

use backend\models\Operacion;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Operacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operacion-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?><!--  Desabilita el botón de submit-->
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'disable-submit-buttons'],
    ]); ?>

    <?= $form->field($model, 'operacion')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= \common\helpers\PermisosHelpers::getAcceso('operacion-create') ?
            Html::submitButton(
                'Guardar',
                [
                    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                    'data' => [
                        'disabled-text' => 'Guardando ...'
                    ]
                ]
            ) : '' ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
