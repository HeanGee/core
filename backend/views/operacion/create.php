<?php

use backend\models\Operacion;


/* @var $this yii\web\View */
/* @var $model Operacion */

$this->title = 'Nueva Operación';
$this->params['breadcrumbs'][] = ['label' => 'Operaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacion-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
