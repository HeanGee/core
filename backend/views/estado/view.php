<?php

use backend\models\Estado;
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\PermisosHelpers;

/* @var $this yii\web\View */
/* @var $model Estado */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Estados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estado-view">
    <p>
        <?= PermisosHelpers::getAcceso('estado-update') ? Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= PermisosHelpers::getAcceso('estado-delete') ? Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', '¿Está seguro de eliminar este elemento?'),
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'estado_nombre',
            'estado_valor',
        ],
    ]) ?>

</div>
