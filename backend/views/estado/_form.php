<?php

use backend\models\Estado;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Estado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estado-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?><!--  Desabilita el botón de submit-->
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'disable-submit-buttons'],
    ]); ?>

    <?= $form->field($model, 'estado_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado_valor')->textInput() ?>

    <div class="form-group">
        <?= \common\helpers\PermisosHelpers::getAcceso('estado-create') ?
            Html::submitButton(
                Yii::t('app', 'Guardar'),
                [
                    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                    'data' => [
                        'disabled-text' => 'Guardando ...'
                    ]
                ]
            ) : '' ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
