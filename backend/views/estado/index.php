<?php

use common\helpers\PermisosHelpers;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\EstadoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Estados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estado-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= PermisosHelpers::getAcceso('estado-create') ? Html::a('Create Estado', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>
    <?php Pjax::begin();
    $template = '';
    $template = $template . (PermisosHelpers::getAcceso('estado-view') ? '{view} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('estado-update') ? '{update} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('estado-delete') ? '{delete} &nbsp&nbsp&nbsp' : '');
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'estado_nombre',
            'estado_valor',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'header' => 'Acciones',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
