<?php

use backend\models\Estado;


/* @var $this yii\web\View */
/* @var $model Estado */

$this->title = Yii::t('app', 'Crear Estado');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estados'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estado-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
