<?php

use backend\models\Estado;

/* @var $this yii\web\View */
/* @var $model Estado */

$this->title = 'Actualizar Estado: ' . $model->estado_nombre;
$this->params['breadcrumbs'][] = ['label' => 'Estados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estado-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
