<?php

use backend\helpers\HtmlHelpers;
use common\helpers\FlashMessageHelpsers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\ParametroSistema */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parametro-sistema-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php try {
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 2,
                    'attributes' => [
                        'nombre' => [
                            'type' => Form::INPUT_TEXT
                        ],
                        'valor' => [
                            'type' => Form::INPUT_TEXT
                        ],
                        'replicate_to' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => Select2::className(),
                            'options' => [
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'data' => ['contabilidad' => "MÓDULO CONTABILIDAD", 'empresa' => "TODAS LAS EMPRESAS"],
                                'options' => [
                                    'placeholder' => 'Por favor Seleccione Uno',
                                    'disabled' => false,
                                ]
                            ],
                            'hint' => 'Seleccione una opción',
                        ],
                        'replicate_as' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => Select2::className(),
                            'options' => [
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'data' => ['create' => "NUEVO (crear solamente si no existe)", 'update' => "ACTUALIZACIÓN (crear nuevo con sobre-escritura si existe)"],
                                'options' => [
                                    'placeholder' => 'Por favor Seleccione Uno',
                                ]
                            ],
                            'hint' => 'Seleccione una opción',
                        ],
                    ],
                ]
            ]
        ]);

        echo HtmlHelpers::SubmitButton();

    } catch (Exception $exception) {
        echo $exception;
        FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>

<?php $js = <<<JS
$('#parametrosistema-replicate_to').change(function() {
    let replace_as = $('#parametrosistema-replicate_as');
    if ($(this).val() === '') {
        if (replace_as.val() !== '') {
            replace_as.select2('close');
            replace_as.val('').trigger('change');
        }
    } else {
        if (replace_as.val() === '')
            replace_as.select2('open');
    }
});

$('#parametrosistema-replicate_as').change(function() {
    let replace_to = $('#parametrosistema-replicate_to');
    if ($(this).val() !== '') {
        if (replace_to.val() === '') {
            replace_to.select2('open');
        }
    } else {
        replace_to.val('').trigger('change');
    }
});
JS;
$this->registerJs($js);
