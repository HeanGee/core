<?php

/* @var $this yii\web\View */
/* @var $model common\models\ParametroSistema */

$this->title = 'Crear Nuevo Parámetro';
$this->params['breadcrumbs'][] = ['label' => 'Parametro Sistema', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parametro-sistema-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
