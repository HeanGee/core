<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 20/04/2018
 * Time: 8:27
 */

use backend\models\Empresa;
use backend\models\SpecialSystemParms;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\TipoDocumentoSet;
use common\models\ParametroSistema;
use kartik\dialog\Dialog;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/* @var $model Empresa|SpecialSystemParms */

?>

<?php

$periodosProvider = new ActiveDataProvider([
    'query' => EmpresaPeriodoContable::find()->where("empresa_id = '{$model->id}'")->indexBy('id'),
    'pagination' => [
        'pageSize' => 20,
    ],
]);

try {
    $table_content = [];

    if (Yii::$app->controller->action->id == 'special-index' || \Yii::$app->session->has('core_empresa_actual') && \Yii::$app->session->has('core_empresa_actual_pc') && \Yii::$app->session->get('core_empresa_actual') == $model->id) {
        {
            // PARA FACTURAS

            $table_content = [];

            // Motivo de no vigencia
            $nombre = "motivo_no_vigencia_venta";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $model->motivo_no_vigencia_venta = $parametro_sistema->valor;
            } else {
                $model->motivo_no_vigencia_venta = '';
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'motivo_no_vigencia_venta',
//            'input' => 'masked_input:numeric',
                'input' => 'text',
                'input_text_class' => 'text',
                'label' => "{$model->getAttributeLabel('motivo_no_vigencia_venta')}",
            ];

            #Ventas cargar en paralelo
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'permitir_carga_venta_paralelo',
                'input' => 'select',
                'allowClear' => false,
                'select_data' => ['si' => 'Si', 'no' => 'No'],
                'select_data_inside_pOption' => null,
                'initValueText' => null,
                'label' => $model->getAttributeLabel('permitir_carga_venta_paralelo'),
            ];

            $nombre = "cuenta_seguros_pagados_id";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_seguros_pagados_id = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_seguros_pagados_id',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => ($model->cuenta_seguros_pagados_id != '') ?
                    (PlanCuenta::findOne($model->cuenta_seguros_pagados_id)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_seguros_pagados_id'),
            ];

            $nombre = "cta_resultado_diff_cambio_debe";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_resultado_diff_cambio_debe = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_resultado_diff_cambio_debe',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => ($model->cta_resultado_diff_cambio_debe != '') ?
                    (PlanCuenta::findOne($model->cta_resultado_diff_cambio_debe)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_resultado_diff_cambio_debe'),
            ];

            $nombre = "cta_resultado_diff_cambio_haber";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_resultado_diff_cambio_haber = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_resultado_diff_cambio_haber',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_resultado_diff_cambio_haber)) ?
                    (PlanCuenta::findOne($model->cta_resultado_diff_cambio_haber)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_resultado_diff_cambio_haber'),
            ];

            $nombre = "cuenta_error_redondeo_perdida";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_error_redondeo_perdida = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_error_redondeo_perdida',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cuenta_error_redondeo_perdida)) ?
                    (PlanCuenta::findOne($model->cuenta_error_redondeo_perdida)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_error_redondeo_perdida'),
            ];

            $nombre = "cuenta_error_redondeo_ganancia";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_error_redondeo_ganancia = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_error_redondeo_ganancia',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cuenta_error_redondeo_ganancia)) ?
                    (PlanCuenta::findOne($model->cuenta_error_redondeo_ganancia)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_error_redondeo_ganancia'),
            ];

            $nombre = "coeficiente_costo";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $model->coeficiente_costo = $parametro_sistema->valor;
            } else {
                $model->coeficiente_costo = 0;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'coeficiente_costo',
//            'input' => 'masked_input:numeric',
                'input' => 'text',
                'input_text_class' => 'decimal',
                'label' => "{$model->getAttributeLabel('coeficiente_costo')}",
            ];

            // Salario Minimo
            $nombre = "salario_minimo";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $model->salario_minimo = $parametro_sistema->valor;
            } else {
                $model->salario_minimo = '';
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'salario_minimo',
//            'input' => 'masked_input:numeric',
                'input' => 'text',
                'input_text_class' => 'decimal',
                'label' => "{$model->getAttributeLabel('salario_minimo')}",
            ];

            echo '<fieldset>';
            {
                echo '<legend class="text-info"><small>Facturas</small></legend>';
                echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
            }
            echo '</fieldset>';
        }

        {
            // PARA RETENCIONES
            $empresa = Empresa::findOne(\Yii::$app->session->get('core_empresa_actual'));
            $entidad = Entidad::findOne(['ruc' => $empresa->ruc]);

            if (!isset($entidad)) {
                echo '<fieldset>';
                {
                    $table_content = [];
                    $table_content[] = [
                        'form' => $form,
                        'model' => $model,
                        'attribute' => 'tipodoc_set_retencion_id',
                        'input' => 'error',
                        'label' => "<span class='btn btn-danger'>No se puede establecer {$model->getAttributeLabel('tipodoc_set_retencion_id')}</span>",
                        'msg' => " << FALTA CREAR ENTIDAD Y ASOCIAR A LA EMPRESA ACTUAL >>",
                    ];
                    echo '<legend class="text-info"><small>Retenciones</small></legend>';
                    echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
                }
                echo '</fieldset>';
            } else {

                $table_content = [];
                $nombre = "tipodoc_set_retencion";
                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                if (isset($parametro_sistema)) {
                    $tipodoc_set = TipoDocumentoSet::findOne($parametro_sistema->valor);
                    if (isset($tipodoc_set))
                        $model->tipodoc_set_retencion_id = $parametro_sistema->valor;
                }
                $table_content[] = [
                    'form' => $form,
                    'model' => $model,
                    'attribute' => 'tipodoc_set_retencion_id',
                    'input' => 'select',
                    'select_data' => null,
                    'allowClear' => true,
                    'select_data_inside_pOption' => TipoDocumentoSet::getLista(true),
                    'initValueText' => ($model->tipodoc_set_retencion_id != '') ?
                        (TipoDocumentoSet::findOne($model->tipodoc_set_retencion_id)->nombre) : '',
                    'label' => $model->getAttributeLabel('tipodoc_set_retencion_id'),
                ];

                #cuenta retenciones emitidas
                $nombre = "cuenta_retenciones_emitidas";
                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                if (isset($parametro_sistema)) {
                    $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                    if (isset($cuenta))
                        $model->cuenta_retenciones_emitidas = $parametro_sistema->valor;
                }
                $table_content[] = [
                    'form' => $form,
                    'model' => $model,
                    'attribute' => 'cuenta_retenciones_emitidas',
                    'input' => 'select',
                    'allowClear' => true,
                    'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(true), 'id', 'text'),
                    'select_data_inside_pOption' => null,
                    'initValueText' => ($model->cuenta_retenciones_emitidas != '') ?
                        (PlanCuenta::findOne($model->cuenta_retenciones_emitidas)->nombre) : '',
                    'label' => $model->getAttributeLabel('cuenta_retenciones_emitidas'),
                ];

                #cuenta retenciones recibidas
                $nombre = "cuenta_retenciones_recibidas";
                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                if (isset($parametro_sistema)) {
                    $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                    if (isset($cuenta))
                        $model->cuenta_retenciones_recibidas = $parametro_sistema->valor;
                }
                $table_content[] = [
                    'form' => $form,
                    'model' => $model,
                    'attribute' => 'cuenta_retenciones_recibidas',
                    'input' => 'select',
                    'allowClear' => true,
                    'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(true), 'id', 'text'),
                    'select_data_inside_pOption' => null,
                    'initValueText' => ($model->cuenta_retenciones_recibidas != '') ?
                        (PlanCuenta::findOne($model->cuenta_retenciones_recibidas)->nombre) : '',
                    'label' => $model->getAttributeLabel('cuenta_retenciones_recibidas'),
                ];

                #cuenta retenciones emitidas renta
                $nombre = "cuenta_retenciones_renta_emitidas";
                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                if (isset($parametro_sistema)) {
                    $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                    if (isset($cuenta))
                        $model->cuenta_retenciones_renta_emitidas = $parametro_sistema->valor;
                }
                $table_content[] = [
                    'form' => $form,
                    'model' => $model,
                    'attribute' => 'cuenta_retenciones_renta_emitidas',
                    'input' => 'select',
                    'allowClear' => true,
                    'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(true), 'id', 'text'),
                    'select_data_inside_pOption' => null,
                    'initValueText' => ($model->cuenta_retenciones_renta_emitidas != '') ?
                        (PlanCuenta::findOne($model->cuenta_retenciones_renta_emitidas)->nombre) : '',
                    'label' => $model->getAttributeLabel('cuenta_retenciones_renta_emitidas'),
                ];

                #cuenta retenciones recibidas renta
                $nombre = "cuenta_retenciones_renta_recibidas";
                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                if (isset($parametro_sistema)) {
                    $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                    if (isset($cuenta))
                        $model->cuenta_retenciones_renta_recibidas = $parametro_sistema->valor;
                }
                $table_content[] = [
                    'form' => $form,
                    'model' => $model,
                    'attribute' => 'cuenta_retenciones_renta_recibidas',
                    'input' => 'select',
                    'allowClear' => true,
                    'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(true), 'id', 'text'),
                    'select_data_inside_pOption' => null,
                    'initValueText' => ($model->cuenta_retenciones_renta_recibidas != '') ?
                        (PlanCuenta::findOne($model->cuenta_retenciones_renta_recibidas)->nombre) : '',
                    'label' => $model->getAttributeLabel('cuenta_retenciones_renta_recibidas'),
                ];

                echo '<fieldset>';
                {
                    echo '<legend class="text-info"><small>Retenciones</small></legend>';
                    echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
                }
                echo '</fieldset>';
            }
        }

        {
            // PARA ACTIVOS FIJOS

            $table_content = [];
            $nombre = "criterio_revaluo";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $model->criterio_revaluo = $parametro_sistema->valor;
            }
            $data = [
                Empresa::CRITERIO_REVALUO_PERIODO_SIGUIENTE => "1. A partir del sigte. ejercicio fiscal",
                Empresa::CRITERIO_REVALUO_MES_SIGUIENTE => "2. A partir del mes sigte. a su adquisición"
            ];
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'criterio_revaluo',
                'input' => 'select',
                'select_data' => $data,
                'allowClear' => true,
                'select_data_inside_pOption' => null,
                'initValueText' => ($model->criterio_revaluo != '') ?
                    $data[$model->criterio_revaluo] : '',
                'label' => $model->getAttributeLabel('criterio_revaluo'),
            ];

            echo '<fieldset>';
            {
                echo '<legend class="text-info"><small>Activos Fijos</small></legend>';
                echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
            }
            echo '</fieldset>';
        }

//        {
//            // PARA IMPORTACIONES
//
//            $table_content = [];
//            $nombre = "cuenta_pago_salario_id";
//            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
//            if (isset($parametro_sistema)) {
//                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
//                if (isset($cuenta))
//                    $model->cuenta_pago_salario_id = $parametro_sistema->valor;
//            }
//            $table_content[] = [
//                'form' => $form,
//                'model' => $model,
//                'attribute' => 'cuenta_pago_salario_id',
//                'input' => 'select',
//                'allowClear' => true,
//                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
//                'select_data_inside_pOption' => null,
//                'initValueText' => (isset($model->cuenta_pago_salario_id)) ?
//                    (PlanCuenta::findOne($model->cuenta_pago_salario_id)->nombre) : '',
//                'label' => $model->getAttributeLabel('cuenta_pago_salario_id'),
//            ];
//
//            $nombre = "cuenta_aporte_ips_id";
//            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
//            if (isset($parametro_sistema)) {
//                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
//                if (isset($cuenta))
//                    $model->cuenta_aporte_ips_id = $parametro_sistema->valor;
//            }
//            $table_content[] = [
//                'form' => $form,
//                'model' => $model,
//                'attribute' => 'cuenta_aporte_ips_id',
//                'input' => 'select',
//                'allowClear' => true,
//                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
//                'select_data_inside_pOption' => null,
//                'initValueText' => (isset($model->cuenta_aporte_ips_id)) ?
//                    (PlanCuenta::findOne($model->cuenta_aporte_ips_id)->nombre) : '',
//                'label' => $model->getAttributeLabel('cuenta_aporte_ips_id'),
//            ];
//
//            echo '<fieldset>';
//            {
//                echo '<legend class="text-info"><small>Importaciones</small></legend>';
//                echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
//            }
//            echo '</fieldset>';
//        }

        {
            // PARA CUENTAS DE CIERRE DE INVENTARIO DE A.BIO
            // José sugirió que sea solo por empresa sin periodo el 6 de diciembre 2018.

            $table_content = [];
            $nombre = "core_empresa-{$model->id}-cuenta_ganado";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_ganado = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_ganado',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_ganado) && $model->cuenta_ganado != "") ?
                    (PlanCuenta::findOne($model->cuenta_ganado)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_ganado'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_costo_venta_gnd";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_costo_venta_gnd = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_costo_venta_gnd',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_costo_venta_gnd) && $model->cuenta_costo_venta_gnd != "") ?
                    (PlanCuenta::findOne($model->cuenta_costo_venta_gnd)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_costo_venta_gnd'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_costo_venta_gd";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_costo_venta_gd = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_costo_venta_gd',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_costo_venta_gd) && $model->cuenta_costo_venta_gd != "") ?
                    (PlanCuenta::findOne($model->cuenta_costo_venta_gd)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_costo_venta_gd'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_procreo";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_procreo = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_procreo',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_procreo) && $model->cuenta_procreo != "") ?
                    (PlanCuenta::findOne($model->cuenta_procreo)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_procreo'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_mortandad_gnd";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_mortandad_gnd = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_mortandad_gnd',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_mortandad_gnd) && $model->cuenta_mortandad_gnd != '') ?
                    (PlanCuenta::findOne($model->cuenta_mortandad_gnd)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_mortandad_gnd'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_mortandad_gd";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_mortandad_gd = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_mortandad_gd',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_mortandad_gd) && $model->cuenta_mortandad_gd != '') ?
                    (PlanCuenta::findOne($model->cuenta_mortandad_gd)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_mortandad_gd'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_consumo_gnd";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_consumo_gnd = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_consumo_gnd',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_consumo_gnd) && $model->cuenta_consumo_gnd != '') ?
                    (PlanCuenta::findOne($model->cuenta_consumo_gnd)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_consumo_gnd'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_consumo_gd";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_consumo_gd = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_consumo_gd',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_consumo_gd) && $model->cuenta_consumo_gd != '') ?
                    (PlanCuenta::findOne($model->cuenta_consumo_gd)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_consumo_gd'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_valorizacion_hacienda";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_valorizacion_hacienda = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_valorizacion_hacienda',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_valorizacion_hacienda) && $model->cuenta_valorizacion_hacienda != '') ?
                    (PlanCuenta::findOne($model->cuenta_valorizacion_hacienda)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_valorizacion_hacienda'),
            ];

            echo '<fieldset>';
            {
                echo '<legend class="text-info"><small>Activo Biológico - Cierre de Inventario</small></legend>';
                echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
            }
            echo '</fieldset>';
        }

        {
            //IMPORTACIONES
            $table_content = [];
            $nombre = "cta_importacion_retencion_renta_debe";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_retencion_renta_debe = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_retencion_renta_debe',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_retencion_renta_debe)) ?
                    (PlanCuenta::findOne($model->cta_importacion_retencion_renta_debe)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_retencion_renta_debe'),
            ];

            $nombre = "cta_importacion_retencion_renta_haber";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_retencion_renta_haber = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_retencion_renta_haber',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_retencion_renta_haber)) ?
                    (PlanCuenta::findOne($model->cta_importacion_retencion_renta_haber)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_retencion_renta_haber'),
            ];

            $nombre = "cta_importacion_retencion_iva_debe";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_retencion_iva_debe = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_retencion_iva_debe',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_retencion_iva_debe)) ?
                    (PlanCuenta::findOne($model->cta_importacion_retencion_iva_debe)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_retencion_iva_debe'),
            ];

            $nombre = "cta_importacion_retencion_iva_haber";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_retencion_iva_haber = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_retencion_iva_haber',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_retencion_iva_haber)) ?
                    (PlanCuenta::findOne($model->cta_importacion_retencion_iva_haber)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_retencion_iva_haber'),
            ];

            $nombre = "cta_importacion_gnd_multa";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_gnd_multa = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_gnd_multa',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_gnd_multa)) ?
                    (PlanCuenta::findOne($model->cta_importacion_gnd_multa)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_gnd_multa'),
            ];

            $nombre = "cta_importacion_mercaderias_importadas";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_mercaderias_importadas = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_mercaderias_importadas',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_mercaderias_importadas)) ?
                    (PlanCuenta::findOne($model->cta_importacion_mercaderias_importadas)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_mercaderias_importadas'),
            ];

            $nombre = "cta_importacion_iva_10";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_iva_10 = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_iva_10',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_iva_10)) ?
                    (PlanCuenta::findOne($model->cta_importacion_iva_10)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_iva_10'),
            ];

            $nombre = "cta_importacion_prov_exterior";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_prov_exterior = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_prov_exterior',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_prov_exterior)) ?
                    (PlanCuenta::findOne($model->cta_importacion_prov_exterior)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_prov_exterior'),
            ];

            $nombre = "cta_importacion_haber";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_haber = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_haber',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_haber)) ?
                    (PlanCuenta::findOne($model->cta_importacion_haber)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_haber'),
            ];

            echo '<fieldset>';
            {
                echo '<legend class="text-info"><small>Importación</small></legend>';
                echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
            }
            echo '</fieldset>';
        }
    }

} catch (Exception $e) {
    echo $e;
}
?>

<?php

echo Dialog::widget();

$script =
    <<<JS
    $(document).on('click', '.tiene_modal', (function () {
        var boton = $(this);
        var title = boton.data('title');
        $.get(
            boton.data('url'),
            function (data) {
                var modal = $(boton.data('target'));
                $('.modal-body', modal).html(data);
                modal.modal();
                $('.modal-header', modal).css('background', '#3c8dbc');
                if (title)
                    $('.modal-title', modal).html(title);
            }
        );
    }));
    $(document).on('click', 'a.empresa-periodo-contable-delete-action', function (e) {
        var delete_btn = $(this);
        krajeeDialog.confirm("¿Está seguro de eliminar !!!?", function (result) {
            if (result) {
                $.post(
                    delete_btn.attr('href')
                )
                    .done(function (result) {
                        $.pjax.reload({container: "#periodos_grid", async: false});
                        $.pjax.reload({container: "#flash_message_id", async: false});
                    });
            }
        });
        e.preventDefault();
        e.stopImmediatePropagation();
        return false;
    });

    $(document).on('click', '.modal_cuentas_prestamo', (function () {
        var boton = $(this);
        var title = boton.data('title');
        $.get(
            boton.data('url'),
            function (data) {
                var modal = $(boton.data('target'));
                $('.modal-body', modal).html(data);
                modal.modal();
                $('.modal-header', modal).css('background', '#3c8dbc');
                if (title)
                    $('.modal-title', modal).html(title);
            }
        );
    }));
    
    // Operaciones al borrar un detalle venta.
    $(document).on('click', '.delete-cuenta-prestamo', function (e) {
        e.preventDefault();
        var deleteUrl = $(this).attr('delete-url');
        krajeeDialog.confirm('Está seguro?',
            function (result) {
                if (result) {
                    $.ajax({
                        url: deleteUrl,
                        type: 'post',
                        error: function (xhr, status, error) {
                            alert('There was an error with your request.' + xhr.responseText);
                        }
                    }).done(function (data) {
                        $.pjax.reload({container: "#grid_cuentas_prestamo", async: false});
                    });
                }
            }
        );
    });
JS;

$this->registerJs($script);

$css = "
        .btn-sin-estilo {
            border: 0;
            padding: 0 1px;
            background-color: inherit;
            color: #337ab7;
        }
        .btn-sin-estilo:hover, 
        .btn-sin-estilo:active, 
        .btn-sin-estilo:focus { color: #72afd2; }
        ";
$this->registerCss($css);

?>