<?php

namespace common\helpers;

use backend\models\Estado;
use backend\models\Moneda;
use backend\models\Rol;
use common\models\User;
use Yii;

class ValorHelpers
{
    public static function removeAccents($string)
    {
        return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'))), ' '));
    }

    public static function rolCoincide($rol_nombre)
    {
        $userTieneRolNombre = Yii::$app->user->identity->rol->rol_nombre;
        return strtoupper(ValorHelpers::removeAccents($userTieneRolNombre)) == strtoupper(ValorHelpers::removeAccents($rol_nombre)) ? true : false;
    }

    public static function getUsersRolValor($userId = null)
    {
        if ($userId == null) {
            $usersRolValor = Yii::$app->user->identity->rol->rol_valor;
            return isset($usersRolValor) ? $usersRolValor : false;
        } else {
            $user = User::findOne($userId);
            $usersRolValor = $user->rol->rol_valor;
            return isset($usersRolValor) ? $usersRolValor : false;
        }
    }

    public static function getRolValor($rol_nombre)
    {
        $rol = Rol::find()
            ->where(['rol_nombre' => $rol_nombre])
            ->one();
        return isset($rol->rol_valor) ? $rol->rol_valor : false;
    }

    public static function esRolNombreValido($rol_nombre)
    {
        $rol = Rol::find()
            ->where(['rol_nombre' => $rol_nombre])
            ->one();
        return isset($rol->rol_nombre) ? true : false;
    }

    public static function estadoCoincide($estado_nombre)
    {
        $userTieneEstadoName = Yii::$app->user->identity->estado->estado_nombre;
        return $userTieneEstadoName == $estado_nombre ? true : false;
    }

    public static function getEstadoId($estado_nombre)
    {
        $estado = Estado::find()
            ->where(['estado_nombre' => $estado_nombre])
            ->one();
        return isset($estado->id) ? $estado->id : false;
    }

    public static function tipoUsuarioCoincide($tipo_usuario_nombre)
    {
        $userTieneTipoUsurioName = Yii::$app->user->identity->tipoUsuario->tipo_usuario_nombre;
        return $userTieneTipoUsurioName == $tipo_usuario_nombre ? true : false;
    }

    /**
     * Formatea un string de la forma "5,000.25" a "5000.25"
     * @param $number
     * @return mixed
     */
    public static function formatStringToFloat($number)
    {
        //Se convierte a una manera que maneje float
        if (strpos($number, '.') && strpos($number, ',')) {
            $number = str_replace(',', '', $number);
        } elseif (strpos($number, '.') && !strpos($number, ',')) {
        } elseif (!strpos($number, '.') && strpos($number, ',')) {
            $number = str_replace(',', '', $number);
        }
        return $number;
    }

    public static function numberFormat($number, $decimales)
    {
        return number_format($number, $decimales, ',', '.');
    }

    /**
     * Retorna un número en formato español.
     * Además, si se especifica una moneda y es en guaraníes, retorna sólo la parte entera, sin redondeo.
     *
     * @param $moneda Moneda
     * @return string
     */
    public static function numberFormatMonedaSensitive($number, $decimals = 2, $moneda = null)
    {
        if ($moneda == null || $moneda->tieneDecimales()) {
            // Formatea el numero: m.cdu,dd
            //Hacer diferecia
            $diff = number_format($number, 2, '.', '') - number_format($number, 0, '.', '');
            $number = $diff != 0 ? number_format((float)$number, $decimals, ',', '.')
                : number_format((float)$number, '0', ',', '.');
            return $number;
        } else {
            // Si la moneda es guaranies, retornar m.cdu
            if ($moneda != null) { // si es guaranies
                $number = number_format((float)$number, '0', ',', '.');
            }
        }

        return $number;
    }
}
