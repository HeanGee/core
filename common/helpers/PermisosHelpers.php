<?php

namespace common\helpers;

use backend\models\Rol;
use Yii;
use yii\helpers\Url;

class PermisosHelpers
{
    public static function requerirEstado($estado_nombre)
    {
        return ValorHelpers::estadoCoincide($estado_nombre);
    }

    public static function requerirRol($rol_nombre)
    {
        return ValorHelpers::rolCoincide($rol_nombre);
    }

    public static function requerirMinimoRol($rol_nombre, $userId = null)
    {
        if (ValorHelpers::esRolNombreValido($rol_nombre)) {
            if ($userId == null) {
                $userRolValor = ValorHelpers::getUsersRolValor();
            } else {
                $userRolValor = ValorHelpers::getUsersRolValor($userId);
            }
            return $userRolValor >= ValorHelpers::getRolValor($rol_nombre) ? true : false;
        } else {
            return false;
        }
    }

    public static function userDebeSerPropietario($model_nombre, $model_id)
    {
        $connection = \Yii::$app->db;
        $userid = Yii::$app->user->identity->id;
        $sql = "SELECT id FROM $model_nombre WHERE usuario_id=:userid AND id=:model_id";
        $command = $connection->createCommand($sql);
        $command->bindValue(":userid", $userid);
        $command->bindValue(":model_id", $model_id);
        if ($result = $command->queryOne()) {
            return true;
        } else {
            return false;
        }
    }

    public static function getAcceso($operacion)
    {
        if (\Yii::$app->getUser()->isGuest &&
            \Yii::$app->getRequest()->url !== Url::to(\Yii::$app->getUser()->loginUrl)
        ) {
            return \Yii::$app->getResponse()->redirect(\Yii::$app->getUser()->loginUrl);
        }

        $superUsuario = Rol::findOne(['rol_nombre' => 'SuperUsuario']);
        if (Yii::$app->user->identity->rol_id == $superUsuario->id) {
            return true;
        }

        $connection = \Yii::$app->db;
        $sql = "SELECT o.operacion
                FROM core_usuario u
                JOIN core_rol r ON u.rol_id = r.id
                JOIN core_rol_operacion ro ON r.id = ro.rol_id
                JOIN core_operacion o ON ro.operacion_id = o.id
                WHERE o.operacion =:operacion
                AND u.rol_id =:rol_id";
        $command = $connection->createCommand($sql);
        $command->bindValue(":operacion", $operacion);
        $command->bindValue(":rol_id", Yii::$app->user->identity->rol_id);
        $result = $command->queryOne();

        if ($result['operacion'] != null) {
            return true;
        } else {
            return false;
        }
    }

    public static function esSuperUsuario()
    {
        $superUsuario = Rol::findOne(['rol_nombre' => 'SuperUsuario']);

        if (Yii::$app->user->identity->rol_id == $superUsuario->id)
            return true;

        return false;
    }

    public static function esAmin()
    {
        $superUsuario = Rol::findOne(['rol_nombre' => 'Admin']);

        if (Yii::$app->user->identity->rol_id == $superUsuario->id)
            return true;

        return false;
    }
}
