<?php
/*
  CifrasEnLetras.php — ProInf.net — nov-2009, ago-2011

  CifrasEnLetras sirve para expresar una serie de cifras en letras.
  A modo de ejemplo convierte "22"> en "veintidós".
  Puede convertir un número entre una y ciento veintiséis cifras como máximo.

  Ejemplos de uso:
    CifrasEnLetras::convertirEurosEnLetras(22.34) --> "veintidós euros con treinta y cuatro céntimos"
    CifrasEnLetras::convertirNumeroEnLetras("35,67") --> "treinta y cinco con sesenta y siete"

  Enlaces de referencia:
    http://es.wikipedia.org/wiki/Anexo:Lista_de_n%C3%BAmeros
    http://es.encarta.msn.com/encyclopedia_761577100/Signos_matem%C3%A1ticos.html
    http://es.wikipedia.org/wiki/Nombres_de_los_n%C3%BAmeros_en_espa%C3%B1ol

  Licencia:
    http://creativecommons.org/licenses/GPL/2.0/deed.es
    Este software está sujeto a la CC-GNU GPL</a>
*/

//===============================================
namespace common\helpers;

use DateTime;

class DiferenciaEntreFechas
{
    public static function calcularDosFechasSimple($fecha_menor, $fecha_mayor)
    {
        $fecha1 = strtotime($fecha_menor);
        $fecha2 = strtotime($fecha_mayor);
        if ($fecha1 > $fecha2)
            echo "fecha_menor > fecha_mayor";

        $fecha1 = new DateTime($fecha_menor);
        $fecha2 = new DateTime($fecha_mayor);
        $diff = $fecha1->diff($fecha2);
        return DiferenciaEntreFechas::get_format_simple($diff);
    }

    public static function calcularHoySimple($fecha_menor)
    {
        $fecha_mayor = new DateTime("now");
        $fecha1 = new DateTime($fecha_menor);
        $diff = $fecha1->diff($fecha_mayor);
        return DiferenciaEntreFechas::get_format_simple($diff);
    }

    function get_format_full($df)
    {
        $str = '';
        $str .= ($df->invert == 1) ? ' - ' : '';
        if ($df->y > 0) {
            // years
            $str .= ($df->y > 1) ? $df->y . ' Años ' : $df->y . ' Año ';
        }
        if ($df->m > 0) {
            // month
            $str .= ($df->m > 1) ? $df->m . ' Meses ' : $df->m . ' Mes ';
        }
        if ($df->d > 0) {
            // days
            $str .= ($df->d > 1) ? $df->d . ' Días ' : $df->d . ' Día ';
        }
        if ($df->h > 0) {
            // hours
            $str .= ($df->h > 1) ? $df->h . ' Horas ' : $df->h . ' Hora ';
        }
        if ($df->i > 0) {
            // minutes
            $str .= ($df->i > 1) ? $df->i . ' Minutos ' : $df->i . ' Minuto ';
        }
        if ($df->s > 0) {
            // seconds
            $str .= ($df->s > 1) ? $df->s . ' Segundos ' : $df->s . ' Segundo ';
        }

        echo $str;
    }

    public static function get_format_simple($df)
    {
        $str = '';
        $str .= ($df->invert == 1) ? ' - ' : '';
        if ($df->y > 0) {
            // years
            $str .= ($df->y > 1) ? $df->y . ' Años ' : $df->y . ' Año ';
        }
        if ($df->m > 0) {
            // month
            $str .= ($df->m > 1) ? $df->m . ' Meses ' : $df->m . ' Mes ';
        }
        if ($df->d > 0) {
            // days
            $str .= ($df->d > 1) ? $df->d . ' Días ' : $df->d . ' Día ';
        }

        return $str;
    }
}

?>
