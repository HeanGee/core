<?php

namespace common\helpers;

use kartik\mpdf\Pdf;

/**
 * Class ParametroSistemaHelpers
 * @package common
 *
 */
class PdfFormatHelpers extends Pdf
{
    const OFICIO_PY = [612.28, 935.43];  // Valor en mm [216, 330]. Este valor está escalado según el factor de escalamiento 72 / 25.4
}
