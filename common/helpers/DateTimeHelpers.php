<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 23/1/2018
 * Time: 08:24
 */

namespace common\helpers;


class DateTimeHelpers
{
    public static function mesATexto($mes){
    $meses = ['01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre',
        '10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre'];
    return $meses[$mes];
    }
}