<?php


use yii\queue\Queue;
use yii\helpers\Html;
use backend\modules\contabilidad\models\Entidad;

class InsertEntidadesFromSet extends \yii\base\BaseObject implements \yii\queue\JobInterface
{

	/**
	 * @param Queue $queue which pushed and is handling the job
	 * @return void|mixed result of the job execution
	 */
	public function execute($queue) {
		echo "Holis";
	}

	private function task() {
		function microtime_float() {
			list($usec, $sec) = explode(" ", microtime());
			return ((float)$usec + (float)$sec);
		}

		$carpeta = Yii::$app->basePath . '/web/uploads/';
		for ($i = 0; $i < 9; $i++) {
			$handle = fopen("{$carpeta}/ruc{$i}.txt", "r");
			if ($handle) {
				$trans = Yii::$app->db->beginTransaction();

				try {
					$cant_lineas = 0;
					$cant_lineas_guardadas = 0;
					$cant_ya_existe = 0;
					$can_filas_no_formato = 0;
					$lines = [];

					$a = microtime_float();
					while (!feof($handle)) {
						$lines[] = trim(fgets($handle));
					}

					$b = microtime_float();
					$log = [];
					foreach ($lines as $index => $line) {
						if (!strlen($line)) continue;
						$cant_lineas++;
						$fila = explode('|', $line);
						$nro_fila = $index + 1;
						if (count($fila) == 5) // 4 columnas + fin de linea
						{
							if (!Entidad::find()->where(['ruc' => $fila[0]])->exists()) {
								$query = new Entidad();
								$query->ruc = $fila[0];
								$query->razon_social = $fila[1];
								$query->digito_verificador = $fila[2];
								$query->direccion = '';
								$query->telefonos = '';
								$query->es_contribuyente = 'Si';
								$query->tipo_entidad = null;

								if ($query->validate()) {
									$cant_lineas_guardadas += $query->save() ? 1 : 0;
								} else {
									$log[] = [
										'fila' => $nro_fila,
										'msg' => "Error validando $fila[1]: {$query->getErrorSummaryAsString()}",
									];
								}
							} else $cant_ya_existe++;
						} else {
							$log[] = [
								'fila' => $nro_fila,
								'msg' => "La fila {$nro_fila} no tiene el formato correcto.",
							];
							$can_filas_no_formato++;
						}
					}
					$c = microtime_float();
					$mensaje = "Filas de archivo leidas: " . $cant_lineas . Html::tag('br/') .
						"Entidades guardadas: " . $cant_lineas_guardadas . Html::tag('br/') .
						"Filas sin el formato correcto: " . $can_filas_no_formato . Html::tag('br/') .
						($cant_ya_existe ? "Entidades ya existentes: " . $cant_ya_existe . '.' . Html::tag('br/') : Html::tag('br/')) .
						"Tiempo de carga : " . (($b - $a) / 60) . " minutos" . Html::tag('br/') .
						"Tiempo de procesado: " . (($c - $b) / 60) . " minutos" . Html::tag('br/');

					$trans->commit();
					echo $mensaje;
				} catch (\Exception $e) {
					$trans->rollBack();
					fclose($handle);
					throw $e;
				}

				fclose($handle);
			}
		}
	}
}