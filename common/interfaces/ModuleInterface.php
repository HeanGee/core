<?php

namespace common\interfaces;

interface ModuleInterface
{
    /**
     * Retorna los ítems de menú del módulo.
     * @return mixed
     */
    public static function getMenuItems();

    /**
     * Retorna la lista de variables de sesión del modulo, dependientes de la empresa actual seleccionada.
     * @return mixed
     */
    public static function getSessionVariablesDepEmpresa();
}
